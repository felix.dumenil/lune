use crate::TokenPos;
use std::collections::{BTreeMap, BTreeSet, HashSet};

pub type Ast = Box<[TopLevelField]>;

#[macro_export]
macro_rules! new_expr {
    (
        $raw_expr:expr,
        $pos:expr$(,)?
    ) => {{
        use crate::ast::RawExpr::*;
        crate::ast::Expr {
            raw_expr: $raw_expr,
            pos: $pos,
        }
    }};
}

#[macro_export]
macro_rules! new_type {
    (
        $raw_type:expr,
        $pos:expr$(,)?
    ) => {{
        #[allow(unused_imports)]
        use crate::ast::RawType::*;
        crate::ast::Type {
            raw_type: $raw_type,
            pos: Some($pos),
        }
    }};
    (
        $raw_type:expr$(,)?
    ) => {{
        #[allow(unused_imports)]
        use crate::ast::RawType::*;
        crate::ast::Type {
            raw_type: $raw_type,
            pos: None,
        }
    }};
}

#[derive(Debug, PartialEq)]
pub enum TopLevelField {
    Function {
        pos: TokenPos,
        container: FunctionContainer,
        name: String,
        args: Box<[(String, Type, bool)]>,
        return_type: Option<Box<Type>>,
        prototype_pos: TokenPos,
        body: Block,
    },
    Class {
        pos: TokenPos,
        name: String,
        body: StructBody,
    },
    Global {
        pos: TokenPos,
        is_mut: bool,
        name: String,
        type_: Option<Box<Type>>,
        value: Box<Expr>,
    },
    Declare {
        pos: TokenPos,
        name: String,
        type_: Type,
    },
    Require {
        pos: TokenPos,
        path: String,
    },
}

#[derive(Debug, PartialEq)]
pub enum FunctionContainer {
    None,
    Method(String, TokenPos),
    StaticFunction(String, TokenPos),
}

#[derive(Debug, PartialEq, Clone)]
pub struct Type {
    pub raw_type: RawType,
    pub pos: Option<TokenPos>,
}

#[derive(Debug, Clone, PartialEq)]
pub enum RawType {
    Int,
    Real,
    Bool,
    String,
    Nil,
    Callable {
        args: Box<[Type]>,
        return_type: Option<Box<Type>>,
    },
    Optional(Box<Type>),
    Array(Box<Type>),
    EmptyArray,
    Map(Box<(Type, Type)>),

    Ident(String),

    ClassInstance(Class),
    ClassObject(Class),
    Struct(StructBody),
    Enum(BTreeSet<String>),

    Anything,
    Never,
}

impl Type {
    pub fn with_nil(&self) -> Self {
        match &self.raw_type {
            RawType::Anything => new_type!(Anything),
            RawType::Nil | RawType::Never => new_type!(Nil),
            RawType::Optional(_child_type) => self.clone(),
            _ => new_type!(Optional(Box::new(self.clone()))),
        }
    }
    pub fn without_nil(&self) -> Self {
        match &self.raw_type {
            RawType::Nil | RawType::Never => new_type!(Never),
            RawType::Optional(child_type) => child_type.as_ref().clone(),
            _ => self.clone(),
        }
    }
    pub fn only_nil(&self) -> Self {
        match &self.raw_type {
            RawType::Anything | RawType::Optional(_) | RawType::Nil => new_type!(Nil),
            _ => new_type!(Never),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Class {
    pub name: String,
    pub body: StructBody,
    // TODO : store as Vec, and keep order
    pub static_decls: BTreeMap<String, Type>,
}

#[derive(Debug, Clone, PartialEq)]
pub struct StructBody {
    // TODO : store as Vec, and keep order
    pub fields: BTreeMap<String, Type>,

    pub union_: Option<Union>,
}

impl StructBody {
    pub fn get_key_type(&self) -> RawType {
        if self.fields.is_empty() {
            // empty enums are kind of buggy
            // TODO : forbid empty enums elsewhere (or fix them)
            RawType::Never
        } else {
            RawType::Enum(self.fields.keys().cloned().collect())
        }
    }

    pub fn get_field(
        &self,
        field_name: &str,
        context: &crate::compiler::CompilationContext,
    ) -> Result<Option<Type>, crate::CompileError> {
        if let Some(field_type) = self.fields.get(field_name) {
            return Ok(Some(field_type.clone()));
        }

        if let Some(union_) = &self.union_ {
            if field_name == union_.kind_field {
                return Ok(Some(union_.get_kind_type()));
            }
            if let Some(activated_variant) = &union_.activated_variant {
                let union_fields = union_
                    .variants
                    .get(activated_variant.as_str())
                    .expect("invalid activated variant");
                if let Some(field_type) = union_fields.get(field_name) {
                    return Ok(Some(field_type.clone()));
                }
            } else {
                // list all union fields, but optional
                let mut union_type: Option<Type> = None;
                for variant in union_.variants.values() {
                    if let Some(field_type) = variant.get(field_name) {
                        if let Some(acc) = union_type {
                            union_type =
                                Some(context.get_type_union(&field_type.with_nil(), &acc)?);
                        } else {
                            union_type = Some(field_type.with_nil());
                        }
                    }
                }
                if let Some(union_type) = union_type {
                    return Ok(Some(union_type));
                }
            }
        }

        Ok(None)
    }
    pub fn get_field_or_nil(
        &self,
        field_name: &str,
        context: &crate::compiler::CompilationContext,
    ) -> Result<Type, crate::CompileError> {
        Ok(self
            .get_field(field_name, context)?
            .unwrap_or_else(|| new_type!(Nil)))
    }

    pub fn get_effective_fields<'s>(
        &'s self,
        context: &'s crate::compiler::CompilationContext,
    ) -> impl Iterator<Item = (&'s String, Type)> + 's {
        self.fields
            .iter()
            .map(|(k, v)| (k, v.clone()))
            .chain(
                self.union_
                    .as_ref()
                    .map(|u| (&u.kind_field, u.get_kind_type())),
            )
            .chain(
                self.union_
                    .iter()
                    .map(move |u: &Union| {
                        u.variants
                            .iter()
                            .filter(|(variant_name, _fields): &(&String, _)| {
                                u.activated_variant.is_none()
                                    || Some(*variant_name) != u.activated_variant.as_ref()
                            })
                            .map(
                                |(_variant_name, fields): (_, &BTreeMap<String, _>)| fields.keys(),
                                // .map(move |(field_name, _field_type)|
                                //     if u.activated_variant.as_ref() == Some(variant_name) {
                                //         (field_name, field_type.with_nil())
                                //     } else {
                                //         (field_name, field_type.clone())
                                //     }
                                // )
                            )
                            .flatten()
                            .collect::<BTreeSet<&String>>() // remove duplicates
                            .into_iter()
                            .map(move |field_name: &String| {
                                (
                                    field_name,
                                    self.get_field(field_name, context)
                                        .expect("TODO error handling for this")
                                        .unwrap_or_else(|| new_type!(Nil)),
                                )
                            })
                    })
                    .flatten(),
            )
    }

    // pub fn get_union_of_all_fields(&self) -> crate::types::Type {
    //     self.fields.values().fold(crate::types::Type::Never, |acc, field_type| {
    //         crate::types::Type::get_type_union(&acc, field_type)
    //     })
    // }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Union {
    pub kind_field: String,
    pub activated_variant: Option<String>,
    pub variants: BTreeMap<String, BTreeMap<String, Type>>,
}

impl Union {
    pub fn get_kind_type(&self) -> Type {
        new_type!(Enum(self.variants.keys().cloned().collect()))
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct Block {
    pub insts: Box<[Inst]>,
    pub pos: TokenPos,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Inst {
    pub raw_inst: RawInst,
    pub pos: TokenPos,
}

#[derive(Debug, Clone, PartialEq)]
pub enum RawInst {
    Let {
        is_mut: bool,
        name: String,
        type_: Option<Type>,
        value: Option<Expr>,
    },
    Expr(Expr),
    Assign(Expr, Expr),
    Return(Expr),
    If {
        condition: Box<Expr>,
        then_block: Block,
        else_block: Option<Block>,
    },
    While {
        condition: Expr,
        block: Block,
    },
    For {
        index_pos: TokenPos,
        index_ident: String,
        value_pos: TokenPos,
        value_ident: String,
        iter_type: ForIterType,
        iterator_expr: Expr,

        block: Block,
    },
    ForRange {
        index_pos: TokenPos,
        index_ident: String,
        start_expr: Expr,
        end_expr: Expr,

        block: Block,
    },
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum ForIterType {
    Array,
    Map,
}

#[derive(Debug, Clone, PartialEq)]
pub struct Expr {
    pub raw_expr: RawExpr,
    pub pos: TokenPos,
}

#[derive(Debug, Clone, PartialEq)]
pub enum RawExpr {
    IntLiteral(i64),
    RealLiteral(f64),
    BoolLiteral(bool),
    NilLiteral,
    StringLiteral(String),

    Literal(String),

    Index {
        container_expr: Box<Expr>,
        index_expr: Box<Expr>,
    },
    StaticIndex {
        container_expr: Box<Expr>,
        index: String,
    },

    FunctionCall {
        function_expr: Box<Expr>,
        args: Box<[Expr]>,
    },
    MethodCall {
        object_expr: Box<Expr>,
        function_name: String,
        args: Box<[Expr]>,
    },
    AtCall {
        at_function: AtFunction,
        args: Box<[Expr]>,
    },
    New {
        class_type: Box<Type>,
        // TODO : store as Vec, and keep order
        fields: BTreeMap<String, Expr>,
    },
    TableLiteral {
        // TODO : store as Vec, and keep order
        fields: BTreeMap<String, Expr>,
    },
    ArrayLiteral(Vec<Expr>),

    Add(Box<(Expr, Expr)>),
    Sub(Box<(Expr, Expr)>),
    Mul(Box<(Expr, Expr)>),
    Div(Box<(Expr, Expr)>),
    Mod(Box<(Expr, Expr)>),
    Concat(Box<(Expr, Expr)>),
    Pow(Box<(Expr, Expr)>),

    Minus(Box<Expr>),

    Eq(Box<(Expr, Expr)>),
    NotEq(Box<(Expr, Expr)>),
    Greater(Box<(Expr, Expr)>),
    GreaterEq(Box<(Expr, Expr)>),
    Lower(Box<(Expr, Expr)>),
    LowerEq(Box<(Expr, Expr)>),

    Not(Box<Expr>),
    And(Box<(Expr, Expr)>),
    Or(Box<(Expr, Expr)>),

    UnwrapOptional(Box<Expr>),
    Cast(Box<Expr>, Box<Type>),
}

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum AtFunction {
    Insert,
    Len,
    Abs,
    Min,
    Max,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum Check {
    IsNotNil {
        ident: String,
    },
    IsNil {
        ident: String,
    },
    FieldIsString {
        ident: String,
        field_name: String,
        value: String,
    },
}

impl Check {
    pub fn invert(self) -> Option<Check> {
        match self {
            Check::IsNotNil { ident } => Some(Check::IsNil { ident }),
            Check::IsNil { ident } => Some(Check::IsNotNil { ident }),
            Check::FieldIsString { .. } => None,
        }
    }
}

// should be a conditional expr
pub fn infer_checks_from_expr(expr: &Expr) -> HashSet<Check> {
    match &expr.raw_expr {
        RawExpr::Not(subexpr) => infer_checks_from_expr(&subexpr)
            .into_iter()
            .filter_map(Check::invert)
            .collect(),
        RawExpr::And(boxed) => {
            let (a, b) = boxed.as_ref();
            let a_checks = infer_checks_from_expr(a);
            let b_checks = infer_checks_from_expr(b);
            a_checks.union(&b_checks).cloned().collect()
        }
        RawExpr::Or(boxed) => {
            let (a, b) = boxed.as_ref();
            let a_checks = infer_checks_from_expr(a);
            let b_checks = infer_checks_from_expr(b);
            a_checks.intersection(&b_checks).cloned().collect()
        }

        RawExpr::Eq(boxed) => {
            let (a, b) = boxed.as_ref();
            match (&a.raw_expr, &b.raw_expr) {
                (RawExpr::Literal(ident), RawExpr::NilLiteral)
                | (RawExpr::NilLiteral, RawExpr::Literal(ident)) => {
                    let mut set = HashSet::new();
                    set.insert(Check::IsNil {
                        ident: ident.clone(),
                    });
                    set
                }
                (
                    RawExpr::StaticIndex {
                        container_expr,
                        index,
                    },
                    RawExpr::StringLiteral(value),
                )
                | (
                    RawExpr::StringLiteral(value),
                    RawExpr::StaticIndex {
                        container_expr,
                        index,
                    },
                ) => match &container_expr.raw_expr {
                    RawExpr::Literal(ident) => {
                        let mut set = HashSet::new();
                        set.insert(Check::FieldIsString {
                            ident: ident.clone(),
                            field_name: index.clone(),
                            value: value.clone(),
                        });
                        set
                    }
                    _ => HashSet::new(),
                },
                _ => HashSet::new(),
            }
        }
        RawExpr::NotEq(boxed) => {
            let (a, b) = boxed.as_ref();
            match (&a.raw_expr, &b.raw_expr) {
                (RawExpr::Literal(ident), RawExpr::NilLiteral)
                | (RawExpr::NilLiteral, RawExpr::Literal(ident)) => {
                    let mut set = HashSet::new();
                    set.insert(Check::IsNotNil {
                        ident: ident.clone(),
                    });
                    set
                }
                _ => HashSet::new(),
            }
        }

        _ => HashSet::new(),
    }
}

impl std::fmt::Display for Type {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        write!(f, "{}", self.raw_type)
    }
}

impl std::fmt::Display for RawType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        match self {
            Self::Int => f.write_str("int")?,
            Self::Real => f.write_str("real")?,
            Self::Bool => f.write_str("bool")?,
            Self::String => f.write_str("string")?,
            Self::Nil => f.write_str("nil")?,

            Self::Anything => f.write_str("anything")?,
            Self::Never => f.write_str("never")?,
            Self::Array(child) => write!(f, "[{}]", child)?,
            Self::EmptyArray => f.write_str("[]")?,
            Self::Optional(child) => write!(f, "?{}", child)?,
            Self::Callable { args, return_type } => {
                f.write_str("fn (")?;
                for (i, arg) in args.iter().enumerate() {
                    if i != 0 {
                        f.write_str(", ")?;
                    }
                    write!(f, "{}", arg)?;
                }
                f.write_str(")")?;
                if let Some(return_type) = return_type {
                    write!(f, " -> {}", return_type)?;
                }
            }

            Self::Ident(s) => f.write_str(s)?,
            Self::Struct(StructBody { fields, union_ }) => {
                f.write_str("struct {")?;

                let mut wrote_struct_field = false;
                let maybe_write_a_comma =
                    |f: &mut std::fmt::Formatter<'_>, wrote_field: &mut bool| {
                        if *wrote_field {
                            f.write_str(", ")?;
                        } else {
                            *wrote_field = true;
                        }
                        Ok(())
                    };

                for (field_name, field_type) in fields {
                    maybe_write_a_comma(f, &mut wrote_struct_field)?;
                    write!(f, "{}: {}", field_name, field_type)?;
                }
                if let Some(u) = union_ {
                    maybe_write_a_comma(f, &mut wrote_struct_field)?;

                    write!(f, "union ({}) {{", u.kind_field)?;
                    let mut wrote_union_field = false;
                    for (variant_name, variant_fields) in &u.variants {
                        maybe_write_a_comma(f, &mut wrote_union_field)?;

                        let mut wrote_variant_field = false;
                        write!(f, "\"{}\" -> {{", variant_name)?;
                        for (field_name, field_type) in variant_fields {
                            maybe_write_a_comma(f, &mut wrote_variant_field)?;
                            write!(f, "{}: {}", field_name, field_type)?;
                        }
                        write!(f, "}}")?;
                    }
                    write!(f, "}}")?;
                }
                f.write_str("}")?;
            }
            Self::Enum(values) => {
                if values.len() == 1 {
                    write!(f, "\"{}\"", values.iter().next().unwrap())?;
                } else {
                    f.write_str("enum {")?;
                    for (i, value) in values.iter().enumerate() {
                        if i != 0 {
                            f.write_str(", ")?;
                        }
                        write!(f, "\"{}\"", value)?;
                    }
                    f.write_str("}")?;
                }
            }
            Self::ClassInstance(class) => f.write_str(&class.name)?,
            Self::ClassObject(class) => write!(f, "Class {}", class.name)?,
            Self::Map(boxed) => {
                let (key, value) = boxed.as_ref();
                write!(f, "map({} -> {})", key, value)?;
            }
        }
        Ok(())
    }
}
