use crate::ast;
use crate::tokenizer::{RawToken, Token};
use crate::{new_expr, new_type};
use crate::{CompileError, TokenPos};
use std::collections::BTreeMap;
use std::collections::VecDeque;
use std::rc::Rc;

macro_rules! match_token {
    (
        ($tokens:expr, $expected:expr)
        {
            $( $pat:pat => $expr:expr ),*
            $(,)?
        }
    ) => {{
        let token = get_token($tokens)?;
        match token.raw_tok {
            $(
                $pat => $expr,
            )*
            _ => return Err(unexpected_token_error(token, $expected)),
        }
    }};
    (
        ($tokens:expr, $expected:expr, $pos:ident)
        {
            $( $pat:pat => $expr:expr ),*
            $(,)?
        }
    ) => {{
        let Token{ raw_tok, pos: $pos } = get_token($tokens)?;
        match raw_tok {
            $(
                $pat => $expr,
            )*
            _ => return Err(unexpected_token_error(Token{ raw_tok, pos: $pos }, $expected)),
        }
    }};
}

pub fn parse(source: &str, filename: Option<Rc<String>>) -> Result<ast::Ast, CompileError> {
    let tokens = crate::tokenizer::tokenize(source, filename)?;
    let mut tokens = VecDeque::from(tokens);
    let mut top_level_fields = Vec::<ast::TopLevelField>::new();

    while !tokens.is_empty() {
        top_level_fields.push(parse_top_level_field(&mut tokens)?);
    }

    Ok(top_level_fields.into_boxed_slice())
}

fn parse_top_level_field(tokens: &mut VecDeque<Token>) -> Result<ast::TopLevelField, CompileError> {
    match_token!((tokens, "'fn', 'class' or 'declare'", token_pos) {
        RawToken::Fn => parse_function(tokens, token_pos),
        RawToken::Declare => parse_declare(tokens, token_pos),
        RawToken::Class => parse_class(tokens, token_pos),
        RawToken::Require => {
            match_token!((tokens, "(") {
                RawToken::ParenOpen => {}
            });
            let path = match_token!((tokens, "require path") {
                RawToken::StringLiteral(s) => s,
            });
            match_token!((tokens, ")") {
                RawToken::ParenClose => {}
            });
            let end_pos = match_token!((tokens, ";", end_pos) {
                RawToken::Semicolon => end_pos
            });
            let pos = token_pos.extend(&end_pos);

            Ok(ast::TopLevelField::Require { pos, path })
        },
        RawToken::Let => {
            let is_mut = consume_next_token_if_its(tokens, RawToken::Mut)?.is_some();
            let name = match_token!((tokens, "binding name for global variable") {
                RawToken::Literal(s) => s,
            });

            let type_;
            if consume_next_token_if_its(tokens, RawToken::Colon)?.is_some() {
                type_ = Some(Box::new(parse_type(tokens)?));
            } else {
                type_ = None;
            }

            match_token!((tokens, "'='") {
                RawToken::Set => {}
            });

            let value = Box::new(parse_expr(tokens)?);

            let end_pos = match_token!((tokens, ";", end_pos) {
                RawToken::Semicolon => end_pos
            });
            let pos = token_pos.extend(&end_pos);

            Ok(ast::TopLevelField::Global {
                pos,
                is_mut,
                name,
                type_,
                value,
            })
        },
    })
}

fn parse_declare(
    tokens: &mut VecDeque<Token>,
    declare_pos: TokenPos,
) -> Result<ast::TopLevelField, CompileError> {
    let name;
    let pos;
    match_token!((tokens, "function name") {
        RawToken::Literal(s) => name = s,
    });

    match_token!((tokens, ":") {
        RawToken::Colon => {}
    });

    let type_ = parse_type(tokens)?;

    match_token!((tokens, ";", end_pos) {
        RawToken::Semicolon => pos = declare_pos.extend(&end_pos),
    });

    Ok(ast::TopLevelField::Declare { name, type_, pos })
}

fn parse_class(
    tokens: &mut VecDeque<Token>,
    class_pos: TokenPos,
) -> Result<ast::TopLevelField, CompileError> {
    let name;
    match_token!((tokens, "class name") {
        RawToken::Literal(s) => {name = s},
    });

    let (body, body_pos) = parse_struct_body(tokens)?;
    let pos = class_pos.extend(&body_pos);

    Ok(ast::TopLevelField::Class { name, body, pos })
}

fn parse_struct_body(
    tokens: &mut VecDeque<Token>,
) -> Result<(ast::StructBody, TokenPos), CompileError> {
    let brace_open_pos = match_token!((tokens, "{", brace_pos) {
        RawToken::BraceOpen => brace_pos,
    });

    let mut fields = BTreeMap::new();
    let pos;
    let mut union_ = None;

    loop {
        match_token!((tokens, "field name or '}'", brace_close_pos) {
            RawToken::Literal(field_name) => {
                match_token!((tokens, "':' (struct fields have types)") {
                    RawToken::Colon => {},
                });

                let field_type = parse_type(tokens)?;

                fields.insert(field_name, field_type);
            },
            RawToken::Union => {
                match_token!((tokens, "(") { RawToken::ParenOpen => {} });
                let kind_field = match_token!((tokens, "union kind field name") { RawToken::Literal(s) => s });
                match_token!((tokens, ")") { RawToken::ParenClose => {} });
                match_token!((tokens, "{") { RawToken::BraceOpen => {} });
                let mut variants = BTreeMap::new();

                // parse variants
                loop {
                    let variant_name = match_token!((tokens, "variant name or '}'") {
                        RawToken::StringLiteral(s) => s,
                        RawToken::BraceClose => break,
                    });
                    match_token!((tokens, "->") { RawToken::ArrowRight => {} });

                    let (variant_fields, _) = parse_union_variant_body(tokens)?;

                    variants.insert(variant_name, variant_fields);

                    match_token!((tokens, "',' or '}'", brace_close_pos) {
                        RawToken::Comma => {},
                        RawToken::BraceClose => {
                            break;
                        }
                    });
                }

                union_ = Some(ast::Union {
                    kind_field,
                    activated_variant: None,
                    variants,
                });
            },
            RawToken::BraceClose => {
                pos = brace_open_pos.extend(&brace_close_pos);
                break;
            }
        });

        match_token!((tokens, "',' or '}'", brace_close_pos) {
            RawToken::Comma => {},
            RawToken::BraceClose => {
                pos = brace_open_pos.extend(&brace_close_pos);
                break;
            }
        });
    }

    Ok((ast::StructBody { fields, union_ }, pos))
}
fn parse_union_variant_body(
    tokens: &mut VecDeque<Token>,
) -> Result<(BTreeMap<String, ast::Type>, TokenPos), CompileError> {
    let brace_open_pos = match_token!((tokens, "{", brace_pos) {
        RawToken::BraceOpen => brace_pos,
    });

    let mut fields = BTreeMap::new();
    let pos;

    loop {
        match_token!((tokens, "field name or '}'", brace_close_pos) {
            RawToken::Literal(field_name) => {
                match_token!((tokens, "':' (union fields have types)") {
                    RawToken::Colon => {},
                });

                let field_type = parse_type(tokens)?;

                fields.insert(field_name, field_type);
            },
            RawToken::BraceClose => {
                pos = brace_open_pos.extend(&brace_close_pos);
                break;
            }
        });

        match_token!((tokens, "',' or '}'", brace_close_pos) {
            RawToken::Comma => {},
            RawToken::BraceClose => {
                pos = brace_open_pos.extend(&brace_close_pos);
                break;
            }
        });
    }

    Ok((fields, pos))
}

fn parse_function(
    tokens: &mut VecDeque<Token>,
    fn_pos: TokenPos,
) -> Result<ast::TopLevelField, CompileError> {
    let first_name;
    let first_name_pos;
    match_token!((tokens, "function or container name", name_pos) {
        RawToken::Literal(s) => {first_name = s; first_name_pos = name_pos},
    });

    let container;
    let name;
    if consume_next_token_if_its(tokens, RawToken::Dot)?.is_some() {
        container = ast::FunctionContainer::StaticFunction(first_name, first_name_pos);
        match_token!((tokens, "function name", name_pos) {
            RawToken::Literal(s) => {name = s},
        });
    } else if consume_next_token_if_its(tokens, RawToken::Colon)?.is_some() {
        container = ast::FunctionContainer::Method(first_name, first_name_pos);
        match_token!((tokens, "function name", name_pos) {
            RawToken::Literal(s) => {name = s},
        });
    } else {
        container = ast::FunctionContainer::None;
        name = first_name;
    }

    match_token!((tokens, "(") {
        RawToken::ParenOpen => {}
    });

    let mut args = Vec::new();

    let paren_close_pos = loop {
        if let Some(paren_pos) = consume_next_token_if_its(tokens, RawToken::ParenClose)? {
            break paren_pos;
        }
        let is_mut = consume_next_token_if_its(tokens, RawToken::Mut)?.is_some();
        let arg_name = match_token!((tokens, "argument name") {
            RawToken::Literal(s) => s,
        });

        match_token!((tokens, "':' (functions args have types)") {
            RawToken::Colon => {}
        });

        let arg_type = parse_type(tokens)?;

        args.push((arg_name, arg_type, is_mut));

        match_token!((tokens, "',' or ')'", paren_pos) {
            RawToken::Comma => continue,
            RawToken::ParenClose => break paren_pos,
        });
    };

    let mut return_type = None;
    let prototype_pos;
    if consume_next_token_if_its(tokens, RawToken::ArrowRight)?.is_some() {
        let ret_type = parse_type(tokens)?;
        prototype_pos = fn_pos.extend(ret_type.pos.as_ref().unwrap());
        return_type = Some(Box::new(ret_type));
    } else {
        prototype_pos = fn_pos.extend(&paren_close_pos);
    }

    let brace_pos;
    match_token!((tokens, "{", pos) {
        RawToken::BraceOpen => brace_pos = pos,
    });

    let body = parse_block(tokens, brace_pos)?;
    let pos = fn_pos.extend(&body.pos);

    Ok(ast::TopLevelField::Function {
        pos,
        container,
        name,
        args: args.into_boxed_slice(),
        return_type,
        prototype_pos,
        body,
    })
}

fn parse_block(
    tokens: &mut VecDeque<Token>,
    brace_pos: TokenPos,
) -> Result<ast::Block, CompileError> {
    let pos;
    let mut insts = Vec::new();
    loop {
        let token = get_token(tokens)?;
        match token.raw_tok {
            RawToken::BraceClose => {
                pos = brace_pos.extend(&token.pos);
                break;
            }
            RawToken::Let => insts.push(parse_let(tokens, token.pos)?),
            RawToken::If => {
                insts.push(parse_if(tokens, token.pos)?);
            }
            RawToken::While => {
                let condition = parse_expr(tokens)?;
                let block = match_token!((tokens, "'{'", brace_pos) {
                    RawToken::BraceOpen => parse_block(tokens, brace_pos)?,
                });
                let pos = token.pos.extend(&block.pos);

                insts.push(ast::Inst {
                    raw_inst: ast::RawInst::While { condition, block },
                    pos,
                });
            }
            RawToken::For => {
                let (index_ident, index_pos) = match_token!((tokens, "index ident", pos) {
                    RawToken::Literal(s) => (s, pos),
                });
                match_token!((tokens, "'->' or 'in'") {
                    RawToken::In => {
                        let start_expr = parse_expr(tokens)?;
                        match_token!((tokens, "','") {
                            RawToken::Comma => {}
                        });
                        let end_expr = parse_expr(tokens)?;

                        let block = match_token!((tokens, "'{'", brace_pos) {
                            RawToken::BraceOpen => parse_block(tokens, brace_pos)?,
                        });
                        let pos = token.pos.extend(&block.pos);

                        insts.push(ast::Inst {
                            raw_inst: ast::RawInst::ForRange {
                                index_pos,
                                index_ident,
                                start_expr,
                                end_expr,
                                block,
                            },
                            pos,
                        });
                    },
                    RawToken::ArrowRight => {
                        let (value_ident, value_pos) = match_token!((tokens, "value ident", pos) {
                            RawToken::Literal(s) => (s, pos),
                        });
                        match_token!((tokens, "'in'") {
                            RawToken::In => {}
                        });
                        let iter_type = match_token!((tokens, "'array' or 'map'") {
                            RawToken::Array => ast::ForIterType::Array,
                            RawToken::Map => ast::ForIterType::Map,
                        });
                        let iterator_expr = parse_expr(tokens)?;

                        let block = match_token!((tokens, "'{'", brace_pos) {
                            RawToken::BraceOpen => parse_block(tokens, brace_pos)?,
                        });
                        let pos = token.pos.extend(&block.pos);

                        insts.push(ast::Inst {
                            raw_inst: ast::RawInst::For {
                                index_pos,
                                index_ident,
                                value_pos,
                                value_ident,
                                iter_type,
                                iterator_expr,
                                block,
                            },
                            pos,
                        });
                    },
                });
            }
            RawToken::Return => {
                let expr = parse_expr(tokens)?;
                let end_pos = match_token!((tokens, "';'", end_pos) {
                    RawToken::Semicolon => end_pos,
                });
                let pos = token.pos.extend(&end_pos);

                insts.push(ast::Inst {
                    raw_inst: ast::RawInst::Return(expr),
                    pos,
                });
            }
            _ => {
                let first_token_pos = token.pos.clone();
                tokens.push_front(token);
                let expr = parse_expr(tokens)?;

                let raw_inst: ast::RawInst;
                let inst_pos: TokenPos;

                match_token!((tokens, "';' or '=' after expr", set_pos) {
                    RawToken::Set => {
                        let src_expr = parse_expr(tokens)?;

                        let end_pos = match_token!((tokens, "';'", end_pos) { RawToken::Semicolon => end_pos, });
                        inst_pos = first_token_pos.extend(&end_pos);

                        raw_inst = ast::RawInst::Assign(expr, src_expr);
                    },
                    RawToken::PlusSet => {
                        let src_expr = parse_expr(tokens)?;

                        let end_pos = match_token!((tokens, "';'", end_pos) { RawToken::Semicolon => end_pos, });
                        inst_pos = first_token_pos.extend(&end_pos);
                        let expr_pos = first_token_pos.extend(&src_expr.pos);

                        raw_inst = ast::RawInst::Assign(expr.clone(), crate::new_expr!(
                            Add(Box::new((expr, src_expr))),
                            expr_pos,
                        ));
                    },
                    RawToken::MinusSet => {
                        let src_expr = parse_expr(tokens)?;

                        let end_pos = match_token!((tokens, "';'", end_pos) { RawToken::Semicolon => end_pos, });
                        inst_pos = first_token_pos.extend(&end_pos);
                        let expr_pos = first_token_pos.extend(&src_expr.pos);

                        raw_inst = ast::RawInst::Assign(expr.clone(), crate::new_expr!(
                            Sub(Box::new((expr, src_expr))),
                            expr_pos,
                        ));
                    },
                    RawToken::MulSet => {
                        let src_expr = parse_expr(tokens)?;

                        let end_pos = match_token!((tokens, "';'", end_pos) { RawToken::Semicolon => end_pos, });
                        inst_pos = first_token_pos.extend(&end_pos);
                        let expr_pos = first_token_pos.extend(&src_expr.pos);

                        raw_inst = ast::RawInst::Assign(expr.clone(), crate::new_expr!(
                            Mul(Box::new((expr, src_expr))),
                            expr_pos,
                        ));
                    },
                    RawToken::DivSet => {
                        let src_expr = parse_expr(tokens)?;

                        let end_pos = match_token!((tokens, "';'", end_pos) { RawToken::Semicolon => end_pos, });
                        inst_pos = first_token_pos.extend(&end_pos);
                        let expr_pos = first_token_pos.extend(&src_expr.pos);

                        raw_inst = ast::RawInst::Assign(expr.clone(), crate::new_expr!(
                            Div(Box::new((expr, src_expr))),
                            expr_pos,
                        ));
                    },
                    RawToken::ModSet => {
                        let src_expr = parse_expr(tokens)?;

                        let end_pos = match_token!((tokens, "';'", end_pos) { RawToken::Semicolon => end_pos, });
                        inst_pos = first_token_pos.extend(&end_pos);
                        let expr_pos = first_token_pos.extend(&src_expr.pos);

                        raw_inst = ast::RawInst::Assign(expr.clone(), crate::new_expr!(
                            Mod(Box::new((expr, src_expr))),
                            expr_pos,
                        ));
                    },
                    RawToken::ConcatSet => {
                        let src_expr = parse_expr(tokens)?;

                        let end_pos = match_token!((tokens, "';'", end_pos) { RawToken::Semicolon => end_pos, });
                        inst_pos = first_token_pos.extend(&end_pos);
                        let expr_pos = first_token_pos.extend(&src_expr.pos);

                        raw_inst = ast::RawInst::Assign(expr.clone(), crate::new_expr!(
                            Concat(Box::new((expr, src_expr))),
                            expr_pos,
                        ));
                    },
                    RawToken::PowSet => {
                        let src_expr = parse_expr(tokens)?;

                        let end_pos = match_token!((tokens, "';'", end_pos) { RawToken::Semicolon => end_pos, });
                        inst_pos = first_token_pos.extend(&end_pos);
                        let expr_pos = first_token_pos.extend(&src_expr.pos);

                        raw_inst = ast::RawInst::Assign(expr.clone(), crate::new_expr!(
                            Pow(Box::new((expr, src_expr))),
                            expr_pos,
                        ));
                    },
                    RawToken::Semicolon => {
                        inst_pos = first_token_pos.extend(&set_pos);
                        raw_inst = ast::RawInst::Expr(expr);
                    },
                });
                insts.push(ast::Inst {
                    raw_inst,
                    pos: inst_pos,
                });
            }
        }
    }

    Ok(ast::Block {
        insts: insts.into_boxed_slice(),
        pos,
    })
}

fn parse_let(tokens: &mut VecDeque<Token>, let_pos: TokenPos) -> Result<ast::Inst, CompileError> {
    let pos;
    let mut is_mut = false;
    if consume_next_token_if_its(tokens, RawToken::Mut)?.is_some() {
        is_mut = true;
    }

    let name;
    match_token!((tokens, "binding name") {
        RawToken::Literal(s) => name = s,
    });

    let mut type_ = None;
    if consume_next_token_if_its(tokens, RawToken::Colon)?.is_some() {
        type_ = Some(parse_type(tokens)?);
    }

    let mut value = None;
    if consume_next_token_if_its(tokens, RawToken::Set)?.is_some() {
        value = Some(parse_expr(tokens)?);
    }

    match_token!((tokens, "':', '=' or ';' after 'let' value", semicolon_pos) {
        RawToken::Semicolon => pos = let_pos.extend(&semicolon_pos),
    });

    Ok(ast::Inst {
        raw_inst: ast::RawInst::Let {
            is_mut,
            name,
            type_,
            value,
        },
        pos,
    })
}

fn parse_if(tokens: &mut VecDeque<Token>, if_pos: TokenPos) -> Result<ast::Inst, CompileError> {
    let condition = Box::new(parse_expr(tokens)?);

    let then_block = match_token!((tokens, "'{' after condition", brace_pos) {
        RawToken::BraceOpen => parse_block(tokens, brace_pos)?,
    });
    let mut pos = if_pos.extend(&then_block.pos);

    let mut else_block = None;

    if let Some(_else_pos) = consume_next_token_if_its(tokens, RawToken::Else)? {
        match_token!((tokens, "'{' or 'if' after 'else'", if_brace_pos) {
            RawToken::If => {
                let elseif_inst = parse_if(tokens, if_brace_pos)?;
                let elseif_pos = elseif_inst.pos.clone();
                pos = pos.extend(&elseif_pos);

                else_block = Some(ast::Block {
                    insts: vec![
                        elseif_inst
                    ].into_boxed_slice(),
                    pos: elseif_pos,
                });
            },
            RawToken::BraceOpen => {
                let block = parse_block(tokens, if_brace_pos)?;
                pos = pos.extend(&block.pos);
                else_block = Some(block);
            },
        });
    }

    Ok(ast::Inst {
        raw_inst: ast::RawInst::If {
            condition,
            then_block,
            else_block,
        },
        pos,
    })
}

fn parse_expr(tokens: &mut VecDeque<Token>) -> Result<ast::Expr, CompileError> {
    parse_and_or_expr(tokens)
}

fn parse_and_or_expr(tokens: &mut VecDeque<Token>) -> Result<ast::Expr, CompileError> {
    let mut expr = parse_cmp_expr(tokens)?;

    loop {
        if consume_next_token_if_its(tokens, RawToken::And)?.is_some() {
            let right_side_expr = parse_cmp_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(And(Box::new((expr, right_side_expr))), pos);
            continue;
        }
        if consume_next_token_if_its(tokens, RawToken::Or)?.is_some() {
            let right_side_expr = parse_cmp_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(Or(Box::new((expr, right_side_expr))), pos);
            continue;
        }

        break;
    }

    Ok(expr)
}

fn parse_cmp_expr(tokens: &mut VecDeque<Token>) -> Result<ast::Expr, CompileError> {
    let mut expr = parse_add_sub_expr(tokens)?;

    loop {
        if consume_next_token_if_its(tokens, RawToken::Eq)?.is_some() {
            let right_side_expr = parse_add_sub_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(Eq(Box::new((expr, right_side_expr))), pos);
            continue;
        }
        if consume_next_token_if_its(tokens, RawToken::NotEq)?.is_some() {
            let right_side_expr = parse_add_sub_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(NotEq(Box::new((expr, right_side_expr))), pos);
            continue;
        }
        if consume_next_token_if_its(tokens, RawToken::Greater)?.is_some() {
            let right_side_expr = parse_add_sub_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(Greater(Box::new((expr, right_side_expr))), pos);
            continue;
        }
        if consume_next_token_if_its(tokens, RawToken::Lower)?.is_some() {
            let right_side_expr = parse_add_sub_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(Lower(Box::new((expr, right_side_expr))), pos);
            continue;
        }
        if consume_next_token_if_its(tokens, RawToken::GreaterEq)?.is_some() {
            let right_side_expr = parse_add_sub_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(GreaterEq(Box::new((expr, right_side_expr))), pos);
            continue;
        }
        if consume_next_token_if_its(tokens, RawToken::LowerEq)?.is_some() {
            let right_side_expr = parse_add_sub_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(LowerEq(Box::new((expr, right_side_expr))), pos);
            continue;
        }

        break;
    }

    Ok(expr)
}

fn parse_add_sub_expr(tokens: &mut VecDeque<Token>) -> Result<ast::Expr, CompileError> {
    let mut expr = parse_mul_div_mod_expr(tokens)?;

    loop {
        if consume_next_token_if_its(tokens, RawToken::Plus)?.is_some() {
            let right_side_expr = parse_mul_div_mod_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(Add(Box::new((expr, right_side_expr))), pos);
            continue;
        }
        if consume_next_token_if_its(tokens, RawToken::Minus)?.is_some() {
            let right_side_expr = parse_mul_div_mod_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(Sub(Box::new((expr, right_side_expr))), pos);
            continue;
        }
        if consume_next_token_if_its(tokens, RawToken::Concat)?.is_some() {
            let right_side_expr = parse_mul_div_mod_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(Concat(Box::new((expr, right_side_expr))), pos);
            continue;
        }

        break;
    }

    Ok(expr)
}

fn parse_mul_div_mod_expr(tokens: &mut VecDeque<Token>) -> Result<ast::Expr, CompileError> {
    let mut expr = parse_pow_expr(tokens)?;

    loop {
        if consume_next_token_if_its(tokens, RawToken::Mul)?.is_some() {
            let right_side_expr = parse_pow_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(Mul(Box::new((expr, right_side_expr))), pos);
            continue;
        }
        if consume_next_token_if_its(tokens, RawToken::Div)?.is_some() {
            let right_side_expr = parse_pow_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(Div(Box::new((expr, right_side_expr))), pos);
            continue;
        }
        if consume_next_token_if_its(tokens, RawToken::Mod)?.is_some() {
            let right_side_expr = parse_pow_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(Mod(Box::new((expr, right_side_expr))), pos);
            continue;
        }

        break;
    }

    Ok(expr)
}

fn parse_pow_expr(tokens: &mut VecDeque<Token>) -> Result<ast::Expr, CompileError> {
    let mut expr = parse_unary_op_expr(tokens)?;

    loop {
        if consume_next_token_if_its(tokens, RawToken::Pow)?.is_some() {
            let right_side_expr = parse_unary_op_expr(tokens)?;
            let pos = expr.pos.extend(&right_side_expr.pos);
            expr = new_expr!(Pow(Box::new((expr, right_side_expr))), pos);
            continue;
        }

        break;
    }

    Ok(expr)
}

fn parse_unary_op_expr(tokens: &mut VecDeque<Token>) -> Result<ast::Expr, CompileError> {
    if let Some(op_pos) = consume_next_token_if_its(tokens, RawToken::Not)? {
        let subexpr = parse_unary_op_expr(tokens)?;
        let pos = op_pos.extend(&subexpr.pos);
        return Ok(new_expr!(Not(Box::new(subexpr)), pos));
    }
    if let Some(op_pos) = consume_next_token_if_its(tokens, RawToken::Minus)? {
        let subexpr = parse_unary_op_expr(tokens)?;
        let pos = op_pos.extend(&subexpr.pos);
        return Ok(new_expr!(Minus(Box::new(subexpr)), pos));
    }
    parse_call_suffix(tokens)
}

fn parse_call_suffix(tokens: &mut VecDeque<Token>) -> Result<ast::Expr, CompileError> {
    let mut expr = parse_base_expr(tokens)?;

    loop {
        if let Some(paren_pos) = consume_next_token_if_its(tokens, RawToken::ParenOpen)? {
            let (args, arg_list_pos) = parse_arg_list(tokens, paren_pos)?;
            let pos = expr.pos.extend(&arg_list_pos);
            expr = new_expr!(
                FunctionCall {
                    function_expr: Box::new(expr),
                    args,
                },
                pos
            );
            continue;
        }

        if consume_next_token_if_its(tokens, RawToken::BracketOpen)?.is_some() {
            let index_expr = Box::new(parse_expr(tokens)?);

            let bracket_pos = match_token!((tokens, "']'", bracket_pos) {
                RawToken::BracketClose => bracket_pos,
            });

            let pos = expr.pos.extend(&bracket_pos);

            expr = new_expr!(
                Index {
                    container_expr: Box::new(expr),
                    index_expr,
                },
                pos
            );
            continue;
        }

        if consume_next_token_if_its(tokens, RawToken::Dot)?.is_some() {
            match_token!((tokens, "field name or '?'", field_pos) {
                RawToken::Literal(index) => {
                    let pos = expr.pos.extend(&field_pos);
                    expr = new_expr!(
                        StaticIndex {
                            container_expr: Box::new(expr),
                            index,
                        },
                        pos
                    );
                },
                RawToken::QuestionMark => {
                    let pos = expr.pos.extend(&field_pos);
                    expr = new_expr!(
                        UnwrapOptional(Box::new(expr)),
                        pos
                    );
                },
            });
            continue;
        }

        if consume_next_token_if_its(tokens, RawToken::Colon)?.is_some() {
            let function_name = match_token!((tokens, "method name") {
                RawToken::Literal(function_name) => function_name,
            });
            let paren_pos = match_token!((tokens, "'('", paren_pos) {
                RawToken::ParenOpen => paren_pos,
            });

            let (args, arg_list_pos) = parse_arg_list(tokens, paren_pos)?;

            let pos = expr.pos.extend(&arg_list_pos);

            expr = new_expr!(
                MethodCall {
                    object_expr: Box::new(expr),
                    function_name,
                    args,
                },
                pos
            );
            continue;
        }

        if consume_next_token_if_its(tokens, RawToken::As)?.is_some() {
            let type_ = parse_type(tokens)?;
            let pos = expr.pos.extend(
                type_
                    .pos
                    .as_ref()
                    .expect("types from parse_type should have a pos"),
            );

            expr = new_expr!(Cast(Box::new(expr), Box::new(type_)), pos);
            continue;
        }
        break;
    }

    Ok(expr)
}

fn parse_arg_list(
    tokens: &mut VecDeque<Token>,
    paren_open_pos: TokenPos,
) -> Result<(Box<[ast::Expr]>, TokenPos), CompileError> {
    let mut args = Vec::new();

    let pos;
    loop {
        if let Some(paren_pos) = consume_next_token_if_its(tokens, RawToken::ParenClose)? {
            pos = paren_open_pos.extend(&paren_pos);
            break;
        }
        args.push(parse_expr(tokens)?);

        match_token!((tokens, "',' or ')'", paren_pos) {
            RawToken::ParenClose => {
                pos = paren_open_pos.extend(&paren_pos);
                break;
            },
            RawToken::Comma => continue,
        });
    }

    Ok((args.into_boxed_slice(), pos))
}

fn parse_base_expr(tokens: &mut VecDeque<Token>) -> Result<ast::Expr, CompileError> {
    match_token!((tokens, "expression", token_pos) {
        RawToken::IntLiteral(i) => Ok(new_expr!(IntLiteral(i), token_pos)),
        RawToken::BoolLiteral(b) => Ok(new_expr!(BoolLiteral(b), token_pos)),
        RawToken::StringLiteral(s) => Ok(new_expr!(StringLiteral(s), token_pos)),
        RawToken::RealLiteral(f) => Ok(new_expr!(RealLiteral(f), token_pos)),
        RawToken::Literal(s) => Ok(new_expr!(Literal(s), token_pos)),
        RawToken::NilLiteral => Ok(new_expr!(NilLiteral, token_pos)),
        RawToken::New => {
            let class_type = Box::new(parse_type(tokens)?);

            let brace_pos = match_token!((tokens, "'{'", brace_pos) {
                RawToken::BraceOpen => brace_pos,
            });

            let (fields, table_pos) = parse_table_literal(tokens, brace_pos)?;
            let pos = token_pos.extend(&table_pos);
            Ok(new_expr!(New {
                class_type,
                fields,
            }, pos))
        },
        RawToken::BraceOpen => {
            let (fields, table_pos) = parse_table_literal(tokens, token_pos)?;
            Ok(new_expr!(TableLiteral { fields }, table_pos))
        },
        RawToken::BracketOpen => {
            let mut items = Vec::new();

            let pos;
            loop {
                if let Some(bracket_pos) = consume_next_token_if_its(tokens, RawToken::BracketClose)? {
                    pos = token_pos.extend(&bracket_pos);
                    break;
                }
                items.push(parse_expr(tokens)?);

                match_token!((tokens, "',' or ']'", bracket_pos) {
                    RawToken::BracketClose => {
                        pos = token_pos.extend(&bracket_pos);
                        break;
                    },
                    RawToken::Comma => {},
                })
            }

            Ok(new_expr!(ArrayLiteral(items), pos))
        },
        RawToken::ParenOpen => {
            let expr = parse_expr(tokens)?;
            match_token!((tokens, "')'") {
                RawToken::ParenClose => {}
            });
            Ok(expr)
        },
        RawToken::At => {
            let at_function = match_token!((tokens, "function name after '@'", literal_pos) {
                RawToken::Literal(s) => match s.as_str() {
                    "len" => ast::AtFunction::Len,
                    "abs" => ast::AtFunction::Abs,
                    "min" => ast::AtFunction::Min,
                    "max" => ast::AtFunction::Max,
                    "insert" => ast::AtFunction::Insert,
                    _ => return Err(CompileError::new(
                        format!("invalid @ function name : '{}'", s),
                        Some(literal_pos),
                    )),
                }
            });
            let paren_open_pos = match_token!((tokens, "'('", paren_open_pos) {
                RawToken::ParenOpen => paren_open_pos,
            });
            let (args, arg_list_pos) = parse_arg_list(tokens, paren_open_pos)?;
            let pos = token_pos.extend(&arg_list_pos);
            Ok(new_expr!(AtCall{
                at_function,
                args,
            }, pos))
        }
    })
}

fn parse_table_literal(
    tokens: &mut VecDeque<Token>,
    brace_pos: TokenPos,
) -> Result<(BTreeMap<String, ast::Expr>, TokenPos), CompileError> {
    let mut fields = BTreeMap::new();
    let pos;

    loop {
        if let Some(token_pos) = consume_next_token_if_its(tokens, RawToken::BraceClose)? {
            pos = brace_pos.extend(&token_pos);
            break;
        }
        let field_name = match_token!((tokens, "field name") {
            RawToken::Literal(field_name) => field_name,
        });
        match_token!((tokens, "'='") {
            RawToken::Set => {},
        });

        let field_expr = parse_expr(tokens)?;
        fields.insert(field_name, field_expr);

        match_token!((tokens, "',' or '}'", token_pos) {
            RawToken::Comma => {},
            RawToken::BraceClose => {
                pos = brace_pos.extend(&token_pos);
                break;
            }
        });
    }

    Ok((fields, pos))
}

fn parse_type(tokens: &mut VecDeque<Token>) -> Result<ast::Type, CompileError> {
    let base_type = match_token!((tokens, "type", pos) {
        RawToken::Int => new_type!(Int, pos),
        RawToken::Real => new_type!(Real, pos),
        RawToken::Bool => new_type!(Bool, pos),
        RawToken::String => new_type!(String, pos),
        RawToken::Anything => new_type!(Anything, pos),
        RawToken::Never => new_type!(Never, pos),
        RawToken::Literal(s) => new_type!(Ident(s), pos),
        RawToken::BracketOpen => {
            let child_type = parse_type(tokens)?;
            match_token!((tokens, "']'") {
                RawToken::BracketClose => {},
            });
            new_type!(Array(Box::new(child_type)), pos)
        },
        RawToken::Fn => new_type!(parse_fn_type(tokens)?, pos),
        RawToken::Struct => {
            let (body, mut body_pos) = parse_struct_body(tokens)?;
            body_pos = body_pos.extend(&pos);
            new_type!(Struct(body), body_pos)
        },
        RawToken::Enum => {
            let mut values = std::collections::BTreeSet::new();
            match_token!((tokens, "{ after 'enum'") {
                RawToken::BraceOpen => {}
            });
            while consume_next_token_if_its(tokens, RawToken::BraceClose)?.is_none() {
                match_token!((tokens, "enum value") {
                    RawToken::StringLiteral(s) => {
                        values.insert(s);
                    }
                });
                match_token!((tokens, "',' or '}' after enum value") {
                    RawToken::BraceClose => break,
                    RawToken::Comma => {}
                });
            }
            new_type!(Enum(values), pos)
        },
        RawToken::QuestionMark => {
            let child_type = parse_type(tokens)?;
            new_type!(Optional(Box::new(child_type)), pos)
        },
        RawToken::Map => {
            match_token!((tokens, "( after 'map'") {
                RawToken::ParenOpen => {}
            });
            let key_type = parse_type(tokens)?;
            match_token!((tokens, "-> after map key") {
                RawToken::ArrowRight => {}
            });
            let value_type = parse_type(tokens)?;
            match_token!((tokens, ") after map value") {
                RawToken::ParenClose => {}
            });
            new_type!(Map(Box::new((
                key_type,
                value_type,
            ))), pos)
        }
    });

    Ok(base_type)
}

fn parse_fn_type(tokens: &mut VecDeque<Token>) -> Result<ast::RawType, CompileError> {
    match_token!((tokens, "(") {
        RawToken::ParenOpen => {}
    });

    let mut args = Vec::new();

    loop {
        if consume_next_token_if_its(tokens, RawToken::ParenClose)?.is_some() {
            break;
        }
        args.push(parse_type(tokens)?);

        match_token!((tokens, "',' or ')'") {
            RawToken::ParenClose => break,
            RawToken::Comma => continue,
        });
    }

    let mut return_type = None;

    if consume_next_token_if_its(tokens, RawToken::ArrowRight)?.is_some() {
        return_type = Some(Box::new(parse_type(tokens)?));
    }

    Ok(ast::RawType::Callable {
        args: args.into_boxed_slice(),
        return_type,
    })
}

fn get_token(tokens: &mut VecDeque<Token>) -> Result<Token, CompileError> {
    tokens.pop_front().ok_or_else(|| {
        CompileError::new(
            "expected token, got EOF".to_string(),
            None, // TODO
        )
    })
}

fn consume_next_token_if_its(
    tokens: &mut VecDeque<Token>,
    expected_token: RawToken,
) -> Result<Option<TokenPos>, CompileError> {
    let obtained_token = get_token(tokens)?;
    if obtained_token.raw_tok == expected_token {
        Ok(Some(obtained_token.pos))
    } else {
        tokens.push_front(obtained_token);
        Ok(None)
    }
}

fn unexpected_token_error(tok: Token, expected: &str) -> CompileError {
    CompileError::new(
        format!(
            "unexpected token '{:?}', expected {}",
            tok.raw_tok, expected
        ),
        Some(tok.pos),
    )
}
