use std::collections::{BTreeMap, BTreeSet, HashMap, HashSet};
use std::path::PathBuf;
use std::rc::Rc;

use crate::ast::{self, Expr, RawExpr, RawType, Type};
use crate::CompileError;

pub fn compile(asts: Vec<(PathBuf, ast::Ast)>) -> Result<HashMap<PathBuf, String>, CompileError> {
    let mut context = CompilationContext {
        // global scope
        scope_stack: vec![Scope::default()],
        function_return_type_stack: Vec::new(),
    };

    for (_file_name, ast) in &asts {
        ast_populate_context(&mut context, &ast)?;
        assert_eq!(context.scope_stack.len(), 1);
        assert_eq!(context.function_return_type_stack.len(), 0);
    }

    for (_file_name, ast) in &asts {
        ast_check_soundness(&mut context, &ast)?;
    }

    Ok(asts
        .into_iter()
        .map(|(mut filename, content)| {
            Ok({
                filename.set_extension("lua");
                (filename, crate::transcriber::transcribe_ast(&content)?)
            })
        })
        .collect::<Result<_, _>>()?)
}

#[allow(dead_code)]
pub fn compile_memory_files(
    files: &[(&str, &str)],
) -> Result<HashMap<PathBuf, String>, CompileError> {
    let asts: Vec<(PathBuf, crate::ast::Ast)> = files
        .iter()
        .map(|(filename, content)| {
            Ok((
                PathBuf::from(filename),
                crate::parser::parse(content, Some(Rc::new(filename.to_string())))?,
            ))
        })
        .collect::<Result<_, CompileError>>()?;

    compile(asts)
}

pub fn compile_real_files(
    start_file: impl AsRef<std::path::Path>,
) -> Result<HashMap<PathBuf, String>, CompileError> {
    let mut asts: Vec<(PathBuf, crate::ast::Ast)> = Vec::new();

    fn add_ast(
        asts: &mut Vec<(PathBuf, crate::ast::Ast)>,
        file_path: &std::path::Path,
        pos: Option<crate::TokenPos>,
    ) -> Result<(), CompileError> {
        let file_content = match std::fs::read_to_string(file_path) {
            Ok(file_content) => file_content,
            Err(io_error) => {
                return Err(CompileError::new(
                    format!(
                        "could not read file '{}' : {}",
                        file_path.to_string_lossy(),
                        io_error
                    ),
                    pos,
                ));
            }
        };
        let ast = crate::parser::parse(
            &file_content,
            Some(Rc::new(file_path.to_string_lossy().to_string())),
        )?;

        let paths_to_require = ast
            .iter()
            .filter_map(|top_level_field| match top_level_field {
                ast::TopLevelField::Require { pos, path } => {
                    let parent = match file_path.parent() {
                        Some(parent) => parent,
                        None => {
                            return Some(Err(CompileError::new(
                                "require path is root".to_string(),
                                Some(pos.clone()),
                            )))
                        }
                    };
                    let mut path_buf = parent.to_path_buf();
                    path_buf.push(path);

                    if !asts.iter().any(|(path, _ast)| path == &path_buf) {
                        Some(Ok((path_buf, pos.clone())))
                    } else {
                        None
                    }
                }
                _ => None,
            })
            .collect::<Result<Vec<_>, CompileError>>()?;

        for (path, pos) in paths_to_require {
            add_ast(asts, &path, Some(pos))?;
        }

        asts.push((file_path.to_path_buf(), ast));

        Ok(())
    };

    add_ast(&mut asts, start_file.as_ref(), None)?;

    compile(asts)
}

#[derive(Debug)]
pub struct CompilationContext {
    scope_stack: Vec<Scope>,
    function_return_type_stack: Vec<Type>,
}

#[derive(Debug, Default)]
pub struct Scope {
    decls: HashMap<String, Decl>,

    initialized_decls_in_this_scope: BTreeSet<String>,

    checks: HashSet<ast::Check>,
}

#[derive(Debug)]
pub struct Decl {
    is_mut: bool,
    type_: Type,
    is_init: bool,
}

impl CompilationContext {
    pub fn get(&self, name: &str) -> Option<&Decl> {
        for scope in self.scope_stack.iter().rev() {
            if let Some(decl) = scope.decls.get(name) {
                return Some(decl);
            }
        }
        None
    }

    pub fn get_type(
        &self,
        name: &str,
        pos: Option<&crate::TokenPos>,
    ) -> Result<Type, CompileError> {
        // container_name: &str,
        // function_name: &str,
        // function_type: Type,

        for scope in self.scope_stack.iter().rev() {
            if let Some(decl) = scope.decls.get(name) {
                match &decl.type_.raw_type {
                    RawType::ClassObject(class) => {
                        return Ok(Type {
                            raw_type: RawType::ClassInstance(class.clone()),
                            pos: decl.type_.pos.clone(),
                        })
                    }
                    RawType::Ident(_) => unreachable!(),
                    _ => {
                        return Err(CompileError::new(
                            format!("'{}' is not a type", name),
                            pos.cloned(),
                        ))
                    }
                }
            }
        }
        Err(CompileError::new(
            format!("unknown type '{}'", name),
            pos.cloned(),
        ))
    }

    pub fn is_initialized(&self, name: &str) -> bool {
        if self
            .latest_scope()
            .initialized_decls_in_this_scope
            .contains(name)
        {
            return true;
        }
        self.get(name)
            .expect("is_initialized should only be called for existing variables")
            .is_init
    }

    pub fn mark_as_initialized(&mut self, name: &str) {
        let latest_scope = self.latest_scope_mut();

        if let Some(decl) = latest_scope.decls.get_mut(name) {
            decl.is_init = true;
        } else {
            latest_scope
                .initialized_decls_in_this_scope
                .insert(name.to_string());
        }
    }

    fn latest_scope(&self) -> &Scope {
        &self.scope_stack[self.scope_stack.len() - 1]
    }

    fn latest_scope_mut(&mut self) -> &mut Scope {
        let len = self.scope_stack.len();
        &mut self.scope_stack[len - 1]
    }

    pub fn iter_checks(&self) -> impl Iterator<Item = &ast::Check> {
        self.scope_stack
            .iter()
            .map(|scope| scope.checks.iter())
            .flatten()
    }

    pub fn insert_into_latest_scope(
        &mut self,
        name: String,
        decl: Decl,
        pos: &crate::TokenPos,
    ) -> Result<(), CompileError> {
        match self.latest_scope_mut().decls.entry(name) {
            std::collections::hash_map::Entry::Occupied(occupied) => Err(CompileError::new(
                format!("value '{}' already exists", occupied.key()),
                Some(pos.clone()),
            )),
            std::collections::hash_map::Entry::Vacant(vacant) => {
                vacant.insert(decl);
                Ok(())
            }
        }
    }

    pub fn push_scope(&mut self) {
        self.scope_stack.push(Scope::default());
    }

    pub fn push_scope_with_checks(&mut self, checks: HashSet<ast::Check>) {
        self.scope_stack.push(Scope {
            decls: HashMap::new(),
            checks,
            initialized_decls_in_this_scope: BTreeSet::new(),
        });
    }

    pub fn pop_scope(&mut self) -> Scope {
        self.scope_stack
            .pop()
            .expect("a scope should have been pushed beforehand")
    }

    pub fn resolve_type(&self, orig_type: &ast::Type) -> Result<ast::Type, CompileError> {
        match &orig_type.raw_type {
            ast::RawType::Ident(name) => Ok(self.get_type(name, orig_type.pos.as_ref())?),
            _ => Ok(orig_type.clone()),
        }
    }

    /// return None, or a reason why this type cannot be included.
    pub fn is_type_included_in(
        &self,
        expected_type: &Type,
        gotten_type: &Type,
    ) -> Result<Option<TypeInclusionError>, CompileError> {
        use RawType::*;

        Ok(match (&expected_type.raw_type, &gotten_type.raw_type) {
            (_, Never) => None,
            (Anything, _) => None,
            (Never, _) => Some(TypeInclusionError::ExpectedNever),
            (_, Anything) => Some(TypeInclusionError::GotAnything),

            (Real, Int) => None,
            (Optional(_), Nil) => None,
            (Optional(expected_child), Optional(gotten_child)) => {
                self.is_type_included_in(expected_child, gotten_child)?
            }
            (Optional(expected_child), _) => {
                self.is_type_included_in(expected_child, gotten_type)?
            }

            (Ident(ident), _) => self.is_type_included_in(
                &self.get_type(&ident, expected_type.pos.as_ref())?,
                gotten_type,
            )?,
            (_, Ident(ident)) => self.is_type_included_in(
                expected_type,
                &self.get_type(&ident, gotten_type.pos.as_ref())?,
            )?,

            (Array(_), EmptyArray) => None,

            (String, Enum(_)) => None,
            (Enum(expected_set), Enum(gotten_set)) => {
                if !expected_set.is_superset(gotten_set) {
                    Some(TypeInclusionError::MissingValuesInEnum {
                        missing_values: gotten_set
                            .difference(expected_set)
                            .cloned()
                            .collect::<Vec<_>>(),
                    })
                    // Some(format!("type '{}' does not have values {}",
                    //     expected_type,
                    //     gotten_set.difference(expected_set)
                    //         .map(|it| it.as_str())
                    //         .collect::<Vec<_>>()
                    //         .join(", "),
                    // ))
                } else {
                    None
                }
            }

            /*
                TODO : If trying to fit a struct with missing fields into a struct
                where those fields are optional, only allow this if the struct come
                from a "const" place, like a literal.
                Actually, do that for every "table" object (like maps), except classes.
            */
            (Struct(expected_struct), Struct(gotten_struct)) => {
                match self.can_fields_be_put_in_struct_body(expected_struct, gotten_struct)? {
                    Some(struct_body_error) => {
                        Some(TypeInclusionError::StructBodyError(struct_body_error))
                    }
                    None => None,
                }
            }
            (Map(boxed), Struct(gotten_struct)) => {
                let (key, value) = boxed.as_ref();

                for (field_name, gotten_field_type) in gotten_struct.get_effective_fields(self) {
                    let field_name_as_type =
                        crate::new_type!(Enum(vec![field_name].into_iter().cloned().collect()));
                    let expected_field_type: &Type = if self
                        .is_type_included_in(key, &field_name_as_type)?
                        .is_none()
                    {
                        value
                    } else {
                        &crate::new_type!(Nil)
                    };
                    if let Some(reason) =
                        self.is_type_included_in(&expected_field_type, &gotten_field_type)?
                    {
                        return Ok(Some(reason));
                    }
                }

                None
            }

            _ => {
                if self.are_types_equals(expected_type, gotten_type)? {
                    None
                } else {
                    Some(TypeInclusionError::TypeUnequals)
                }
            }
        })
    }

    fn can_fields_be_put_in_struct_body(
        &self,
        expected_struct: &ast::StructBody,
        gotten_struct: &ast::StructBody,
    ) -> Result<Option<StructBodyError>, CompileError> {
        for (field_name, gotten_field_type) in gotten_struct.get_effective_fields(self) {
            let expected_field_type = expected_struct.get_field(field_name, self)?;
            let expected_field_type_or_nil = expected_field_type
                .as_ref()
                .unwrap_or(&crate::new_type!(Nil));
            if let Some(type_inclusion_error) =
                self.is_type_included_in(expected_field_type_or_nil, &gotten_field_type)?
            {
                if let Some(expected_field_type) = expected_field_type {
                    return Ok(Some(StructBodyError::IncorrectFieldType {
                        expected_field_type: Box::new(expected_field_type),
                        gotten_field_type: Box::new(gotten_field_type),
                        field_name: field_name.clone(),
                        type_inclusion_error: Box::new(type_inclusion_error),
                    }));
                } else {
                    return Ok(Some(StructBodyError::UnknownField {
                        field_name: field_name.clone(),
                    }));
                }
            }
        }

        for (field_name, expected_field_type) in expected_struct.get_effective_fields(self) {
            let gotten_field_type = gotten_struct.get_field(field_name, self)?;
            let gotten_field_type_or_nil =
                gotten_field_type.as_ref().unwrap_or(&crate::new_type!(Nil));
            if let Some(type_inclusion_error) =
                self.is_type_included_in(&expected_field_type, gotten_field_type_or_nil)?
            {
                if let Some(gotten_field_type) = gotten_field_type {
                    return Ok(Some(StructBodyError::IncorrectFieldType {
                        expected_field_type: Box::new(expected_field_type),
                        gotten_field_type: Box::new(gotten_field_type),
                        field_name: field_name.clone(),
                        type_inclusion_error: Box::new(type_inclusion_error),
                    }));
                } else {
                    return Ok(Some(StructBodyError::MissingField {
                        expected_field_type: Box::new(expected_field_type),
                        field_name: field_name.clone(),
                    }));
                }
            }
        }

        if let Some(expected_union) = &expected_struct.union_ {
            if let Some(_gotten_union) = &gotten_struct.union_ {
                todo!();
            } else {
                let gotten_kind_field_type =
                    gotten_struct.get_field_or_nil(&expected_union.kind_field, self)?;
                let gotten_kind_set = match &gotten_kind_field_type.raw_type {
                    RawType::Enum(set) => set,
                    _ => {
                        return Ok(Some(StructBodyError::IncorrectFieldType {
                            expected_field_type: Box::new(expected_union.get_kind_type()),
                            gotten_field_type: Box::new(gotten_kind_field_type.clone()),
                            field_name: expected_union.kind_field.clone(),
                            type_inclusion_error: Box::new(
                                TypeInclusionError::IncorrectUnionKindType {
                                    kind_field: expected_union.kind_field.clone(),
                                    gotten_field_type: gotten_kind_field_type.clone(),
                                },
                            ),
                        }))
                    }
                };
                let possible_variants_are_valids: Option<StructBodyError> = expected_union
                    .variants
                    .iter()
                    .filter(|(variant_name, _variant)| {
                        gotten_kind_set.contains(variant_name.as_str())
                    })
                    .map(|(_variant_name, variant)| -> Result<Option<_>, _> {
                        for (field_name, expected_type) in variant {
                            let gotten_field_type = gotten_struct.get_field(field_name, self)?;
                            let gotten_field_type_or_nil =
                                gotten_field_type.as_ref().unwrap_or(&crate::new_type!(Nil));
                            if let Some(type_inclusion_error) =
                                self.is_type_included_in(&expected_type, gotten_field_type_or_nil)?
                            {
                                if let Some(gotten_field_type) = gotten_field_type {
                                    return Ok(Some(StructBodyError::IncorrectFieldType {
                                        expected_field_type: Box::new(expected_type.clone()),
                                        gotten_field_type: Box::new(gotten_field_type),
                                        field_name: field_name.clone(),
                                        type_inclusion_error: Box::new(type_inclusion_error),
                                    }));
                                } else {
                                    return Ok(Some(StructBodyError::MissingField {
                                        expected_field_type: Box::new(expected_type.clone()),
                                        field_name: field_name.clone(),
                                    }));
                                }
                            }
                        }
                        Ok(None)
                    })
                    .filter_map(|it| it.transpose())
                    .next()
                    .transpose()?;
                // .try_fold(None, |acc, (_variant_name, variant)| {
                //     for (field_name, expected_type) in variant {
                //         let gotten_field_type =
                //             gotten_struct.get_field_or_nil(field_name, self)?;
                //         if !self
                //             .is_type_included_in(&expected_type, &gotten_field_type)?
                //         {
                //             return Ok(false);
                //         }
                //     }
                //     Ok(acc)
                // })?;
                if let Some(struct_body_error) = possible_variants_are_valids {
                    return Ok(Some(struct_body_error));
                }
            }
        }

        Ok(None)
    }

    pub fn types_can_be_compared_for_equality(
        &self,
        type_a: &Type,
        type_b: &Type,
    ) -> Result<bool, CompileError> {
        use RawType::*;

        Ok(match (&type_a.raw_type, &type_b.raw_type) {
            (_, Anything) | (Anything, _) => true,
            (Int, Real) | (Real, Int) => true,
            (Optional(_), Nil) | (Nil, Optional(_)) => true,
            (Optional(child_a), Optional(child_b)) => {
                self.types_can_be_compared_for_equality(child_a, child_b)?
            }
            (Optional(child_a), _) => self.types_can_be_compared_for_equality(child_a, type_b)?,
            (_, Optional(child_b)) => self.types_can_be_compared_for_equality(type_a, child_b)?,

            (Ident(ident), _) => self.types_can_be_compared_for_equality(
                &self.get_type(&ident, type_a.pos.as_ref())?,
                type_b,
            )?,
            (_, Ident(ident)) => self.types_can_be_compared_for_equality(
                type_a,
                &self.get_type(&ident, type_b.pos.as_ref())?,
            )?,

            (String, Enum(_)) | (Enum(_), String) => true,
            (Enum(values_a), Enum(values_b)) => values_a.intersection(values_b).next().is_some(),

            (Struct(_), Struct(_)) => true,

            _ => self.are_types_equals(type_a, type_b)?,
        })
    }

    pub fn get_type_union(&self, type_a: &Type, type_b: &Type) -> Result<Type, CompileError> {
        if self.are_types_equals(type_a, type_b)? {
            return Ok(type_a.clone());
        }

        use crate::new_type;
        use RawType::*;

        Ok(match (&type_a.raw_type, &type_b.raw_type) {
            (_, Anything) | (Anything, _) => new_type!(Anything),
            (_, Never) => type_a.clone(),
            (Never, _) => type_b.clone(),

            (Int, Real) | (Real, Int) => new_type!(Real),

            (Optional(_), Nil) => type_a.clone(),
            (Nil, Optional(_)) => type_b.clone(),
            (Optional(subtype_a), Optional(subtype_b)) => {
                new_type!(Optional(Box::new(
                    self.get_type_union(&subtype_a, &subtype_b)?
                )))
            }
            (Optional(subtype), _) => {
                new_type!(Optional(Box::new(self.get_type_union(&subtype, type_b)?)))
            }
            (_, Optional(subtype)) => {
                new_type!(Optional(Box::new(self.get_type_union(type_a, &subtype)?)))
            }
            (_, Nil) => new_type!(Optional(Box::new(type_a.clone()))),
            (Nil, _) => new_type!(Optional(Box::new(type_b.clone()))),

            (String, Enum(_)) | (Enum(_), String) => new_type!(String),
            (Enum(values_a), Enum(values_b)) => {
                new_type!(Enum(values_a.union(values_b).cloned().collect()))
            }

            (EmptyArray, Array(child)) | (Array(child), EmptyArray) => {
                new_type!(Array(child.clone()))
            }

            (
                Struct(ast::StructBody {
                    fields: _fields_a,
                    union_: Some(_),
                }),
                Struct(ast::StructBody {
                    fields: _fields_b,
                    union_: Some(_),
                }),
            ) => todo!(),

            (Struct(struct_a), Struct(struct_b)) => {
                let mut struct_union = Vec::new();

                for (field_name, field_type_a) in struct_a.get_effective_fields(self) {
                    let field_type_b = struct_b.get_field(field_name, self)?;
                    let field_type_b = field_type_b.unwrap_or(new_type!(Nil));
                    struct_union.push((
                        field_name.clone(),
                        self.get_type_union(&field_type_a, &field_type_b)?,
                    ));
                }

                for (field_name, field_type_b) in struct_b.get_effective_fields(self) {
                    if struct_a.get_field(field_name, self)?.is_none() {
                        struct_union.push((field_name.clone(), field_type_b.with_nil()));
                    }
                }

                new_type!(Struct(ast::StructBody {
                    fields: struct_union.into_iter().collect(),
                    union_: None,
                }))
            }
            (Map(map_boxed), Struct(struct_body)) | (Struct(struct_body), Map(map_boxed)) => {
                let (map_key, map_value) = map_boxed.as_ref();

                new_type!(Map(Box::new((
                    self.get_type_union(&map_key, &new_type!(struct_body.get_key_type()))?,
                    self.get_type_union(&map_value, &self.get_union_of_all_fields(struct_body)?)?,
                ))))
            }

            (Ident(ident), _) => {
                self.get_type_union(&self.get_type(&ident, type_a.pos.as_ref())?, type_b)?
            }
            (_, Ident(ident)) => {
                self.get_type_union(type_a, &self.get_type(&ident, type_b.pos.as_ref())?)?
            }

            _ => new_type!(Anything),
        })
    }

    // Basically do what the derived PartialEq would have done, but ignore the TokenPos in the types. Also, resolve types.
    pub fn are_types_equals(&self, a: &ast::Type, b: &ast::Type) -> Result<bool, CompileError> {
        use ast::RawType::*;
        Ok(match (&a.raw_type, &b.raw_type) {
            (Int, Int)
            | (Real, Real)
            | (Bool, Bool)
            | (String, String)
            | (Anything, Anything)
            | (Never, Never)
            | (EmptyArray, EmptyArray)
            | (Nil, Nil) => true,
            (
                Callable {
                    args: args_a,
                    return_type: None,
                },
                Callable {
                    args: args_b,
                    return_type: None,
                },
            ) => {
                if args_a.len() != args_b.len() {
                    return Ok(false);
                }
                for (arg_a, arg_b) in args_a.iter().zip(args_b.iter()) {
                    if !self.are_types_equals(arg_a, arg_b)? {
                        return Ok(false);
                    }
                }
                true
            }
            (
                Callable {
                    args: args_a,
                    return_type: Some(rt_a),
                },
                Callable {
                    args: args_b,
                    return_type: Some(rt_b),
                },
            ) if self.are_types_equals(&rt_a, &rt_b)? => {
                if args_a.len() != args_b.len() {
                    return Ok(false);
                }
                for (arg_a, arg_b) in args_a.iter().zip(args_b.iter()) {
                    if !self.are_types_equals(arg_a, arg_b)? {
                        return Ok(false);
                    }
                }
                true
            }
            (Optional(sub_a), Optional(sub_b)) | (Array(sub_a), Array(sub_b)) => {
                self.are_types_equals(sub_a, sub_b)?
            }
            (Map(boxed_a), Map(boxed_b)) => {
                self.are_types_equals(&boxed_a.0, &boxed_b.0)?
                    && self.are_types_equals(&boxed_a.1, &boxed_b.1)?
            }

            (Ident(name_a), _) => {
                self.are_types_equals(&self.get_type(name_a, a.pos.as_ref())?, b)?
            }
            (_, Ident(name_b)) => {
                self.are_types_equals(a, &self.get_type(name_b, b.pos.as_ref())?)?
            }

            (ClassInstance(class_a), ClassInstance(class_b))
            | (ClassObject(class_a), ClassObject(class_b)) => {
                if class_a.name != class_b.name {
                    return Ok(false);
                }
                // TODO :
                // pub body: StructBody,
                // pub functions: BTreeMap<String, Type>,
                true
            }
            (
                Struct(ast::StructBody {
                    fields: fields_a,
                    union_: union_a,
                }),
                Struct(ast::StructBody {
                    fields: fields_b,
                    union_: union_b,
                }),
            ) => {
                fields_a.iter().try_fold(true, |acc, (key, a_val)| {
                    Ok(acc
                        && fields_b
                            .get(key)
                            .map(|b_val| self.are_types_equals(a_val, b_val))
                            .unwrap_or(Ok(false))?)
                })? && fields_b.iter().try_fold(true, |acc, (key, b_val)| {
                    Ok(acc
                        && fields_a
                            .get(key)
                            .map(|a_val| self.are_types_equals(a_val, b_val))
                            .unwrap_or(Ok(false))?)
                })? && match (union_a, union_b) {
                    (
                        Some(ast::Union {
                            kind_field: kind_field_a,
                            activated_variant: activated_variant_a,
                            variants: variants_a,
                        }),
                        Some(ast::Union {
                            kind_field: kind_field_b,
                            activated_variant: activated_variant_b,
                            variants: variants_b,
                        }),
                    ) => {
                        if kind_field_a != kind_field_b {
                            return Ok(false);
                        }
                        if activated_variant_a != activated_variant_b {
                            return Ok(false);
                        }

                        for (variant_name, variant_a) in variants_a {
                            if let Some(variant_b) = variants_b.get(variant_name) {
                                for (field_name, field_type_a) in variant_a {
                                    if let Some(field_type_b) = variant_b.get(field_name) {
                                        if !self.are_types_equals(field_type_a, field_type_b)? {
                                            return Ok(false);
                                        }
                                    } else {
                                        return Ok(false);
                                    }
                                }
                                for field_name in variant_b.keys() {
                                    if variant_a.get(field_name).is_none() {
                                        return Ok(false);
                                    }
                                }
                            } else {
                                return Ok(false);
                            }
                        }
                        for variant_name in variants_b.keys() {
                            if variants_a.get(variant_name).is_none() {
                                return Ok(false);
                            }
                        }

                        // variants_a
                        //     .iter()
                        //     .all(|(variant_name, variant)|)
                        // todo!()

                        true
                    }
                    (Some(_), None) | (None, Some(_)) => false,
                    (None, None) => true,
                }
            }
            // Struct(StructBody),
            (Enum(set_a), Enum(set_b)) => set_a == set_b,

            _ => false,
        })
    }

    pub fn get_union_of_all_fields(
        &self,
        struct_body: &ast::StructBody,
    ) -> Result<Type, CompileError> {
        struct_body
            .get_effective_fields(self)
            .try_fold(crate::new_type!(Never), |acc, (_field_name, field_type)| {
                self.get_type_union(&acc, &field_type)
            })
    }
}

#[derive(Debug)]
pub enum StructBodyError {
    MissingField {
        expected_field_type: Box<Type>,
        field_name: String,
    },
    IncorrectFieldType {
        field_name: String,
        expected_field_type: Box<Type>,
        gotten_field_type: Box<Type>,
        type_inclusion_error: Box<TypeInclusionError>,
    },
    UnknownField {
        field_name: String,
    },
}

impl StructBodyError {
    pub fn get_main_message(&self, expected_type: &Type, received_type: &Type) -> String {
        match self {
            StructBodyError::MissingField {
                field_name,
                expected_field_type,
            } => {
                format!(
                    "field '{}' (type: {}) is missing from {}",
                    field_name, expected_field_type, received_type
                )
            }
            StructBodyError::UnknownField { field_name } => {
                format!("Unknown field \'{}\' in type {}", field_name, expected_type)
            }
            StructBodyError::IncorrectFieldType {
                field_name,
                expected_field_type,
                gotten_field_type,
                type_inclusion_error: _,
            } => {
                format!(
                    "expected field '{}' to be {}, got {}",
                    field_name, expected_field_type, gotten_field_type
                )
            }
        }
    }

    pub fn get_sub_notes(&self) -> Vec<String> {
        match self {
            StructBodyError::MissingField { .. } | StructBodyError::UnknownField { .. } => {
                Vec::new()
            }
            StructBodyError::IncorrectFieldType {
                field_name: _,
                expected_field_type,
                gotten_field_type,
                type_inclusion_error,
            } => type_inclusion_error.get_notes(expected_field_type, gotten_field_type),
        }
    }

    pub fn get_notes(&self, expected_type: &Type, received_type: &Type) -> Vec<String> {
        let mut notes = self.get_sub_notes();
        notes.insert(0, self.get_main_message(expected_type, received_type));
        notes
    }
}

#[derive(Debug)]
pub enum TypeInclusionError {
    GotAnything,
    ExpectedNever,
    StructBodyError(StructBodyError),
    MissingValuesInEnum {
        missing_values: Vec<String>,
    },
    TypeUnequals,
    IncorrectUnionKindType {
        kind_field: String,
        gotten_field_type: Type,
    },
}

impl TypeInclusionError {
    pub fn get_notes(&self, expected_type: &Type, received_type: &Type) -> Vec<String> {
        match self {
            TypeInclusionError::GotAnything => {
                vec!["received type is 'anything', use a cast (a as type) to narrow it".to_string()]
            }
            TypeInclusionError::ExpectedNever => {
                vec!["'never' type cannot be obtained from any other type".to_string()]
            }
            TypeInclusionError::StructBodyError(struct_body_error) => {
                struct_body_error.get_notes(expected_type, received_type)
            }
            TypeInclusionError::MissingValuesInEnum { missing_values } => vec![format!(
                "type '{}' does not have values {}",
                expected_type,
                missing_values.join(", "),
            )],
            TypeInclusionError::TypeUnequals => Vec::new(),
            TypeInclusionError::IncorrectUnionKindType {
                kind_field,
                gotten_field_type,
            } => vec![format!(
                "field '{}' is an union enum, cannot be initialized with type {}",
                kind_field, gotten_field_type,
            )],
        }
    }
}

fn ast_populate_context(
    context: &mut CompilationContext,
    ast: &ast::Ast,
) -> Result<(), CompileError> {
    use ast::TopLevelField::*;
    for top_level_field in ast.iter() {
        match top_level_field {
            Function {
                pos: _,
                container,
                name,
                args,
                return_type,
                prototype_pos,
                body: _,
            } => {
                let mut function_args: Vec<Type> = args
                    .iter()
                    .map(|(_arg_name, arg_type, _is_mut)| arg_type.clone())
                    .collect();

                match container {
                    ast::FunctionContainer::Method(container_name, container_pos) => function_args
                        .insert(
                            0,
                            crate::new_type!(Ident(container_name.clone()), container_pos.clone()),
                        ),
                    ast::FunctionContainer::StaticFunction(_, _) | ast::FunctionContainer::None => {
                    }
                }

                let function_type = crate::new_type!(
                    Callable {
                        args: function_args.into_boxed_slice(),
                        return_type: return_type.clone(),
                    },
                    prototype_pos.clone()
                );

                match container {
                    ast::FunctionContainer::Method(container_name, container_pos)
                    | ast::FunctionContainer::StaticFunction(container_name, container_pos) => {
                        add_decl_to_type(
                            context,
                            container_name,
                            container_pos,
                            name,
                            function_type,
                            prototype_pos,
                        )?;
                    }
                    ast::FunctionContainer::None => {
                        let function_type_resolved = context.resolve_type(&function_type)?;
                        context.insert_into_latest_scope(
                            name.clone(),
                            Decl {
                                type_: function_type_resolved,
                                is_mut: false,
                                is_init: true,
                            },
                            prototype_pos,
                        )?;
                    }
                }
            }
            // Type::StructClass
            Class { pos, name, body } => {
                context.insert_into_latest_scope(
                    name.clone(),
                    Decl {
                        type_: crate::new_type!(
                            ClassObject(ast::Class {
                                name: name.clone(),
                                body: body.clone(),
                                static_decls: BTreeMap::new(),
                            }),
                            pos.clone()
                        ),
                        is_mut: false,
                        is_init: true,
                    },
                    pos,
                )?;
            }
            Declare { pos, name, type_ } => {
                context.insert_into_latest_scope(
                    name.clone(),
                    Decl {
                        type_: context.resolve_type(type_)?,
                        is_mut: false,
                        is_init: true,
                    },
                    pos,
                )?;
            }
            Require { pos: _, path: _ } => {
                // Nothing
            }
            Global {
                pos,
                is_mut,
                name,
                type_,
                value,
            } => {
                let value_type: ast::Type;
                let effective_type: &ast::Type;
                if let Some(type_) = type_ {
                    effective_type = type_;
                } else {
                    // NOTE : the context is incomplete, so this may fail
                    value_type = get_expr_return_type(context, value, false)?;
                    effective_type = &value_type;
                }
                context.insert_into_latest_scope(
                    name.clone(),
                    Decl {
                        type_: context.resolve_type(effective_type)?,
                        is_mut: *is_mut,
                        is_init: true,
                    },
                    pos,
                )?;
            }
        }
    }
    Ok(())
}

fn add_decl_to_type(
    context: &mut CompilationContext,
    container_name: &str,
    container_pos: &crate::TokenPos,
    decl_name: &str,
    decl_type: Type,
    pos: &crate::TokenPos,
) -> Result<(), CompileError> {
    if let Some(decl) = context.latest_scope_mut().decls.get_mut(container_name) {
        let static_decls = match &mut decl.type_.raw_type {
            RawType::ClassObject(class) => &mut class.static_decls,
            RawType::Struct(struct_body) => &mut struct_body.fields,
            _ => {
                return Err(CompileError::new(
                    format!(
                        "expected type '{}' to be a container, is {}",
                        container_name, decl.type_
                    ),
                    Some(container_pos.clone()),
                ))
            }
        };
        match static_decls.entry(decl_name.to_string()) {
            std::collections::btree_map::Entry::Occupied(_) => {
                return Err(CompileError::new(
                    format!("{}.{}() already exists", container_name, decl_name),
                    Some(pos.clone()),
                ));
            }
            std::collections::btree_map::Entry::Vacant(vacant) => {
                vacant.insert(decl_type);
            }
        }
    } else {
        return Err(CompileError::new(
            format!("Container type '{}' does not exist", container_name),
            Some(container_pos.clone()),
        ));
    }
    Ok(())
}

fn ast_check_soundness(
    context: &mut CompilationContext,
    ast: &ast::Ast,
) -> Result<(), CompileError> {
    use ast::TopLevelField::*;
    for top_level_field in ast.iter() {
        match top_level_field {
            Function {
                pos,
                container,
                name,
                args,
                return_type,
                prototype_pos,
                body,
            } => {
                let mut actual_args: Vec<(String, Type, bool)> = args
                    .iter()
                    .map(|(arg_name, arg_type, is_mut)| {
                        Ok((arg_name.clone(), context.resolve_type(arg_type)?, *is_mut))
                    })
                    .collect::<Result<_, _>>()?;

                match container {
                    ast::FunctionContainer::Method(container_name, container_pos) => {
                        let self_type = context.get_type(container_name, Some(container_pos))?;
                        actual_args.push(("self".to_string(), self_type, false));
                    }
                    ast::FunctionContainer::StaticFunction(_, _) | ast::FunctionContainer::None => {
                    }
                }

                let resolved_return_type = match return_type {
                    Some(return_type) => context.resolve_type(return_type)?,
                    None => crate::new_type!(Nil),
                };

                context.push_scope();
                context
                    .function_return_type_stack
                    .push(resolved_return_type.clone());
                for (arg_name, arg_type, is_mut) in actual_args.iter() {
                    // let arg_type_resolved = context.resolve_type(arg_type, arg_pos)?;
                    context.insert_into_latest_scope(
                        arg_name.clone(),
                        Decl {
                            type_: arg_type.clone(),
                            is_mut: *is_mut,
                            is_init: true,
                        },
                        arg_type.pos.as_ref().unwrap_or(prototype_pos),
                    )?;
                }

                block_check_soundness(context, &body)?;

                if !block_ends_with_return(&body) && resolved_return_type.raw_type != RawType::Nil {
                    return Err(CompileError::new(
                        format!("Function '{}' must return {}", name, resolved_return_type),
                        Some(pos.clone()),
                    ));
                }

                context.function_return_type_stack.pop();
                context.pop_scope();
            }
            Class {
                pos: _,
                name: _,
                body,
            } => {
                for field_type in body.fields.values() {
                    context.resolve_type(field_type)?;
                }
                // TODO : Ensure no duplicate field, and no duplicate kind
                if let Some(u) = &body.union_ {
                    for variant in u.variants.values() {
                        for field_type in variant.values() {
                            context.resolve_type(field_type)?;
                        }
                    }
                }
            }
            Declare {
                pos: _,
                name: _,
                type_,
            } => {
                context.resolve_type(type_)?;
            }
            Require { pos: _, path: _ } => {
                // Nothing
            }
            Global {
                pos,
                is_mut: _,
                name: _,
                type_,
                value,
            } => {
                expr_check_soundness(context, value)?;
                let value_type: ast::Type = get_expr_return_type(context, value, false)?;
                let effective_type: &ast::Type;
                if let Some(type_) = type_ {
                    effective_type = type_;
                } else {
                    effective_type = &value_type;
                }

                expect_type(context, effective_type, &value_type, pos)?;
            }
        }
    }
    Ok(())
}

fn expr_check_soundness(context: &mut CompilationContext, expr: &Expr) -> Result<(), CompileError> {
    match &expr.raw_expr {
        RawExpr::IntLiteral(_)
        | RawExpr::RealLiteral(_)
        | RawExpr::BoolLiteral(_)
        | RawExpr::NilLiteral
        | RawExpr::StringLiteral(_) => {}

        RawExpr::Literal(name) => {
            if context.get(name).is_none() {
                return Err(CompileError::new(
                    format!("unknown value '{}'", name),
                    Some(expr.pos.clone()),
                ));
            }
        }

        RawExpr::Index {
            container_expr,
            index_expr,
        } => {
            expr_check_soundness(context, container_expr)?;
            expr_check_soundness(context, index_expr)?;

            let container_expr_type = get_expr_return_type(context, container_expr, false)?;
            let index_expr_type = get_expr_return_type(context, index_expr, false)?;
            match (&container_expr_type.raw_type, &index_expr_type.raw_type) {
                (RawType::Array(_), RawType::Int) => {}
                (RawType::Struct(struct_body), RawType::Enum(enum_values))
                | (
                    RawType::ClassInstance(ast::Class {
                        body: struct_body, ..
                    }),
                    RawType::Enum(enum_values),
                ) => {
                    for enum_value in enum_values {
                        if struct_body.get_field(enum_value, context)?.is_none() {
                            return Err(CompileError::new(
                                format!(
                                    "cannot index {} with {} : doesn't have field '{}'",
                                    container_expr_type, index_expr_type, enum_value,
                                ),
                                Some(index_expr.pos.clone()),
                            ));
                        }
                    }
                }
                (RawType::Struct(_), _) | (RawType::ClassInstance(_), _) => {
                    return Err(CompileError::new(
                        format!(
                            "cannot index {} with {}",
                            container_expr_type, index_expr_type
                        ),
                        Some(index_expr.pos.clone()),
                    ))
                }
                (RawType::Map(boxed), _) => {
                    let (key, _value) = boxed.as_ref();

                    expect_type(context, key, &index_expr_type, &expr.pos)?;
                }
                _ => {
                    return Err(CompileError::new(
                        format!(
                            "Cannot index {} with {}",
                            container_expr_type, index_expr_type
                        ),
                        Some(container_expr.pos.clone()),
                    ))
                }
            }
        }

        RawExpr::StaticIndex {
            container_expr,
            index,
        } => {
            expr_check_soundness(context, container_expr)?;

            let container_expr_type = get_expr_return_type(context, container_expr, false)?;
            // let container_expr_type = context.resolve_type(&container_expr_type)?;
            match &container_expr_type.raw_type {
                RawType::ClassInstance(class) => {
                    if class.body.get_field(index, context)?.is_none()
                        && class.static_decls.get(index).is_none()
                    {
                        return Err(unknown_field_error(
                            &container_expr_type,
                            index,
                            expr.pos.clone(),
                        ));
                    }
                }
                RawType::Struct(struct_body) => {
                    if struct_body.get_field(index, context)?.is_none() {
                        return Err(unknown_field_error(
                            &container_expr_type,
                            index,
                            expr.pos.clone(),
                        ));
                    }
                }
                RawType::ClassObject(class) => {
                    if class.static_decls.get(index).is_none() {
                        return Err(unknown_field_error(
                            &container_expr_type,
                            index,
                            expr.pos.clone(),
                        ));
                    }
                }
                RawType::Map(boxed) => {
                    let (key, _value) = boxed.as_ref();
                    match &key.raw_type {
                        RawType::String => {}
                        RawType::Enum(set) if set.contains(index) => {}
                        _ => {
                            return Err(CompileError::new(
                                format!("expected map key type {}, got '{}'", key, index),
                                Some(expr.pos.clone()),
                            ))
                        }
                    }
                }
                _ => {
                    return Err(CompileError::new(
                        format!("Type {} cannot be indexed", container_expr_type),
                        Some(expr.pos.clone()),
                    ))
                }
            }
        }

        RawExpr::FunctionCall {
            function_expr,
            args,
        } => {
            expr_check_soundness(context, &function_expr)?;
            let function_type: Type = get_expr_return_type(context, function_expr, false)?;

            let function_type_args: &[Type] = match &function_type.raw_type {
                RawType::Callable {
                    args: function_type_args,
                    return_type: _,
                } => function_type_args,
                _ => {
                    return Err(CompileError::new(
                        format!("Cannot call type {}", function_type),
                        Some(expr.pos.clone()),
                    ))
                }
            };

            if args.len() > function_type_args.len() {
                return Err(CompileError::new(
                    format!(
                        "Too many args ({}) for {}, should be {}",
                        args.len(),
                        function_type,
                        function_type_args.len()
                    ),
                    Some(expr.pos.clone()),
                ));
            }

            for (i, arg_expr) in args.iter().enumerate() {
                expr_check_soundness(context, &arg_expr)?;
                let arg_type = get_expr_return_type(context, arg_expr, false)?;

                expect_type(context, &function_type_args[i], &arg_type, &arg_expr.pos)?;
            }
            // check that the non-given args are all optional
            for (i, arg_type) in function_type_args[args.len()..].iter().enumerate() {
                if context
                    .is_type_included_in(&arg_type, &crate::new_type!(Nil))?
                    .is_some()
                {
                    return Err(CompileError::new(
                        format!(
                            "argument {} missing, type {} is not optional",
                            i + args.len() + 1,
                            arg_type,
                        ),
                        Some(expr.pos.clone()),
                    ));
                }
            }
        }

        RawExpr::MethodCall {
            object_expr,
            function_name,
            args,
        } => {
            expr_check_soundness(context, &object_expr)?;
            let object_expr_type = get_expr_return_type(context, object_expr, false)?;
            let object_expr_type = context.resolve_type(&object_expr_type)?;
            let class = match &object_expr_type.raw_type {
                RawType::ClassInstance(class) => class,
                _ => {
                    return Err(CompileError::new(
                        format!("expected class instance, got {}", object_expr_type),
                        Some(object_expr.pos.clone()),
                    ))
                }
            };

            let function_type = if let Some(function_type) = class.static_decls.get(function_name) {
                function_type
            } else {
                return Err(CompileError::new(
                    format!("Unknown method '{}:{}'", class.name, function_name),
                    Some(expr.pos.clone()),
                ));
            };

            let function_type_args: &[Type] = match &function_type.raw_type {
                RawType::Callable {
                    args: function_type_args,
                    return_type: _,
                } => &function_type_args,
                _ => {
                    return Err(CompileError::new(
                        format!("Cannot call type {}", function_type),
                        Some(expr.pos.clone()),
                    ))
                }
            };

            let function_type_args_resolved: Vec<Type> = function_type_args
                .iter()
                .map(|t| context.resolve_type(t))
                .collect::<Result<_, _>>()?;

            let mut actual_args = Vec::from(args.as_ref());
            actual_args.insert(0, object_expr.as_ref().clone());

            if actual_args.len() > function_type_args_resolved.len() {
                return Err(CompileError::new(
                    format!(
                        "Too many args ({}) for {}, should be {}",
                        actual_args.len(),
                        function_type,
                        function_type_args_resolved.len()
                    ),
                    Some(expr.pos.clone()),
                ));
            }

            for (i, arg_expr) in actual_args.iter().enumerate() {
                expr_check_soundness(context, &arg_expr)?;
                let arg_type = get_expr_return_type(context, arg_expr, false)?;

                expect_type(
                    context,
                    &function_type_args_resolved[i],
                    &arg_type,
                    &arg_expr.pos,
                )?;
            }
            // check that the non-given args are all optional
            for (i, arg_type) in function_type_args_resolved[actual_args.len()..]
                .iter()
                .enumerate()
            {
                if context
                    .is_type_included_in(&arg_type, &crate::new_type!(Nil))?
                    .is_some()
                {
                    return Err(CompileError::new(
                        format!(
                            "argument {} missing, type {} is not optional",
                            i + actual_args.len() + 1,
                            arg_type,
                        ),
                        Some(expr.pos.clone()),
                    ));
                }
            }
        }

        RawExpr::New {
            class_type, //: Box<Type>,
            fields,     //: HashMap<String, Expr>,
        } => {
            let class_type_resolved = context.resolve_type(class_type)?;
            let class = match &class_type_resolved.raw_type {
                RawType::ClassInstance(class) => class,
                RawType::Ident(_) => unreachable!(),
                _ => {
                    return Err(CompileError::new(
                        format!("Invalid type for instanciation : {}", class_type_resolved),
                        Some(expr.pos.clone()),
                    ))
                }
            };

            let fields_struct_body = ast::StructBody {
                fields: fields
                    .iter()
                    .map(|(field_name, field_expr)| {
                        Ok((
                            field_name.clone(),
                            get_expr_return_type(context, field_expr, false)?,
                        ))
                    })
                    .collect::<Result<_, _>>()?,
                union_: None,
            };

            if let Some(struct_body_error) =
                context.can_fields_be_put_in_struct_body(&class.body, &fields_struct_body)?
            {
                let received_type = crate::new_type!(Struct(fields_struct_body));
                let pos = match &struct_body_error {
                    StructBodyError::MissingField {
                        field_name: _,
                        expected_field_type: _,
                    } => expr.pos.clone(),
                    StructBodyError::UnknownField { field_name } => {
                        let given_field_expr = fields.get(field_name.as_str()).unwrap();
                        given_field_expr.pos.clone()
                    }
                    StructBodyError::IncorrectFieldType {
                        field_name,
                        expected_field_type: _,
                        gotten_field_type: _,
                        type_inclusion_error: _,
                    } => fields.get(field_name.as_str()).unwrap().pos.clone(),
                };
                return Err(CompileError {
                    description: struct_body_error
                        .get_main_message(&class_type_resolved, &received_type),
                    pos: Some(pos),
                    notes: struct_body_error.get_sub_notes(),
                });
            }
        }

        RawExpr::TableLiteral {
            fields, //: HashMap<String, Expr>,
        } => {
            for field_expr in fields.values() {
                expr_check_soundness(context, field_expr)?;
            }
        }

        RawExpr::Add(boxed)
        | RawExpr::Sub(boxed)
        | RawExpr::Mul(boxed)
        | RawExpr::Div(boxed)
        | RawExpr::Mod(boxed)
        | RawExpr::Concat(boxed)
        | RawExpr::Pow(boxed) => {
            let (a, b) = boxed.as_ref();
            expr_check_soundness(context, &a)?;
            expr_check_soundness(context, &b)?;

            // get_expr_return_type is making sure that the types are correct for the op
            let _ = get_expr_return_type(context, expr, false)?;
        }

        RawExpr::And(boxed) => {
            let (a, b) = boxed.as_ref();
            expr_check_soundness(context, &a)?;

            // if b is evaluated, then a is true
            let checks = ast::infer_checks_from_expr(a);
            context.push_scope_with_checks(checks);

            expr_check_soundness(context, &b)?;

            context.pop_scope();

            let _ = get_expr_return_type(context, expr, false)?;
        }

        RawExpr::Or(boxed) => {
            let (a, b) = boxed.as_ref();
            expr_check_soundness(context, &a)?;

            // if b is evaluated, then a is false
            let checks = ast::infer_checks_from_expr(a);
            context.push_scope_with_checks(checks.into_iter().filter_map(|c| c.invert()).collect());

            expr_check_soundness(context, &b)?;

            context.pop_scope();

            let _ = get_expr_return_type(context, expr, false)?;
        }
        RawExpr::Minus(sub_expr) | RawExpr::Not(sub_expr) => {
            expr_check_soundness(context, sub_expr.as_ref())?;

            let _ = get_expr_return_type(context, expr, false)?;
        }

        RawExpr::Eq(boxed) | RawExpr::NotEq(boxed) => {
            let (a, b) = boxed.as_ref();
            expr_check_soundness(context, &a)?;
            expr_check_soundness(context, &b)?;
            let a_type = get_expr_return_type(context, a, false)?;
            let b_type = get_expr_return_type(context, b, false)?;

            if !context.types_can_be_compared_for_equality(&a_type, &b_type)? {
                return Err(CompileError::new(
                    format!(
                        "Types {} and {} cannot be compared for equality",
                        a_type, b_type
                    ),
                    Some(expr.pos.clone()),
                ));
            }
        }

        RawExpr::Greater(boxed)
        | RawExpr::GreaterEq(boxed)
        | RawExpr::Lower(boxed)
        | RawExpr::LowerEq(boxed) => {
            let (a, b) = boxed.as_ref();
            expr_check_soundness(context, &a)?;
            expr_check_soundness(context, &b)?;
            let a_type = get_expr_return_type(context, a, false)?;
            let b_type = get_expr_return_type(context, b, false)?;

            match (&a_type.raw_type, &b_type.raw_type) {
                (RawType::Real, RawType::Real)
                | (RawType::Real, RawType::Int)
                | (RawType::Int, RawType::Real)
                | (RawType::Int, RawType::Int) => {}
                (RawType::String, RawType::String)
                | (RawType::String, RawType::Enum(_))
                | (RawType::Enum(_), RawType::String) => {}
                (RawType::Enum(_), RawType::Enum(_)) => {}
                (RawType::Ident(_), _) | (_, RawType::Ident(_)) => unreachable!(),
                _ => {
                    return Err(CompileError::new(
                        format!("Types {} and {} cannot be compared", a_type, b_type),
                        Some(expr.pos.clone()),
                    ))
                }
            }
        }
        RawExpr::ArrayLiteral(items) => {
            for item in items {
                expr_check_soundness(context, item)?;
            }
        }
        RawExpr::UnwrapOptional(expr) => {
            let type_ = get_expr_return_type(context, expr.as_ref(), false)?;
            match type_.raw_type {
                RawType::Nil => {}
                RawType::Optional(_) => {}
                RawType::Ident(_) => unreachable!(),
                _ => {
                    return Err(CompileError::new(
                        format!("Cannot unwrap type {} , it cannot be nil", type_),
                        Some(expr.pos.clone()),
                    ))
                }
            }
        }
        RawExpr::Cast(expr, _type) => {
            expr_check_soundness(context, expr)?;
        }
        RawExpr::AtCall { at_function, args } => {
            for arg in args.iter() {
                expr_check_soundness(context, &arg)?;
            }

            match at_function {
                ast::AtFunction::Len => {
                    if args.len() != 1 {
                        return Err(CompileError::new(
                            format!("@len() function need one argument, got {}", args.len()),
                            Some(expr.pos.clone()),
                        ));
                    }
                    let type_ = get_expr_return_type(context, &args[0], false)?;
                    match type_.raw_type {
                        RawType::Array(_) | RawType::EmptyArray => {}
                        RawType::Ident(_) => unreachable!(),

                        _ => {
                            return Err(CompileError::new(
                                format!("Expected array (for @len()), got {}", type_),
                                Some(args[0].pos.clone()),
                            ))
                        }
                    }
                }
                ast::AtFunction::Abs => {
                    if args.len() != 1 {
                        return Err(CompileError::new(
                            format!("@abs() function need one argument, got {}", args.len()),
                            Some(expr.pos.clone()),
                        ));
                    }
                    let type_ = get_expr_return_type(context, &args[0], false)?;
                    match type_.raw_type {
                        RawType::Int | RawType::Real => {}
                        RawType::Ident(_) => unreachable!(),

                        _ => {
                            return Err(CompileError::new(
                                format!("Expected int or real (for @abs()), got {}", type_),
                                Some(args[0].pos.clone()),
                            ))
                        }
                    }
                }
                ast::AtFunction::Min | ast::AtFunction::Max => {
                    if args.len() < 2 {
                        return Err(CompileError::new(
                            format!(
                                "@{:?}() function need two arguments or more, got {}",
                                at_function,
                                args.len()
                            ),
                            Some(expr.pos.clone()),
                        ));
                    }
                    for arg in args.iter() {
                        let type_ = get_expr_return_type(context, &arg, false)?;
                        match type_.raw_type {
                            RawType::Int | RawType::Real => {}
                            RawType::Ident(_) => unreachable!(),

                            _ => {
                                return Err(CompileError::new(
                                    format!(
                                        "Expected int or real (for @{:?}()), got {}",
                                        at_function, type_
                                    ),
                                    Some(arg.pos.clone()),
                                ))
                            }
                        }
                    }
                }
                ast::AtFunction::Insert => {
                    if args.len() != 2 {
                        return Err(CompileError::new(
                            format!("@insert() function need two arguments, got {}", args.len()),
                            Some(expr.pos.clone()),
                        ));
                    }
                    let container_type = get_expr_return_type(context, &args[0], false)?;
                    let item_type = get_expr_return_type(context, &args[1], false)?;
                    match container_type.raw_type {
                        RawType::Array(child_type) => {
                            expect_type(context, &child_type, &item_type, &args[1].pos)?;
                        }
                        RawType::EmptyArray => {
                            return Err(CompileError::new(
                                format!(
                                    "This array is empty and need a specified type, like [{}]",
                                    item_type
                                ),
                                Some(container_type.pos.as_ref().unwrap_or(&args[0].pos).clone()),
                            ))
                        }
                        RawType::Ident(_) => unreachable!(),

                        _ => {
                            return Err(CompileError::new(
                                format!("Expected array (for @insert()), got {}", container_type),
                                Some(args[0].pos.clone()),
                            ))
                        }
                    }
                }
            }
        }
    }

    Ok(())
}

fn block_ends_with_return(block: &ast::Block) -> bool {
    let last_inst = block.insts.iter().rev().next();
    match last_inst.map(|i| &i.raw_inst) {
        Some(ast::RawInst::If {
            condition: _,
            then_block,
            else_block: Some(else_block),
        }) => block_ends_with_return(then_block) && block_ends_with_return(else_block),
        Some(ast::RawInst::Return(_)) => true,
        _ => false,
    }
}

// return vars initialized in this block
fn block_check_soundness(
    context: &mut CompilationContext,
    block: &ast::Block,
) -> Result<BTreeSet<String>, CompileError> {
    context.push_scope();
    for inst in block.insts.iter() {
        inst_check_soundness(context, inst)?;
    }
    let scope = context.pop_scope();
    Ok(scope.initialized_decls_in_this_scope)
}

fn inst_check_soundness(
    context: &mut CompilationContext,
    inst: &ast::Inst,
) -> Result<(), CompileError> {
    use ast::RawInst;
    match &inst.raw_inst {
        RawInst::Expr(expr) => {
            expr_check_soundness(context, expr)?;
        }
        RawInst::Assign(dest_expr, src_expr) => {
            expr_check_soundness(context, dest_expr)?;
            expr_check_soundness(context, src_expr)?;

            let src_type = get_expr_return_type(context, src_expr, false)?;
            let dest_type = get_expr_return_type(context, dest_expr, true)?;

            expect_type(context, &dest_type, &src_type, &inst.pos)?;

            check_expr_is_valid_assign_dest(context, dest_expr)?;

            match &dest_expr.raw_expr {
                RawExpr::Literal(name) => context.mark_as_initialized(name),
                _ => {}
            }
        }
        RawInst::Let {
            is_mut,
            name,
            type_,
            value,
        } => {
            if let Some(value_expr) = value {
                expr_check_soundness(context, value_expr)?;
            }

            let pos: &crate::TokenPos;
            let is_init;
            // register the var in scope
            let actual_type = if let Some(type_) = type_ {
                pos = type_.pos.as_ref().unwrap();
                let resolved_type = context.resolve_type(type_)?;
                if let Some(value_expr) = value {
                    let value_expr_type = get_expr_return_type(context, value_expr, false)?;
                    expect_type(context, &resolved_type, &value_expr_type, &inst.pos)?;
                    is_init = true;
                } else {
                    // ex : let foo: ?int; is already initialized
                    is_init = context
                        .is_type_included_in(&type_, &crate::new_type!(Nil))?
                        .is_none();
                }
                resolved_type
            } else if let Some(value_expr) = value {
                pos = &value_expr.pos;
                is_init = true;
                get_expr_return_type(context, value_expr, false)?
            } else {
                return Err(CompileError::new(
                    format!("No type and no value for '{}' binding", name),
                    Some(inst.pos.clone()),
                ));
            };
            context.insert_into_latest_scope(
                name.clone(),
                Decl {
                    type_: actual_type,
                    is_mut: *is_mut,
                    is_init,
                },
                // TODO : use name pos
                pos,
            )?;
        }
        RawInst::Return(expr) => {
            expr_check_soundness(context, expr)?;
            let return_expr_type = get_expr_return_type(context, expr, false)?;
            let return_type: &Type = context
                .function_return_type_stack
                .iter()
                .rev()
                .next()
                .expect("context should have a return type pushed");
            expect_type(context, &return_type, &return_expr_type, &inst.pos)?;
        }
        RawInst::If {
            condition,
            then_block,
            else_block,
        } => {
            expr_check_soundness(context, condition)?;
            let condition_type = get_expr_return_type(context, condition, false)?;
            if condition_type.raw_type != RawType::Bool {
                return Err(CompileError::new(
                    format!("if condition isn't a bool, but {}", condition_type),
                    Some(inst.pos.clone()),
                ));
            }

            let checks = ast::infer_checks_from_expr(condition);
            context.push_scope_with_checks(checks.clone());

            let init_vars_in_then_scope = block_check_soundness(context, then_block)?;

            context.pop_scope();

            if let Some(else_block) = &else_block {
                let checks_invert = checks
                    .into_iter()
                    .filter_map(|check| check.invert())
                    .collect();
                context.push_scope_with_checks(checks_invert);

                let init_vars_in_else_scope = block_check_soundness(context, else_block)?;
                context.pop_scope();

                for var_name in init_vars_in_then_scope.intersection(&init_vars_in_else_scope) {
                    context.mark_as_initialized(var_name);
                }
            }
        }
        RawInst::While { condition, block } => {
            expr_check_soundness(context, condition)?;
            let condition_type = get_expr_return_type(context, condition, false)?;
            if condition_type.raw_type != RawType::Bool {
                return Err(CompileError::new(
                    format!("while condition isn't a bool, but {}", condition_type),
                    Some(inst.pos.clone()),
                ));
            }

            block_check_soundness(context, &block)?;
        }
        ast::RawInst::For {
            index_ident,
            index_pos,
            value_ident,
            value_pos,
            iter_type,
            iterator_expr,
            block,
        } => {
            expr_check_soundness(context, iterator_expr)?;

            let index_type: Type;
            let value_type: Type;

            let iterator_type = get_expr_return_type(context, iterator_expr, false)?;

            match iter_type {
                ast::ForIterType::Array => {
                    index_type = crate::new_type!(Int);

                    match iterator_type.raw_type {
                        RawType::Array(child) => {
                            value_type = child.as_ref().clone();
                        }
                        RawType::Ident(_) => unreachable!(),
                        _ => {
                            return Err(CompileError::new(
                                format!("for iterator isn't an array, but {}", iterator_type),
                                Some(iterator_expr.pos.clone()),
                            ))
                        }
                    }
                }
                ast::ForIterType::Map => match iterator_type.raw_type {
                    RawType::Struct(struct_body) => {
                        index_type = crate::new_type!(struct_body.get_key_type());
                        value_type = context.get_union_of_all_fields(&struct_body)?;
                    }
                    RawType::ClassInstance(class) => {
                        index_type = crate::new_type!(class.body.get_key_type());
                        value_type = context.get_union_of_all_fields(&class.body)?;
                    }
                    RawType::Map(boxed) => {
                        let (key, value) = boxed.as_ref();
                        index_type = key.clone();
                        value_type = value.clone();
                    }
                    RawType::Ident(_) => unreachable!(),
                    _ => {
                        return Err(CompileError::new(
                            format!("expected map for 'for' iterator, got {}", iterator_type),
                            Some(iterator_expr.pos.clone()),
                        ))
                    }
                },
            }

            context.push_scope();

            context.insert_into_latest_scope(
                index_ident.clone(),
                Decl {
                    type_: index_type,
                    is_mut: false,
                    is_init: true,
                },
                index_pos,
            )?;
            context.insert_into_latest_scope(
                value_ident.clone(),
                Decl {
                    type_: value_type,
                    is_mut: false,
                    is_init: true,
                },
                value_pos,
            )?;

            block_check_soundness(context, &block)?;
            context.pop_scope();
        }
        ast::RawInst::ForRange {
            index_ident,
            index_pos,
            start_expr,
            end_expr,
            block,
        } => {
            let start_type = get_expr_return_type(context, start_expr, false)?;
            expect_type(
                context,
                &crate::new_type!(Int),
                &start_type,
                &start_expr.pos,
            )?;
            let end_type = get_expr_return_type(context, end_expr, false)?;
            expect_type(context, &crate::new_type!(Int), &end_type, &end_expr.pos)?;

            context.push_scope();

            context.insert_into_latest_scope(
                index_ident.clone(),
                Decl {
                    type_: crate::new_type!(Int),
                    is_mut: false,
                    is_init: true,
                },
                index_pos,
            )?;

            block_check_soundness(context, &block)?;
            context.pop_scope();
        }
    }
    Ok(())
}

fn check_expr_is_valid_assign_dest(
    context: &mut CompilationContext,
    dest_expr: &Expr,
) -> Result<(), CompileError> {
    match &dest_expr.raw_expr {
        RawExpr::Literal(name) => {
            if let Some(decl) = context.get(name) {
                if !decl.is_mut && context.is_initialized(name) {
                    return Err(CompileError::new(
                        format!("binding '{}' is not mut", name),
                        Some(dest_expr.pos.clone()),
                    ));
                }
            } else {
                return Err(CompileError::new(
                    format!("unknown variable '{}'", name),
                    Some(dest_expr.pos.clone()),
                ));
            }
        }
        RawExpr::Index {
            container_expr: _,
            index_expr: _,
        } => {}
        RawExpr::StaticIndex {
            container_expr: _,
            index: _,
        } => {}
        _ => {
            return Err(CompileError::new(
                format!("invalid l-value : {:?}", dest_expr.raw_expr),
                Some(dest_expr.pos.clone()),
            ))
        }
    }

    Ok(())
}

// The returned type MUST be resolved.
fn get_expr_return_type(
    context: &mut CompilationContext,
    expr: &Expr,
    // If it's an l-value, we don't apply checks
    is_l_value: bool,
) -> Result<Type, CompileError> {
    use crate::new_type;
    let return_type = match &expr.raw_expr {
        RawExpr::IntLiteral(_) => new_type!(Int),
        RawExpr::RealLiteral(_) => new_type!(Real),
        RawExpr::BoolLiteral(_) => new_type!(Bool),
        RawExpr::NilLiteral => new_type!(Nil),
        RawExpr::StringLiteral(s) => {
            let mut set = BTreeSet::new();
            set.insert(s.clone());
            new_type!(Enum(set))
        }

        RawExpr::Literal(name) => {
            let decl = if let Some(decl) = context.get(name) {
                decl
            } else {
                return Err(CompileError::new(
                    format!("Could not find '{:?}'", name),
                    Some(expr.pos.clone()),
                ));
            };
            let mut decl_type: Type = context.resolve_type(&decl.type_)?;

            if !is_l_value && !context.is_initialized(name) {
                return Err(CompileError::new(
                    format!("'{}' is not initialized", name),
                    Some(expr.pos.clone()),
                ));
            }

            // Apply checks to narrow the type if we're just reading from the var.
            if !is_l_value {
                for check in context.iter_checks() {
                    match check {
                        ast::Check::IsNotNil { ident } if ident == name => {
                            decl_type = context.resolve_type(&decl_type.without_nil())?;
                        }
                        ast::Check::IsNil { ident } if ident == name => {
                            decl_type = decl_type.only_nil();
                        }
                        ast::Check::FieldIsString {
                            ident,
                            field_name,
                            value,
                        } if ident == name => match &decl_type.raw_type {
                            RawType::Struct(ast::StructBody {
                                fields,
                                union_:
                                    Some(ast::Union {
                                        kind_field,
                                        activated_variant: _,
                                        variants,
                                    }),
                            }) if kind_field == field_name && variants.contains_key(value) => {
                                decl_type = Type {
                                    raw_type: RawType::Struct(ast::StructBody {
                                        fields: fields.clone(),
                                        union_: Some(ast::Union {
                                            kind_field: kind_field.clone(),
                                            activated_variant: Some(value.clone()),
                                            variants: variants.clone(),
                                        }),
                                    }),
                                    pos: decl_type.pos.clone(),
                                };
                            }
                            // TODO classes
                            RawType::ClassInstance(ast::Class {
                                name,
                                body:
                                    ast::StructBody {
                                        fields,
                                        union_:
                                            Some(ast::Union {
                                                kind_field,
                                                activated_variant: _,
                                                variants,
                                            }),
                                    },
                                static_decls,
                            }) if kind_field == field_name && variants.contains_key(value) => {
                                decl_type = Type {
                                    raw_type: RawType::ClassInstance(ast::Class {
                                        name: name.clone(),
                                        static_decls: static_decls.clone(),
                                        body: ast::StructBody {
                                            fields: fields.clone(),
                                            union_: Some(ast::Union {
                                                kind_field: kind_field.clone(),
                                                activated_variant: Some(value.clone()),
                                                variants: variants.clone(),
                                            }),
                                        },
                                    }),
                                    pos: decl_type.pos.clone(),
                                };
                            }
                            _ => {}
                        },
                        _ => {}
                    }
                }
            }

            decl_type
        }

        RawExpr::Index {
            container_expr,
            index_expr,
        } => {
            let container_expr_type = get_expr_return_type(context, container_expr, false)?;
            let index_expr_type = get_expr_return_type(context, index_expr, false)?;
            match (&container_expr_type.raw_type, &index_expr_type.raw_type) {
                (RawType::Array(child_type), _) => context.resolve_type(child_type)?,
                (RawType::Struct(struct_body), RawType::Enum(enum_values))
                | (
                    RawType::ClassInstance(ast::Class {
                        body: struct_body, ..
                    }),
                    RawType::Enum(enum_values),
                ) => {
                    let mut type_union = crate::new_type!(Never);
                    for enum_value in enum_values {
                        if let Some(field_type) =
                            struct_body.get_field(enum_value.as_str(), context)?
                        {
                            let field_type_resolved = context.resolve_type(&field_type)?;
                            type_union =
                                context.get_type_union(&type_union, &field_type_resolved)?;
                        } else {
                            return Err(CompileError::new(
                                format!(
                                    "Cannot index {} with {} : doesn't have field '{}'",
                                    container_expr_type, index_expr_type, enum_value,
                                ),
                                Some(index_expr.pos.clone()),
                            ));
                        }
                    }
                    type_union
                }
                (RawType::Struct(_), _) | (RawType::ClassInstance(_), _) => {
                    return Err(CompileError::new(
                        format!(
                            "cannot index {} with {}",
                            container_expr_type, index_expr_type
                        ),
                        Some(index_expr.pos.clone()),
                    ))
                }
                (RawType::Map(boxed), _) => {
                    let (_key, value) = boxed.as_ref();
                    let value_resolved = context.resolve_type(&value)?;
                    context.get_type_union(&crate::new_type!(Nil), &value_resolved)?
                }
                (RawType::Ident(_), _) => unreachable!(),
                _ => {
                    return Err(CompileError::new(
                        format!("expected indexable, got {}", container_expr_type),
                        Some(container_expr.pos.clone()),
                    ))
                }
            }
        }

        RawExpr::StaticIndex {
            container_expr,
            index,
        } => {
            let container_expr_type = get_expr_return_type(context, container_expr, false)?;
            match &container_expr_type.raw_type {
                RawType::ClassInstance(class) => {
                    if let Some(field_type) = class.body.get_field(index, context)? {
                        context.resolve_type(&field_type)?
                    } else if let Some(static_decl_type) = class.static_decls.get(index) {
                        context.resolve_type(static_decl_type)?
                    } else {
                        return Err(unknown_field_error(
                            &container_expr_type,
                            index,
                            expr.pos.clone(),
                        ));
                    }
                }
                RawType::Struct(struct_body) => {
                    if let Some(field_type) = struct_body.get_field(index, context)? {
                        context.resolve_type(&field_type)?
                    } else {
                        return Err(unknown_field_error(
                            &container_expr_type,
                            index,
                            expr.pos.clone(),
                        ));
                    }
                }
                RawType::ClassObject(class) => {
                    if let Some(static_decl_type) = class.static_decls.get(index) {
                        context.resolve_type(static_decl_type)?
                    } else {
                        return Err(CompileError::new(
                            format!("Unknown static decl '{}' in {}", index, container_expr_type),
                            Some(expr.pos.clone()),
                        ));
                    }
                }
                RawType::Map(boxed) => {
                    let (_key, value) = boxed.as_ref();
                    context.get_type_union(&crate::new_type!(Nil), &value)?
                }
                RawType::Ident(_) => unreachable!(),
                _ => {
                    return Err(CompileError::new(
                        format!("expected indexable, got {:?}", container_expr_type),
                        Some(expr.pos.clone()),
                    ))
                }
            }
        }

        RawExpr::FunctionCall {
            function_expr,
            args: _,
        } => {
            let function_expr_type = get_expr_return_type(context, function_expr, false)?;
            match function_expr_type.raw_type {
                RawType::Callable {
                    args: _,
                    return_type,
                } => match return_type {
                    Some(return_type) => context.resolve_type(&return_type)?,
                    None => new_type!(Nil),
                },
                RawType::Ident(_) => unreachable!(),
                _ => {
                    return Err(CompileError::new(
                        format!("expected callable, got {:?}", function_expr_type),
                        Some(function_expr.pos.clone()),
                    ))
                }
            }
        }
        RawExpr::MethodCall {
            object_expr,
            function_name,
            args: _,
        } => {
            let object_expr_type = get_expr_return_type(context, object_expr, false)?;
            let class = match &object_expr_type.raw_type {
                RawType::ClassInstance(class) => class,
                RawType::Ident(_) => unreachable!(),
                _ => {
                    return Err(CompileError::new(
                        format!("expected class instance, got {:?}", object_expr_type),
                        Some(object_expr.pos.clone()),
                    ))
                }
            };

            if let Some(function_type) = class.static_decls.get(function_name) {
                match &function_type.raw_type {
                    RawType::Callable {
                        args: _,
                        return_type,
                    } => match return_type {
                        Some(return_type) => context.resolve_type(return_type)?,
                        None => new_type!(Nil),
                    },
                    RawType::Ident(_) => unreachable!(),
                    _ => {
                        return Err(CompileError::new(
                            format!("expected callable, got {:?}", function_type),
                            Some(expr.pos.clone()),
                        ))
                    }
                }
            } else {
                return Err(CompileError::new(
                    format!("Unknown method '{}'", function_name),
                    Some(expr.pos.clone()),
                ));
            }
        }

        RawExpr::New {
            class_type,
            fields: _,
        } => context.resolve_type(class_type)?,

        RawExpr::TableLiteral { fields } => new_type!(Struct(ast::StructBody {
            fields: fields
                .iter()
                .map(|(field_name, field_expr)| {
                    Ok((
                        field_name.clone(),
                        get_expr_return_type(context, field_expr, false)?,
                    ))
                })
                .collect::<Result<_, _>>()?,
            union_: None,
        })),

        RawExpr::Add(boxed) | RawExpr::Sub(boxed) | RawExpr::Mul(boxed) | RawExpr::Mod(boxed) => {
            let type_a = get_expr_return_type(context, &boxed.0, false)?;
            let type_b = get_expr_return_type(context, &boxed.1, false)?;
            match (&type_a.raw_type, &type_b.raw_type) {
                (RawType::Int, RawType::Int) => new_type!(Int),
                (RawType::Real, RawType::Int)
                | (RawType::Int, RawType::Real)
                | (RawType::Real, RawType::Real) => {
                    new_type!(Real)
                }
                _ => {
                    return Err(CompileError::new(
                        format!("Invalid types {} and {} for binary op", type_a, type_b),
                        Some(expr.pos.clone()),
                    ))
                }
            }
        }

        RawExpr::Div(boxed) | RawExpr::Pow(boxed) => {
            let type_a = get_expr_return_type(context, &boxed.0, false)?;
            let type_b = get_expr_return_type(context, &boxed.1, false)?;
            match (&type_a.raw_type, &type_b.raw_type) {
                (RawType::Int, RawType::Int)
                | (RawType::Real, RawType::Int)
                | (RawType::Int, RawType::Real)
                | (RawType::Real, RawType::Real) => new_type!(Real),
                _ => {
                    return Err(CompileError::new(
                        format!("Invalid types {} and {} for binary op", type_a, type_b),
                        Some(expr.pos.clone()),
                    ))
                }
            }
        }
        RawExpr::Concat(boxed) => {
            let type_a = get_expr_return_type(context, &boxed.0, false)?;
            let type_b = get_expr_return_type(context, &boxed.1, false)?;
            match (&type_a.raw_type, &type_b.raw_type) {
                (RawType::Enum(set_a), RawType::Enum(set_b)) => {
                    let mut set = BTreeSet::new();

                    for it_a in set_a {
                        for it_b in set_b {
                            set.insert(format!("{}{}", it_a, it_b));
                        }
                    }

                    new_type!(RawType::Enum(set))
                }
                (RawType::String, RawType::Enum(_))
                | (RawType::Enum(_), RawType::String)
                | (RawType::String, RawType::String) => new_type!(String),
                _ => {
                    return Err(CompileError::new(
                        format!("Invalid types {} and {} for binary op", type_a, type_b),
                        Some(expr.pos.clone()),
                    ))
                }
            }
        }
        RawExpr::Minus(boxed_expr) => {
            let inner_type = get_expr_return_type(context, boxed_expr, false)?;
            match inner_type.raw_type {
                RawType::Int => new_type!(Int),
                RawType::Real => new_type!(Real),
                _ => {
                    return Err(CompileError::new(
                        format!("Invalid types {} for unary op", inner_type),
                        Some(expr.pos.clone()),
                    ))
                }
            }
        }

        RawExpr::Eq(_)
        | RawExpr::NotEq(_)
        | RawExpr::Greater(_)
        | RawExpr::GreaterEq(_)
        | RawExpr::Lower(_)
        | RawExpr::LowerEq(_)
        | RawExpr::Not(_) => new_type!(Bool),

        RawExpr::And(boxed) => {
            let (a, b) = boxed.as_ref();

            let a_type = get_expr_return_type(context, a, false)?;
            let b_type = get_expr_return_type(context, b, false)?;

            match (&a_type.raw_type, &b_type.raw_type) {
                (RawType::Bool, RawType::Bool) => new_type!(Bool),
                (RawType::Optional(_), _) => {
                    context.resolve_type(&context.get_type_union(&new_type!(Nil), &b_type)?)?
                }
                _ => {
                    return Err(CompileError::new(
                        format!("Expected bools or optional, got {} & {}", a_type, b_type),
                        Some(expr.pos.clone()),
                    ))
                }
            }
        }

        RawExpr::Or(boxed) => {
            let (a, b) = boxed.as_ref();

            let a_type = get_expr_return_type(context, a, false)?;
            let b_type = get_expr_return_type(context, b, false)?;

            match (&a_type.raw_type, &b_type.raw_type) {
                (RawType::Bool, RawType::Bool) => new_type!(Bool),
                (RawType::Optional(sub_type), _) => {
                    context.resolve_type(&context.get_type_union(&sub_type, &b_type)?)?
                }
                _ => {
                    return Err(CompileError::new(
                        format!("Expected bools or optional, got {} & {}", a_type, b_type),
                        Some(expr.pos.clone()),
                    ))
                }
            }
        }

        RawExpr::ArrayLiteral(items) => {
            if items.is_empty() {
                new_type!(EmptyArray)
            } else {
                let child_type =
                    items
                        .iter()
                        .try_fold(crate::new_type!(Never), |acc, field_expr| {
                            let field_type = get_expr_return_type(context, field_expr, false)?;
                            Ok(context.get_type_union(&acc, &field_type)?)
                        })?;

                new_type!(Array(Box::new(child_type)))
            }
        }
        RawExpr::UnwrapOptional(expr) => {
            let type_ = get_expr_return_type(context, expr.as_ref(), false)?;
            match type_.raw_type {
                RawType::Nil => new_type!(Never),
                RawType::Optional(child) => context.resolve_type(&child)?,
                _ => {
                    return Err(CompileError::new(
                        format!("Cannot unwrap type {} , it cannot be nil", type_),
                        Some(expr.pos.clone()),
                    ))
                }
            }
        }
        RawExpr::Cast(_expr, type_) => context.resolve_type(&type_)?,
        RawExpr::AtCall { at_function, args } => match at_function {
            ast::AtFunction::Len => new_type!(Int),
            ast::AtFunction::Abs => {
                let type_ = get_expr_return_type(context, &args[0], false)?;
                match &type_.raw_type {
                    RawType::Int => new_type!(Int),
                    RawType::Real => new_type!(Real),
                    RawType::Ident(_) => unreachable!(),

                    _ => {
                        return Err(CompileError::new(
                            format!("Expected int or real (for @abs()), got {}", type_),
                            Some(args[0].pos.clone()),
                        ))
                    }
                }
            }
            ast::AtFunction::Min | ast::AtFunction::Max => {
                let mut is_all_int = true;
                for arg in args.iter() {
                    let type_ = get_expr_return_type(context, &arg, false)?;
                    match &type_.raw_type {
                        RawType::Int => {}
                        RawType::Real => is_all_int = false,
                        RawType::Ident(_) => unreachable!(),
                        _ => {
                            return Err(CompileError::new(
                                format!(
                                    "Expected int or real (for @{:?}()), got {}",
                                    at_function, type_
                                ),
                                Some(arg.pos.clone()),
                            ))
                        }
                    }
                }

                if is_all_int {
                    new_type!(Int)
                } else {
                    new_type!(Real)
                }
            }
            ast::AtFunction::Insert => new_type!(Nil),
        },
    };

    if let RawType::Ident(_name) = &return_type.raw_type {
        unimplemented!(
            "get_expr_return_type({:#?}) returned {}",
            expr.raw_expr,
            return_type
        );
    }

    Ok(return_type)
}

fn unknown_field_error(
    container_type: &ast::Type,
    field_name: &str,
    pos: crate::TokenPos,
) -> CompileError {
    CompileError::new(
        format!("Unknown field '{}' in type {}", field_name, container_type),
        Some(pos),
    )
}

fn expect_type(
    context: &CompilationContext,
    expected_type: &ast::Type,
    received_type: &ast::Type,
    pos: &crate::TokenPos,
) -> Result<(), CompileError> {
    if let Some(type_inclusion_error) = context.is_type_included_in(expected_type, received_type)? {
        // dbg!(context
        //     .scope_stack
        //     .iter()
        //     .map(|scope| &scope.checks)
        //     .collect::<Vec<_>>());
        return Err(unexpected_type_error(
            expected_type,
            received_type,
            Some(type_inclusion_error),
            pos.clone(),
        ));
    }
    Ok(())
}

fn unexpected_type_error(
    expected_type: &ast::Type,
    received_type: &ast::Type,
    reason: Option<TypeInclusionError>,
    pos: crate::TokenPos,
) -> CompileError {
    let notes;
    if let Some(reason) = &reason {
        notes = reason.get_notes(expected_type, received_type);
    } else {
        notes = Vec::new();
    }
    CompileError {
        description: format!("expected type {}, got {}", expected_type, received_type),
        pos: Some(pos),
        notes,
    }
}
