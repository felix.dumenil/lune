use crate::ast;
use crate::CompileError;

#[derive(Debug)]
struct Transcriber {
    lua_src: String,
    indent: usize,
    line_started: bool,
}

impl Default for Transcriber {
    fn default() -> Self {
        Transcriber {
            lua_src: String::new(),
            indent: 0,
            line_started: false,
        }
    }
}

impl Transcriber {
    pub fn add_line(&mut self, line: &str) {
        self.add_str(line);
        self.start_new_line();
    }
    pub fn add_str(&mut self, s: &str) {
        self.add_indent_if_needed();
        self.lua_src.push_str(s);
    }
    pub fn add_char(&mut self, c: char) {
        self.add_indent_if_needed();
        self.lua_src.push(c);
    }
    pub fn add_indent_if_needed(&mut self) {
        if !self.line_started {
            self.lua_src.push_str(&"    ".repeat(self.indent));
            self.line_started = true;
        }
    }
    pub fn start_new_line(&mut self) {
        self.lua_src.push('\n');
        self.line_started = false;
    }
    pub fn indent(&mut self) {
        self.indent += 1;
    }
    pub fn unindent(&mut self) {
        self.indent -= 1;
    }
}

pub fn transcribe_ast(ast: &ast::Ast) -> Result<String, CompileError> {
    let mut transcriber = Transcriber::default();

    for top_level_field in ast.iter() {
        match top_level_field {
            ast::TopLevelField::Function { .. } => {
                transcribe_function(top_level_field, &mut transcriber)?;
                transcriber.start_new_line();
            }
            ast::TopLevelField::Class {
                name,
                body: _,
                pos: _,
            } => {
                transcriber.add_str(name);
                transcriber.add_str(" = setmetatable({}, {__call = function(self, vars)");
                transcriber.start_new_line();
                transcriber.indent();

                transcriber.add_str("return setmetatable(vars, {__index=self})");
                transcriber.start_new_line();

                transcriber.unindent();
                transcriber.add_str("end})");
                transcriber.start_new_line();
                transcriber.start_new_line();
            }
            ast::TopLevelField::Declare { .. } => {}
            ast::TopLevelField::Require { pos: _, path } => {
                transcriber.add_str("require(");
                let path_no_ext = path.rsplitn(2, '.').last().unwrap();
                transcribe_string(path_no_ext, &mut transcriber);
                transcriber.add_str(")");
                transcriber.start_new_line();
                transcriber.start_new_line();
            }
            ast::TopLevelField::Global {
                pos: _,
                is_mut: _,
                name,
                type_: _,
                value,
            } => {
                transcriber.add_str(name);
                transcriber.add_str(" = ");
                transcribe_expr(value, false, &mut transcriber)?;
                transcriber.start_new_line();
                transcriber.start_new_line();
            }
        }
    }

    Ok(transcriber.lua_src)
}

fn transcribe_function(
    function: &ast::TopLevelField,
    transcriber: &mut Transcriber,
) -> Result<(), CompileError> {
    if let ast::TopLevelField::Function {
        pos: _,
        container,
        name,
        args,
        return_type: _,
        prototype_pos: _,
        body,
    } = function
    {
        transcriber.add_str("function ");
        match container {
            ast::FunctionContainer::StaticFunction(container_name, _container_pos) => {
                transcriber.add_str(container_name);
                transcriber.add_str(".");
            }
            ast::FunctionContainer::Method(container_name, _container_pos) => {
                transcriber.add_str(container_name);
                transcriber.add_str(":");
            }
            ast::FunctionContainer::None => {}
        }
        transcriber.add_str(name);
        transcriber.add_str("(");

        for (i, arg) in args.iter().enumerate() {
            if i != 0 {
                transcriber.add_str(", ");
            }
            transcriber.add_str(&arg.0);
        }
        transcriber.add_str(")");
        transcriber.start_new_line();
        transcriber.indent();
        // body
        for inst in body.insts.iter() {
            transcribe_inst(inst, transcriber)?;
        }
        transcriber.unindent();
        transcriber.add_line("end");

        Ok(())
    } else {
        unreachable!()
    }
}

fn transcribe_inst(inst: &ast::Inst, transcriber: &mut Transcriber) -> Result<(), CompileError> {
    match &inst.raw_inst {
        ast::RawInst::Let {
            is_mut: _,
            name,
            type_: _,
            value,
        } => {
            transcriber.add_str("local ");
            transcriber.add_str(name);
            if let Some(value) = value {
                transcriber.add_str(" = ");
                transcribe_expr(value, false, transcriber)?;
            }
            transcriber.add_char(';');
            transcriber.start_new_line();
        }
        ast::RawInst::If {
            condition,
            then_block,
            else_block,
        } => {
            transcriber.add_str("if ");
            transcribe_expr(condition, false, transcriber)?;
            transcriber.add_str(" then");
            transcriber.indent();
            transcriber.start_new_line();

            for inst in then_block.insts.iter() {
                transcribe_inst(inst, transcriber)?;
            }

            transcriber.unindent();

            if let Some(else_block) = else_block {
                transcriber.add_str("else");
                transcriber.indent();
                transcriber.start_new_line();

                for inst in else_block.insts.iter() {
                    transcribe_inst(inst, transcriber)?;
                }

                transcriber.unindent();
            }
            transcriber.add_str("end");
            transcriber.start_new_line();
        }
        ast::RawInst::Expr(expr) => {
            transcribe_expr(expr, false, transcriber)?;
            transcriber.add_char(';');
            transcriber.start_new_line();
        }
        ast::RawInst::Assign(dst_expr, src_expr) => {
            transcribe_expr(dst_expr, false, transcriber)?;
            transcriber.add_str(" = ");
            transcribe_expr(src_expr, false, transcriber)?;
            transcriber.add_char(';');
            transcriber.start_new_line();
        }
        ast::RawInst::Return(expr) => {
            transcriber.add_str("return ");
            transcribe_expr(expr, false, transcriber)?;
            transcriber.start_new_line();
        }
        ast::RawInst::While { condition, block } => {
            transcriber.add_str("while ");
            transcribe_expr(condition, false, transcriber)?;
            transcriber.add_str(" do");
            transcriber.indent();
            transcriber.start_new_line();

            for inst in block.insts.iter() {
                transcribe_inst(inst, transcriber)?;
            }

            transcriber.unindent();
            transcriber.add_str("end");
            transcriber.start_new_line();
        }
        ast::RawInst::For {
            index_pos: _,
            index_ident,
            value_pos: _,
            value_ident,
            iter_type,
            iterator_expr,
            block,
        } => {
            transcriber.add_str("for ");
            transcriber.add_str(index_ident);
            transcriber.add_str(", ");
            transcriber.add_str(value_ident);
            transcriber.add_str(" in ");

            match iter_type {
                ast::ForIterType::Array => transcriber.add_str("ipairs("),
                ast::ForIterType::Map => transcriber.add_str("pairs("),
            }

            transcribe_expr(iterator_expr, false, transcriber)?;
            transcriber.add_str(") do");
            transcriber.indent();
            transcriber.start_new_line();

            for inst in block.insts.iter() {
                transcribe_inst(inst, transcriber)?;
            }

            transcriber.unindent();
            transcriber.add_str("end");
            transcriber.start_new_line();
        }
        ast::RawInst::ForRange {
            index_pos: _,
            index_ident,
            start_expr,
            end_expr,
            block,
        } => {
            transcriber.add_str("for ");
            transcriber.add_str(index_ident);
            transcriber.add_str(" = ");
            transcribe_expr(start_expr, false, transcriber)?;
            transcriber.add_str(", ");
            transcribe_expr(end_expr, false, transcriber)?;

            transcriber.add_str(" do");
            transcriber.indent();
            transcriber.start_new_line();

            for inst in block.insts.iter() {
                transcribe_inst(inst, transcriber)?;
            }

            transcriber.unindent();
            transcriber.add_str("end");
            transcriber.start_new_line();
        }
    }
    Ok(())
}

fn transcribe_expr(
    expr: &ast::Expr,
    ensure_parens: bool,
    transcriber: &mut Transcriber,
) -> Result<(), CompileError> {
    use ast::RawExpr;
    match &expr.raw_expr {
        RawExpr::IntLiteral(i) => transcriber.add_str(&format!("{}", i)),
        RawExpr::RealLiteral(r) => transcriber.add_str(&format!("{}", r)),
        RawExpr::BoolLiteral(b) => transcriber.add_str(&format!("{}", b)),
        RawExpr::NilLiteral => transcriber.add_str("nil"),
        RawExpr::StringLiteral(s) => transcribe_string(s, transcriber),
        RawExpr::Literal(s) => transcriber.add_str(&s),
        RawExpr::FunctionCall {
            function_expr,
            args,
        } => {
            transcribe_expr(function_expr, true, transcriber)?;
            transcriber.add_char('(');
            for (i, arg_expr) in args.iter().enumerate() {
                if i != 0 {
                    transcriber.add_str(", ");
                }
                transcribe_expr(arg_expr, false, transcriber)?;
            }
            transcriber.add_char(')');
        }
        RawExpr::New { class_type, fields } => {
            let class_name = match &class_type.as_ref().raw_type {
                ast::RawType::Ident(class_name) => class_name,
                _ => panic!("can't instanciate this type"),
            };
            transcriber.add_str(class_name);
            transcriber.add_str("(");
            transcribe_table_literal(fields, transcriber)?;
            transcriber.add_str(")");
        }
        RawExpr::StaticIndex {
            container_expr,
            index,
        } => {
            transcribe_expr(container_expr, true, transcriber)?;
            transcriber.add_str(".");
            transcriber.add_str(index);
        }
        RawExpr::MethodCall {
            object_expr,
            function_name,
            args,
        } => {
            transcribe_expr(object_expr, true, transcriber)?;
            transcriber.add_str(":");
            transcriber.add_str(function_name);
            transcriber.add_str("(");
            for (i, arg) in args.iter().enumerate() {
                if i != 0 {
                    transcriber.add_str(", ");
                }
                transcribe_expr(arg, false, transcriber)?;
            }
            transcriber.add_str(")");
        }
        RawExpr::TableLiteral { fields } => {
            transcribe_table_literal(fields, transcriber)?;
        }
        RawExpr::Add(boxed) => {
            transcribe_binop(" + ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::Sub(boxed) => {
            transcribe_binop(" - ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::Mul(boxed) => {
            transcribe_binop(" * ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::Div(boxed) => {
            transcribe_binop(" / ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::Mod(boxed) => {
            transcribe_binop(" % ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::Pow(boxed) => {
            transcribe_binop(" ^ ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::Concat(boxed) => {
            transcribe_binop(" .. ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::Eq(boxed) => {
            transcribe_binop(" == ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::NotEq(boxed) => {
            transcribe_binop(" ~= ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::Greater(boxed) => {
            transcribe_binop(" > ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::GreaterEq(boxed) => {
            transcribe_binop(" >= ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::Lower(boxed) => {
            transcribe_binop(" < ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::LowerEq(boxed) => {
            transcribe_binop(" <= ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::And(boxed) => {
            transcribe_binop(" and ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::Or(boxed) => {
            transcribe_binop(" or ", &boxed.0, &boxed.1, ensure_parens, transcriber)?
        }
        RawExpr::Not(subexpr) => {
            if ensure_parens {
                transcriber.add_str("(")
            }
            transcriber.add_str("not ");
            transcribe_expr(subexpr, true, transcriber)?;
            if ensure_parens {
                transcriber.add_str(")")
            }
        }
        RawExpr::Minus(subexpr) => {
            if ensure_parens {
                transcriber.add_str("(")
            }
            transcriber.add_str("-");
            transcribe_expr(subexpr, true, transcriber)?;
            if ensure_parens {
                transcriber.add_str(")")
            }
        }
        RawExpr::Index {
            container_expr,
            index_expr,
        } => {
            transcribe_expr(container_expr, true, transcriber)?;
            transcriber.add_str("[");
            transcribe_expr(index_expr, false, transcriber)?;
            transcriber.add_str("]");
        }
        RawExpr::ArrayLiteral(items) => {
            transcriber.add_str("{");
            for (i, item) in items.iter().enumerate() {
                if i != 0 {
                    transcriber.add_str(", ");
                }
                transcribe_expr(item, false, transcriber)?;
            }
            transcriber.add_str("}");
        }
        RawExpr::UnwrapOptional(expr) => {
            if ensure_parens {
                transcriber.add_str("(");
            }
            transcribe_expr(expr, ensure_parens, transcriber)?;
            transcriber.add_str(" or error(\"unwrapped optional\")");
            if ensure_parens {
                transcriber.add_str(")");
            }
        }
        RawExpr::AtCall {
            at_function: ast::AtFunction::Len,
            args,
        } => {
            assert_eq!(args.len(), 1);

            transcriber.add_char('#');
            transcribe_expr(&args[0], true, transcriber)?;
        }
        RawExpr::AtCall { at_function, args } => {
            let function_name = match at_function {
                ast::AtFunction::Insert => "table.insert",
                ast::AtFunction::Abs => "math.abs",
                ast::AtFunction::Min => "math.min",
                ast::AtFunction::Max => "math.max",
                ast::AtFunction::Len => unreachable!(),
            };
            transcriber.add_str(function_name);
            transcriber.add_str("(");
            let mut wrote_field = false;
            for arg in args.iter() {
                if wrote_field {
                    transcriber.add_str(", ");
                }
                transcribe_expr(arg, false, transcriber)?;
                wrote_field = true;
            }
            transcriber.add_str(")");
        }
        RawExpr::Cast(expr, _type) => {
            transcribe_expr(expr, ensure_parens, transcriber)?;
        }
    }
    Ok(())
}

fn transcribe_string(s: &str, transcriber: &mut Transcriber) {
    transcriber.add_char('"');
    for c in s.chars() {
        match c {
            '\r' => transcriber.add_str("\\r"),
            '\t' => transcriber.add_str("\\t"),
            '\n' => transcriber.add_str("\\n"),
            '\"' => transcriber.add_str("\\\""),
            '\\' => transcriber.add_str("\\\\"),
            // TODO add more escapes
            _ => transcriber.add_char(c),
        }
    }
    transcriber.add_char('"');
}

fn transcribe_binop(
    binop: &str,
    a: &ast::Expr,
    b: &ast::Expr,
    ensure_parens: bool,
    transcriber: &mut Transcriber,
) -> Result<(), CompileError> {
    if ensure_parens {
        transcriber.add_str("(")
    }
    transcribe_expr(a, true, transcriber)?;
    transcriber.add_str(binop);
    transcribe_expr(b, true, transcriber)?;
    if ensure_parens {
        transcriber.add_str(")")
    }
    Ok(())
}

fn transcribe_table_literal(
    fields: &std::collections::BTreeMap<String, ast::Expr>,
    transcriber: &mut Transcriber,
) -> Result<(), CompileError> {
    transcriber.add_str("{");
    if !fields.is_empty() {
        transcriber.start_new_line();
        transcriber.indent();
        for (key, value_expr) in fields {
            transcriber.add_str(key);
            transcriber.add_str(" = ");
            transcribe_expr(value_expr, false, transcriber)?;
            transcriber.add_str(",");
            transcriber.start_new_line();
        }
        transcriber.unindent();
    }
    transcriber.add_str("}");
    Ok(())
}
