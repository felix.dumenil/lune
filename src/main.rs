use std::rc::Rc;

mod ast;
mod compiler;
mod parser;
mod tokenizer;
mod transcriber;

#[cfg(test)]
mod tests;

#[derive(Debug, PartialEq)]
pub struct CompileError {
    description: String,
    pos: Option<TokenPos>,
    notes: Vec<String>,
}

impl CompileError {
    pub fn new(description: String, pos: Option<TokenPos>) -> Self {
        Self {
            description,
            pos,
            notes: Vec::new(),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub struct TokenPos {
    filename: Option<Rc<String>>,
    start_line: u32,
    start_column: u32,
    end_line: u32,
    end_column: u32,
}

impl TokenPos {
    pub fn new(
        filename: Option<&str>,
        start_line: u32,
        start_column: u32,
        end_line: u32,
        end_column: u32,
    ) -> Self {
        Self {
            filename: filename.map(|f| Rc::new(f.to_string())),
            start_line,
            start_column,
            end_line,
            end_column,
        }
    }

    #[must_use]
    pub fn extend(&self, other: &Self) -> Self {
        assert_eq!(
            self.filename, other.filename,
            "why are you extending with a token from another file ?"
        );
        let (start_line, start_column) = std::cmp::min(
            (self.start_line, self.start_column),
            (other.start_line, other.start_column),
        );
        let (end_line, end_column) = std::cmp::max(
            (self.end_line, self.end_column),
            (other.end_line, other.end_column),
        );
        Self {
            filename: self.filename.clone(),
            start_line,
            start_column,
            end_line,
            end_column,
        }
    }
}

impl std::fmt::Display for CompileError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        if let Some(pos) = &self.pos {
            write!(f, "{} : {}", pos, self.description)?;
        } else {
            write!(f, "(unknown file) : {}", self.description)?;
        }
        Ok(())
    }
}

impl std::fmt::Display for TokenPos {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::result::Result<(), std::fmt::Error> {
        if let Some(filename) = &self.filename {
            write!(f, "{}:{}:{}", filename, self.start_line, self.start_column)?;
        } else {
            write!(f, "{}:{}", self.start_line, self.start_column)?;
        }
        Ok(())
    }
}

fn main() {
    let path = std::env::args().nth(1).expect("missing argument");
    let compile_result = compiler::compile_real_files(path);

    match compile_result {
        Ok(result) => {
            for (file_path, content) in result {
                eprintln!("writing {}", file_path.to_string_lossy());
                std::fs::write(file_path, content).expect("could not write file");
            }
        }
        Err(err) => {
            print_error(&err).expect("could not print error");
            std::process::exit(1);
        }
    }
}

fn print_error(err: &CompileError) -> Result<(), Box<dyn std::error::Error>> {
    eprintln!("error : {}", err);
    for note in &err.notes {
        eprintln!("note : {}", note);
    }
    if let Some(pos) = &err.pos {
        if let Some(filename) = &pos.filename {
            let source = std::fs::read_to_string(filename.as_ref())?;
            const CONTEXT_LINES: u32 = 2;
            let start_line: u32 = (pos.start_line - 1).saturating_sub(CONTEXT_LINES);
            let end_line: u32 = pos.end_line - 1 + CONTEXT_LINES;
            let total_context_lines = end_line - start_line + 1;
            let biggest_line_number = std::cmp::min(
                start_line + total_context_lines,
                source.lines().count() as u32,
            );
            let line_num_len = biggest_line_number.to_string().len();
            for (i, line) in source
                .lines()
                .enumerate()
                .skip(start_line as usize)
                .take(total_context_lines as usize)
            {
                let line = line.replace("\t", "        ");
                if pos.start_line as usize <= i + 1 && i < pos.end_line as usize {
                    eprintln!(
                        " {:>line_num_len$} > {}",
                        i + 1,
                        line,
                        line_num_len = line_num_len
                    );
                } else {
                    eprintln!(
                        " {:>line_num_len$} | {}",
                        i + 1,
                        line,
                        line_num_len = line_num_len
                    );
                }

                let highlight = |start_column, end_column| {
                    if end_column < start_column {
                        return;
                    }
                    eprintln!(
                        " {} | {}{}",
                        " ".repeat(line_num_len),
                        " ".repeat(start_column),
                        "^".repeat(end_column - start_column + 1)
                    );
                };

                let mut first_non_white_space_column = 0;
                for c in line.chars() {
                    match c {
                        ' ' => first_non_white_space_column += 1,
                        '\t' => first_non_white_space_column += 8,
                        _ => break,
                    }
                }

                if !line.is_empty() {
                    if i + 1 == pos.start_line as usize && i + 1 == pos.end_line as usize {
                        highlight(pos.start_column as usize, pos.end_column as usize);
                    } else if i + 1 == pos.start_line as usize {
                        highlight(pos.start_column as usize, line.len() - 1);
                    } else if i + 1 == pos.end_line as usize {
                        highlight(first_non_white_space_column, pos.end_column as usize);
                    } else if i + 1 > pos.start_line as usize && i + 1 < pos.end_line as usize {
                        highlight(first_non_white_space_column, line.len() - 1);
                    }
                }
            }
        }
    }
    Ok(())
}
