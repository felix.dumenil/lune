use crate::{CompileError, TokenPos};
use std::rc::Rc;

#[derive(Debug, PartialEq)]
pub enum RawToken {
    IntLiteral(i64),
    RealLiteral(f64),
    BoolLiteral(bool),
    NilLiteral,
    Literal(String),
    StringLiteral(String),

    ParenOpen,
    ParenClose,
    BracketOpen,
    BracketClose,
    BraceOpen,
    BraceClose,
    Semicolon,
    Colon,
    Comma,
    ArrowRight,
    Dot,
    QuestionMark,
    At,

    Declare,
    If,
    Else,
    While,
    For,
    In,
    Let,
    Mut,
    New,
    Return,
    As,

    Int,
    Real,
    Bool,
    String,
    Fn,
    Struct,
    Class,
    Enum,
    Union,
    Array,
    Map,
    Require,
    Anything,
    Never,

    Plus,
    Minus,
    Mul,
    Div,
    Mod,
    Concat,
    Pow,
    Len,

    Set,
    PlusSet,
    MinusSet,
    MulSet,
    DivSet,
    ModSet,
    ConcatSet,
    PowSet,

    Eq,
    NotEq,
    Greater,
    GreaterEq,
    Lower,
    LowerEq,

    Not,
    And,
    Or,
}

#[derive(Debug)]
pub struct Token {
    pub raw_tok: RawToken,
    pub pos: TokenPos,
}

impl RawToken {
    pub fn from_literal(s: String) -> RawToken {
        match s.as_str() {
            "fn" => RawToken::Fn,
            "if" => RawToken::If,
            "else" => RawToken::Else,
            "while" => RawToken::While,
            "let" => RawToken::Let,
            "mut" => RawToken::Mut,
            "for" => RawToken::For,
            "in" => RawToken::In,

            "true" => RawToken::BoolLiteral(true),
            "false" => RawToken::BoolLiteral(false),
            "nil" => RawToken::NilLiteral,

            "int" => RawToken::Int,
            "real" => RawToken::Real,
            "bool" => RawToken::Bool,
            "class" => RawToken::Class,
            "struct" => RawToken::Struct,
            "string" => RawToken::String,
            "enum" => RawToken::Enum,
            "union" => RawToken::Union,
            "array" => RawToken::Array,
            "map" => RawToken::Map,
            "anything" => RawToken::Anything,
            "never" => RawToken::Never,

            "declare" => RawToken::Declare,
            "and" => RawToken::And,
            "or" => RawToken::Or,
            "new" => RawToken::New,
            "return" => RawToken::Return,
            "require" => RawToken::Require,
            "as" => RawToken::As,

            _ => RawToken::Literal(s),
        }
    }
}

#[derive(Debug)]
enum State {
    Start,

    Literal(String),
    StringLiteral(String),
    StringLiteralAntislash(String),
    Digits(String),
    DigitsDot(String),
    DigitsDotDigits(String),

    Plus,
    PlusPlus,
    Minus,
    Star,
    Slash,
    Percent,
    Pow,
    Eq,
    Greater,
    Lower,
    ExclamationMark,

    LineComment,
    MultilineComment,
    MultilineCommentStar,
}

pub fn tokenize(source: &str, filename: Option<Rc<String>>) -> Result<Vec<Token>, CompileError> {
    let mut tokens = Vec::new();
    let mut state = State::Start;

    let mut line: u32 = 1;
    let mut column: u32 = 0;
    let mut token_pos = TokenPos {
        start_line: line,
        start_column: line,
        end_line: line,
        end_column: line,
        filename,
    };
    let mut previous_token_pos = token_pos.clone();

    macro_rules! finish_tok {
        ($state:expr) => {
            finish_token(&mut tokens, $state, &previous_token_pos)
        };
    }

    macro_rules! new_token {
        ($token_name:ident) => {
            Token {
                raw_tok: RawToken::$token_name,
                pos: token_pos.clone(),
            }
        };
        ($token_name:ident($token_arg:expr)) => {
            Token {
                raw_tok: RawToken::$token_name($token_arg),
                pos: token_pos.clone(),
            }
        };
    }

    fn finish_token(
        tokens: &mut Vec<Token>,
        state: State,
        pos: &TokenPos,
    ) -> Result<(), CompileError> {
        let raw_tok = match state {
            State::Start | State::LineComment => None,
            State::Literal(s) => Some(RawToken::from_literal(s)),
            State::StringLiteral(_s) | State::StringLiteralAntislash(_s) => {
                return Err(CompileError::new(
                    "string literal is not closed".to_string(),
                    Some(pos.clone()),
                ))
            }
            State::Digits(s) => Some(RawToken::IntLiteral(
                s.parse().expect("Digits should contain a valid int"),
            )),
            State::DigitsDot(_s) => {
                return Err(CompileError::new(
                    "real literal is not closed".to_string(),
                    Some(pos.clone()),
                ))
            }
            State::DigitsDotDigits(s) => Some(RawToken::RealLiteral(
                s.parse()
                    .expect("DigitsDotDigits should contain a valid real"),
            )),
            State::Plus => Some(RawToken::Plus),
            State::PlusPlus => Some(RawToken::Concat),
            State::Minus => Some(RawToken::Minus),
            State::Star => Some(RawToken::Mul),
            State::Slash => Some(RawToken::Div),
            State::Percent => Some(RawToken::Mod),
            State::Eq => Some(RawToken::Set),
            State::Greater => Some(RawToken::Greater),
            State::Lower => Some(RawToken::Lower),
            State::ExclamationMark => Some(RawToken::Not),
            State::Pow => Some(RawToken::Pow),
            State::MultilineComment | State::MultilineCommentStar => {
                return Err(CompileError::new(
                    "multi-line comment is not closed".to_string(),
                    Some(pos.clone()),
                ))
            }
        };

        if let Some(raw_tok) = raw_tok {
            tokens.push(Token {
                raw_tok,
                pos: pos.clone(),
            });
        }

        Ok(())
    }

    for c in source.chars() {
        loop {
            match &mut state {
                State::Start => {
                    token_pos.start_line = line;
                    token_pos.start_column = column;
                    token_pos.end_line = line;
                    token_pos.end_column = column;
                    match c {
                        ' ' | '\t' | '\r' | '\n' => {}
                        'a'..='z' | 'A'..='Z' | '_' => state = State::Literal(c.to_string()),
                        '(' => tokens.push(new_token!(ParenOpen)),
                        ')' => tokens.push(new_token!(ParenClose)),
                        '[' => tokens.push(new_token!(BracketOpen)),
                        ']' => tokens.push(new_token!(BracketClose)),
                        '{' => tokens.push(new_token!(BraceOpen)),
                        '}' => tokens.push(new_token!(BraceClose)),
                        '=' => state = State::Eq,
                        '>' => state = State::Greater,
                        '<' => state = State::Lower,
                        '!' => state = State::ExclamationMark,
                        '^' => state = State::Pow,
                        ';' => tokens.push(new_token!(Semicolon)),
                        ':' => tokens.push(new_token!(Colon)),
                        ',' => tokens.push(new_token!(Comma)),
                        '.' => tokens.push(new_token!(Dot)),
                        '#' => tokens.push(new_token!(Len)),
                        '?' => tokens.push(new_token!(QuestionMark)),
                        '@' => tokens.push(new_token!(At)),

                        '0'..='9' => state = State::Digits(c.to_string()),

                        '+' => state = State::Plus,
                        '-' => state = State::Minus,
                        '*' => state = State::Star,
                        '/' => state = State::Slash,
                        '%' => state = State::Percent,
                        '"' => state = State::StringLiteral(String::new()),

                        _ => {
                            return Err(CompileError::new(
                                format!("Unexpected char : {:?}", c),
                                Some(token_pos),
                            ))
                        }
                    }
                }
                State::Literal(s) => match c {
                    'a'..='z' | 'A'..='Z' | '_' | '0'..='9' => s.push(c),
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::Digits(s) => match c {
                    '0'..='9' => s.push(c),
                    '.' => {
                        s.push(c);
                        state = State::DigitsDot(s.clone());
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::DigitsDot(s) => match c {
                    '0'..='9' => {
                        s.push(c);
                        state = State::DigitsDotDigits(s.clone());
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::DigitsDotDigits(s) => match c {
                    '0'..='9' => s.push(c),
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::StringLiteral(s) => match c {
                    '"' => match std::mem::replace(&mut state, State::Start) {
                        State::StringLiteral(s) => tokens.push(new_token!(StringLiteral(s))),
                        _ => unreachable!(),
                    },
                    '\\' => state = State::StringLiteralAntislash(s.clone()),
                    _ => {
                        s.push(c);
                    }
                },
                State::StringLiteralAntislash(s) => {
                    match c {
                        'r' => s.push('\r'),
                        't' => s.push('\t'),
                        'n' => s.push('\n'),
                        'a'..='z' => {
                            return Err(CompileError::new(
                                format!("Invalid escape : '\\{:?}'", c),
                                Some(token_pos),
                            ))
                        }
                        _ => s.push(c),
                    }
                    state = State::StringLiteral(s.clone());
                }
                State::Plus => match c {
                    '+' => state = State::PlusPlus,
                    '=' => {
                        tokens.push(new_token!(PlusSet));
                        state = State::Start;
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::PlusPlus => match c {
                    '=' => {
                        tokens.push(new_token!(ConcatSet));
                        state = State::Start;
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::Minus => match c {
                    '=' => {
                        tokens.push(new_token!(MinusSet));
                        state = State::Start;
                    }
                    '>' => {
                        tokens.push(new_token!(ArrowRight));
                        state = State::Start;
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::Star => match c {
                    '=' => {
                        tokens.push(new_token!(MulSet));
                        state = State::Start;
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::Slash => match c {
                    '=' => {
                        tokens.push(new_token!(DivSet));
                        state = State::Start;
                    }
                    '/' => {
                        state = State::LineComment;
                    }
                    '*' => {
                        state = State::MultilineComment;
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::Percent => match c {
                    '=' => {
                        tokens.push(new_token!(ModSet));
                        state = State::Start;
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::Pow => match c {
                    '=' => {
                        tokens.push(new_token!(PowSet));
                        state = State::Start;
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::LineComment => {
                    if c == '\n' {
                        state = State::Start;
                    }
                }
                State::MultilineComment => {
                    if c == '*' {
                        state = State::MultilineCommentStar;
                    }
                }
                State::MultilineCommentStar => {
                    if c == '/' {
                        state = State::Start;
                    } else {
                        state = State::MultilineComment;
                    }
                }
                State::Eq => match c {
                    '=' => {
                        tokens.push(new_token!(Eq));
                        state = State::Start;
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::Greater => match c {
                    '=' => {
                        tokens.push(new_token!(GreaterEq));
                        state = State::Start;
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::Lower => match c {
                    '=' => {
                        tokens.push(new_token!(LowerEq));
                        state = State::Start;
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
                State::ExclamationMark => match c {
                    '=' => {
                        tokens.push(new_token!(NotEq));
                        state = State::Start;
                    }
                    _ => {
                        finish_tok!(std::mem::replace(&mut state, State::Start))?;
                        continue;
                    }
                },
            }
            break;
        }
        if c == '\n' {
            line += 1;
            column = 0;
        } else if c == '\t' {
            // TODO accurate tab handling
            column += 8;
        } else {
            column += 1;
        }
        previous_token_pos = token_pos.clone();
        token_pos.end_line = line;
        token_pos.end_column = column;
    }
    finish_tok!(state)?;

    Ok(tokens)
}
