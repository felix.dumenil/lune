use super::split_lines;
use crate::compiler::*;

fn test_compile_success(source: &str, expected_lua_code: &str) {
    let compile_result = match compile_memory_files(&[("main.lune", source)]) {
        Ok(compile_result) => compile_result,
        Err(compile_error) => {
            crate::print_error(&compile_error).unwrap();
            panic!("Compilation should not fail");
        }
    };

    assert_eq!(compile_result.len(), 1);
    let compiled_lua_code = compile_result
        .get(&std::path::PathBuf::from("main.lua"))
        .expect("main.lune should be compiled to main.lua");

    let compiled_lines = split_lines(compiled_lua_code);
    let expected_lines = split_lines(expected_lua_code);

    assert_eq!(
        compiled_lines.len(),
        expected_lines.len(),
        "wrong line count.\n----------\n{}\n----------\n{}\n----------\n",
        compiled_lines.join("\n"),
        expected_lines.join("\n")
    );

    for (compiled_line, expected_line) in compiled_lines.iter().zip(expected_lines.iter()) {
        assert_eq!(compiled_line, expected_line);
    }
}

#[test]
fn test_compilation() {
    test_compile_success(
        "
			declare print: fn (string);
			fn main(arg: bool) {
				print(\"hello, world\");
			}
		",
        "
            function main(arg)
                print(\"hello, world\");
            end
        ",
    );
    test_compile_success(
        "
			declare print: fn (string);
			fn main(cond_1: bool, cond_2: bool) {
				if cond_1 {
					print(\"if\");
				} else if cond_2 {
					print(\"else if\");
				} else {
					print(\"else\");
				}
			}
		",
        "
            function main(cond_1, cond_2)
                if cond_1 then
                    print(\"if\");
                else
                    if cond_2 then
                        print(\"else if\");
                    else
                        print(\"else\");
                    end
                end
            end
        ",
    );
    test_compile_success(
        "
			class Point {
				x: real,
				y: real,
			}

			fn Point:add(bb: Point) -> Point {
				let p = new Point {
					x = self.x + bb.x,
					y = self.y + bb.y,
				};
				return p;
			}

			fn Point: sub(b: Point) -> Point {
				let p: Point = new Point {
					x = self.x - b.x,
					y = self.y - b.y,
				};
				return p;
			}

			fn Point:mul(m: real) -> Point {
				let p: Point = new Point {
					x = self.x * m,
					y = self.y * m,
				};
				return p;
			}

			fn Point:len() -> real {
				let mut len_squared = 0.0;
				len_squared += self.x ^ 2;
				len_squared += self.y ^ 2;
				return len_squared^0.5;
			}

			fn Point:norm() -> Point {
				return self:mul(1 / self:len());
			}

			fn Point:rot_left() -> Point {
				return new Point { x = self.y, y = -self.x };
			}

			fn Point.dist(a: Point, b: Point) -> real {
				return Point.sub(a, b):len();
			}
		",
        "
            Point = setmetatable({}, {__call = function(self, vars)
                return setmetatable(vars, {__index=self})
            end})
            
            function Point:add(bb)
                local p = Point({
                    x = self.x + bb.x,
                    y = self.y + bb.y,
                });
                return p
            end
            
            function Point:sub(b)
                local p = Point({
                    x = self.x - b.x,
                    y = self.y - b.y,
                });
                return p
            end
            
            function Point:mul(m)
                local p = Point({
                    x = self.x * m,
                    y = self.y * m,
                });
                return p
            end
            
            function Point:len()
                local len_squared = 0;
                len_squared = len_squared + (self.x ^ 2);
                len_squared = len_squared + (self.y ^ 2);
                return len_squared ^ 0.5
            end
            
            function Point:norm()
                return self:mul(1 / self:len())
            end
            
            function Point:rot_left()
                return Point({
                    x = self.y,
                    y = -self.x,
                })
            end
            
            function Point.dist(a, b)
                return Point.sub(a, b):len()
            end
        ",
    );
    test_compile_success(
        "
            declare love: struct {
                graphics: struct {
                    print: fn (string, real, real),
                    lineWidth: real,
                }
            };

            fn main() {
                love.graphics.lineWidth = 2.5;
                love.graphics.print(\"aaa\", 0, 1);
            }
        ",
        "
            function main()
                love.graphics.lineWidth = 2.5;
                love.graphics.print(\"aaa\", 0, 1);
            end
        ",
    );
    test_compile_success(
        "
            fn main() {
                let A: enum {\"a\", \"b\"} = \"a\";
                let B: enum {\"a\", \"b\", \"c\"} = A;
                let C: string = B;
            }
        ",
        "
            function main()
                local A = \"a\";
                local B = A;
                local C = B;
            end
        ",
    );
    test_compile_success(
        "
            fn main() {
                let a: map(int -> string) = {};
            }
        ",
        "
            function main()
                local a = {};
            end
        ",
    );

    test_compile_success(
        "
            let arr: [int] = [];
            let st: struct { foo: ?int } = {};
            let ma: map(string -> int) = {};
        ",
        "
        arr = {}

        st = {}

        ma = {}
        ",
    );

    test_compile_success(
        "
            declare exit: fn (int) -> never;
            fn main() -> int {
                return exit(0);
            }
            fn im_out_of_here() -> never {
                return exit(0);
            }
        ",
        "
            function main()
                return exit(0)
            end

            function im_out_of_here()
                return exit(0)
            end
        ",
    );
}

#[test]
fn bin_ops() {
    test_compile_success(
        "
            fn main() {
                let A = 1 + 2;
                let B = A * 3;
                let C = B / 4;
                let D = C % 5;
                let E = D ^ 6;
                let F = \"abc\" ++ \"def\";

                let cmp1 = A == 3;
                let cmp2 = B != 9;
                let cmp3 = C <= 2.25;
                let cmp4 = C < 2.25;
                let cmp5 = C >= 2.25;
                let cmp6 = C > 2.25;
            }
        ",
        "
            function main()
                local A = 1 + 2;
                local B = A * 3;
                local C = B / 4;
                local D = C % 5;
                local E = D ^ 6;
                local F = \"abc\" .. \"def\";
                local cmp1 = A == 3;
                local cmp2 = B ~= 9;
                local cmp3 = C <= 2.25;
                local cmp4 = C < 2.25;
                local cmp5 = C >= 2.25;
                local cmp6 = C > 2.25;
            end
        ",
    );
}

#[test]
fn struct_indexation_with_enums() {
    test_compile_success(
        "
            fn main(a: struct { foo: int, bar: real }) {
                let i: enum {\"foo\", \"bar\"} = \"foo\";
                let c: real = a[i];
            }
        ",
        "
            function main(a)
                local i = \"foo\";
                local c = a[i];
            end
        ",
    );
}

#[test]
fn for_with_arrays() {
    test_compile_success(
        "
            fn main() {
                for i -> v in array [\"a\", \"b\", \"c\"] {
                    let x: string = v;
                    let ii: int = i;
                }
            }
        ",
        "
            function main()
                for i, v in ipairs({\"a\", \"b\", \"c\"}) do
                    local x = v;
                    local ii = i;
                end
            end
        ",
    );
}

#[test]
fn for_with_maps() {
    test_compile_success(
        "
            fn main() {
                for i -> v in map {a = 123, b = 456} {
                    let x: int = v;
                    let ii: string = i;
                }
            }
        ",
        "
            function main()
                for i, v in pairs({
                    a = 123,
                    b = 456,
                }) do
                    local x = v;
                    local ii = i;
                end
            end
        ",
    );
}

#[test]
fn for_range() {
    test_compile_success(
        "
            fn main(end_: int) {
                for i in 0, end_ {

                }
            }
        ",
        "
            function main(end_)
                for i = 0, end_ do
                end
            end
        ",
    );
}

#[test]
fn functions_with_optional_args() {
    test_compile_success(
        "
            declare foo: fn (int, ?int, ?struct { bar: ?int });
            fn aaa() {
                foo(123, 456, { bar = 789 });
                foo(123, 456, { bar = nil });
                foo(123, 456, {});
                foo(123, 456, nil);
                foo(123, 456);
                foo(123);
            }
        ",
        "
            function aaa()
                foo(123, 456, {
                    bar = 789,
                });
                foo(123, 456, {
                    bar = nil,
                });
                foo(123, 456, {});
                foo(123, 456, nil);
                foo(123, 456);
                foo(123);
            end
        ",
    );
}

#[test]
fn maps() {
    test_compile_success(
        "
            fn aaa() {
                let a: map(string -> real) = {
                    abc = 123,
                    ghj = 456,
                };
                let b: ?real = a[\"abc\"];
                let c: ?real = a.whatever;
                a.something = 789;
                a[\"else\"] = 123456;
                a[\"abc\"] = nil;
            }
        ",
        "
            function aaa()
                local a = {
                    abc = 123,
                    ghj = 456,
                };
                local b = a[\"abc\"];
                local c = a.whatever;
                a.something = 789;
                a[\"else\"] = 123456;
                a[\"abc\"] = nil;
            end
        ",
    );
}

#[test]
fn checks() {
    test_compile_success(
        "
            declare take_int: fn(int);
            declare take_opt_int: fn(?int);
            fn aaa() {
                let a: ?int = 123;

                if a != nil and a > 6 {
                    take_int(a);
                }

                if a == nil or a > 6 {
                    take_opt_int(a);
                }
            }
        ",
        "
            function aaa()
                local a = 123;
                if (a ~= nil) and (a > 6) then
                    take_int(a);
                end
                if (a == nil) or (a > 6) then
                    take_opt_int(a);
                end
            end
        ",
    );
}

#[test]
fn checks_lvalues() {
    // Checks don't keep from writing into an unwrapped optional
    test_compile_success(
        "
            fn aaa() {
                let mut a: ?int = nil;

                if a == nil {
                    a = 123;
                }
            }
        ",
        "
            function aaa()
                local a = nil;
                if a == nil then
                    a = 123;
                end
            end
        ",
    );

    test_compile_success(
        "
            fn aaa() {
                let a: struct {
                    foo: ?int,
                } = { foo = nil };

                if a.foo == nil {
                    a.foo = 123;
                }
            }
        ",
        "
            function aaa()
                local a = {
                    foo = nil,
                };
                if a.foo == nil then
                    a.foo = 123;
                end
            end
        ",
    );
}

#[test]
fn recursive_types() {
    test_compile_success(
        "
            class Recursive {
                foo: ?Recursive,
            }
            fn make_rec() -> Recursive {
                return new Recursive {
                    foo = new Recursive {
                        foo = nil,
                    },
                };
            }
        ",
        "
            Recursive = setmetatable({}, {__call = function(self, vars)
                return setmetatable(vars, {__index=self})
            end})

            function make_rec()
                return Recursive({
                    foo = Recursive({
                        foo = nil,
                    }),
                })
            end
        ",
    );

    test_compile_success(
        "
            class Ping {
                pong: ?Pong,
            }
            class Pong {
                ping: ?Ping,
            }
            fn make_ping() -> Ping {
                return new Ping {
                    pong = new Pong {
                        ping = nil,
                    },
                };
            }
        ",
        "
            Ping = setmetatable({}, {__call = function(self, vars)
                return setmetatable(vars, {__index=self})
            end})

            Pong = setmetatable({}, {__call = function(self, vars)
                return setmetatable(vars, {__index=self})
            end})

            function make_ping()
                return Ping({
                    pong = Pong({
                        ping = nil,
                    }),
                })
            end
        ",
    );
}

#[test]
fn bool_ops_on_optionals() {
    test_compile_success(
        "
            fn foo() {
                let a: ?int = 123;
                let b: int = a or 456;
                let c: ?int = a and 456;
                let d: int = (a and 456).?;
            }
        ",
        "
            function foo()
                local a = 123;
                local b = a or 456;
                local c = a and 456;
                local d = a and 456 or error(\"unwrapped optional\");
            end
        ",
    );
}

#[test]
fn test_no_unresolved_expr_type() {
    // Array index
    test_compile_success(
        "
            class Foo {
                bar: int,
            }
            fn foo(arr: [Foo]) {
                let a = arr[123].bar;
            }
        ",
        "
            Foo = setmetatable({}, {__call = function(self, vars)
                return setmetatable(vars, {__index=self})
            end})

            function foo(arr)
                local a = arr[123].bar;
            end
        ",
    );

    // Unwrap
    test_compile_success(
        "
            class Foo {
                bar: int,
            }
            fn foo(m: map(int -> Foo)) {
                let a = m[123].?.bar;
            }
        ",
        "
            Foo = setmetatable({}, {__call = function(self, vars)
                return setmetatable(vars, {__index=self})
            end})

            function foo(m)
                local a = (m[123] or error(\"unwrapped optional\")).bar;
            end
        ",
    );
}

#[test]
fn unions() {
    test_compile_success(
        "
            let foo: struct {
                common: bool,
                union (type) {
                    \"foo\" -> {
                        the_foo: int,
                        same_name: string,
                    },
                    \"bar\" -> {
                        the_bar: int,
                        same_name: int,
                    },
                }
            } = {
                common = true,
                type = \"foo\",
                the_foo = 1234,
                same_name = \"abcd\",
            };
        ",
        "
            foo = {
                common = true,
                same_name = \"abcd\",
                the_foo = 1234,
                type = \"foo\",
            }
        ",
    );
    test_compile_success(
        "
            fn foo() {
                let foo: ?struct { union(type) { \"bar\"-> {
                    my_field: int,
                } } } = {
                    type = \"bar\",
                    my_field = 123,
                };
                if foo != nil {
                    if foo.type == \"bar\" {
                        let the_field: int = foo.my_field;
                    }
                }
            }
        ",
        "
            function foo()
                local foo = {
                    my_field = 123,
                    type = \"bar\",
                };
                if foo ~= nil then
                    if foo.type == \"bar\" then
                        local the_field = foo.my_field;
                    end
                end
            end
        ",
    );
    test_compile_success(
        "
            class Foo {
                union(type) {
                    \"bar\"-> {
                        my_field: int,
                    }
                }
            }
            fn foo() {
                let foo: ?Foo = new Foo {
                    type = \"bar\",
                    my_field = 123,
                };
                if foo != nil {
                    if foo.type == \"bar\" {
                        let the_field: int = foo.my_field;
                    }
                }
            }
        ",
        "
            Foo = setmetatable({}, {__call = function(self, vars)
                return setmetatable(vars, {__index=self})
            end})

            function foo()
                local foo = Foo({
                    my_field = 123,
                    type = \"bar\",
                });
                if foo ~= nil then
                    if foo.type == \"bar\" then
                        local the_field = foo.my_field;
                    end
                end
            end
        ",
    );
    test_compile_success(
        "
            class Shape {
                union(type) {
                    \"point\"-> {
                        x: int,
                        y: int,
                    }
                }
            }
            fn get_coord(shape: Shape, coord: enum{\"x\", \"y\"}) -> int {
                if shape.type == \"point\" {
                    return shape[coord];
                } else {
                    return 123456;
                }
            }
        ",
        "
            Shape = setmetatable({}, {__call = function(self, vars)
                return setmetatable(vars, {__index=self})
            end})

            function get_coord(shape, coord)
                if shape.type == \"point\" then
                    return shape[coord]
                else
                    return 123456
                end
            end
        ",
    );
}

#[test]
fn type_casting() {
    test_compile_success(
        "
            let a: int = true as int;
        ",
        "
            a = true
        ",
    );
}

#[test]
fn partially_init_vars() {
    test_compile_success(
        "
            fn main() -> int {
                let foo: int;
                foo = 1;
                return foo;
            }
        ",
        "
            function main()
                local foo;
                foo = 1;
                return foo
            end
        ",
    );
    test_compile_success(
        "
            fn main(b: bool) -> int {
                let foo: int;
                if b {
                    foo = 1;
                    return foo;
                }
                return 2;
            }
        ",
        "
            function main(b)
                local foo;
                if b then
                    foo = 1;
                    return foo
                end
                return 2
            end
        ",
    );
    // maybe we should require mut for this
    test_compile_success(
        "
            fn main(b: bool) -> int {
                let foo: int;
                if b {
                    foo = 1;
                }
                foo = 2;
                return foo;
            }
        ",
        "
            function main(b)
                local foo;
                if b then
                    foo = 1;
                end
                foo = 2;
                return foo
            end
        ",
    );
    test_compile_success(
        "
            fn main(b: bool) -> int {
                let foo: int;
                if b {
                    foo = 1;
                } else {
                    foo = 2;
                }
                return foo;
            }
        ",
        "
            function main(b)
                local foo;
                if b then
                    foo = 1;
                else
                    foo = 2;
                end
                return foo
            end
        ",
    );
    test_compile_success(
        "
            fn main(b: bool) -> int {
                let foo: int;
                if b {
                    foo = 1;
                } else if !b {
                    foo = 2;
                } else {
                    foo = 3;
                }
                return foo;
            }
        ",
        "
            function main(b)
                local foo;
                if b then
                    foo = 1;
                else
                    if not b then
                        foo = 2;
                    else
                        foo = 3;
                    end
                end
                return foo
            end
        ",
    );
}

#[test]
fn blogpost_example() {
    test_compile_success(
        "
            class BlogPost {
                title: string,
                union(type) {
                    \"text\" -> {
                        content: string,
                    },
                    \"image\" -> {
                        path: string,
                    },
                },
                comment_count: int,
                date: ?struct {
                    day: int,
                    month: int,
                    year: int,
                },
            }

            declare escapeHTML: fn (string) -> string;
            declare tostring: fn (anything) -> string;

            fn BlogPost:toHTML() -> string {
                let mut html: string = \"<article>\";
                html ++= \"<h1>\" ++ escapeHTML(self.title) ++ \"</h1>\";
                let date = self.date;
                if date != nil {
                    let date_str = tostring(date.year) ++ \"-\" ++ tostring(date.month) ++ \"-\" ++ tostring(date.day);
                    html ++= \"<time>\" ++ date_str ++ \"</time>\";
                }
                if self.type == \"text\" {
                    html ++= \"<p>\" ++ escapeHTML(self.content) ++ \"</p>\";
                } else if self.type == \"image\"  {
                    html ++= \"<img src=\\\"\" ++ escapeHTML(self.path) ++ \"\\\" alt=\\\"A cool picture\\\"/>\";
                }
                html ++= \"</article>\";
                return html;
            }
        ",
        "
            BlogPost = setmetatable({}, {__call = function(self, vars)
                return setmetatable(vars, {__index=self})
            end})

            function BlogPost:toHTML()
                local html = \"<article>\";
                html = html .. ((\"<h1>\" .. escapeHTML(self.title)) .. \"</h1>\");
                local date = self.date;
                if date ~= nil then
                    local date_str = (((tostring(date.year) .. \"-\") .. tostring(date.month)) .. \"-\") .. tostring(date.day);
                    html = html .. ((\"<time>\" .. date_str) .. \"</time>\");
                end
                if self.type == \"text\" then
                    html = html .. ((\"<p>\" .. escapeHTML(self.content)) .. \"</p>\");
                else
                    if self.type == \"image\" then
                        html = html .. ((\"<img src=\\\"\" .. escapeHTML(self.path)) .. \"\\\" alt=\\\"A cool picture\\\"/>\");
                    end
                end
                html = html .. \"</article>\";
                return html
            end
        ",
    );
}
