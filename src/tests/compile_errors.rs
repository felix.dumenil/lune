use crate::compiler::*;

fn test_compile_error(source: &str, expected_error: &str) {
    assert_eq!(
        format!(
            "{}",
            compile_memory_files(&[(
                "main.lune",
                super::ignore_useless_whitespace(source).as_str()
            ),])
            .expect_err("Compilation should fail")
        ),
        format!("main.lune:{}", expected_error),
    );
    // TODO : test notes
}

#[test]
fn compile_errors() {
    test_compile_error(
        "
            fn main() {
                print(\"hello, world\");
            }
        ",
        "2:4 : unknown value \'print\'",
    );

    test_compile_error(
        "
            fn main() {
                let a = 1 + \"abc\";
            }
        ",
        "2:12 : Invalid types int and \"abc\" for binary op",
    );
    test_compile_error(
        "
            class Thing {
                foo: int,
            }
            fn main(thing: Thing) {
                let aaa = thing.bar;
            }
        ",
        "5:14 : Unknown field \'bar\' in type Thing",
    );
    test_compile_error(
        "
            fn main() {
                if \"aaa\" > 3 {

                }
            }
        ",
        "2:7 : Types \"aaa\" and int cannot be compared",
    );
    test_compile_error(
        "
            class Level {
                field: int,
            }
            fn main() {
                let level = new Level {};
            }
        ",
        "5:16 : field \'field\' (type: int) is missing from struct {}",
    );
    test_compile_error(
        "
            class Level {}
            fn main() {
                let level = new Level { field = 123 };
            }
        ",
        "3:36 : Unknown field \'field\' in type Level",
    );

    // Top level
    test_compile_error(
        "
            class Level {}
            let level = new Level { field = 123 };
        ",
        "2:32 : Unknown field \'field\' in type Level",
    );
}

#[test]
fn let_bindings_and_assign() {
    test_compile_error(
        "
            fn main() {
                let aaa = true;
                let aaa = 123;
            }
        ",
        "3:14 : value \'aaa\' already exists",
    );
    test_compile_error(
        "
            fn main() {
                let a = 1;
                a = 2;
            }
        ",
        "3:4 : binding 'a' is not mut",
    );
    test_compile_error(
        "
            fn main() {
                a = 2;
            }
        ",
        "2:4 : unknown value \'a\'",
    );
    test_compile_error(
        "
            fn main() {
                123 = 456;
            }
        ",
        "2:4 : invalid l-value : IntLiteral(123)",
    );
    test_compile_error(
        "
            fn main() -> int {
                let foo: int;
                return foo;
            }
        ",
        "3:11 : \'foo\' is not initialized",
    );
    test_compile_error(
        "
            fn main(b: bool) -> int {
                let foo: int;
                if (b) {
                    foo = 4;
                }
                return foo;
            }
        ",
        "6:11 : \'foo\' is not initialized",
    );
    test_compile_error(
        "
            fn main(b: bool) -> int {
                let foo: int;
                foo = 1;
                foo = 2;
                return foo;
            }
        ",
        "4:4 : binding \'foo\' is not mut",
    );
}

#[test]
fn enums() {
    test_compile_error(
        "
            fn main() {
                let item : enum {\"a\", \"b\"} = \"c\";
            }
        ",
        "2:4 : expected type enum {\"a\", \"b\"}, got \"c\"",
    );
    test_compile_error(
        "
            fn get_string() -> string {
                return \"a\";
            }
            fn main() {
                let item : enum {\"a\", \"b\"} = get_string();
            }
        ",
        "5:4 : expected type enum {\"a\", \"b\"}, got string",
    );
    test_compile_error(
        "
            fn main(a: struct { foo: int, bar: real }) {
                let i: enum {\"foo\", \"bar\", \"zoo\"} = \"foo\";
                let c: real = a[i];
            }
        ",
        "3:20 : cannot index struct {bar: real, foo: int} with enum {\"bar\", \"foo\", \"zoo\"} : doesn\'t have field \'zoo\'",
    );
    test_compile_error(
        "
            fn main(a: struct { foo: int, bar: real }) {
                let i: enum {\"foo\", \"bar\"} = \"foo\";
                let c: int = a[i];
            }
        ",
        "3:4 : expected type int, got real",
    );
}

#[test]
fn functions_args_and_return_values() {
    test_compile_error(
        "
            declare print: fn (string);
            fn main() {
                print(123);
            }
        ",
        "3:10 : expected type string, got int",
    );

    test_compile_error(
        "
            fn main(a: not_existing_type) {
            }
        ",
        "1:11 : unknown type \'not_existing_type\'",
    );

    test_compile_error(
        "
            fn main() -> not_existing_type {
            }
        ",
        "1:13 : unknown type \'not_existing_type\'",
    );

    test_compile_error(
        "
            fn main(bbb: bool, bbb: int) {
            }
        ",
        "1:24 : value \'bbb\' already exists",
    );

    test_compile_error(
        "
            declare foo: fn (int, ?int, ?struct { bar: int });
            fn aaa() {
                foo();
            }
        ",
        "3:4 : argument 1 missing, type int is not optional",
    );

    test_compile_error(
        "
            declare foo: fn (int, ?int, ?struct { bar: int });
            fn aaa() {
                foo(123, 456, { bar = 789 }, 101112);
            }
        ",
        "3:4 : Too many args (4) for fn (int, ?int, ?struct {bar: int}), should be 3",
    );

    //
    test_compile_error(
        "
            fn main() -> never {
            }
        ",
        "1:0 : Function \'main\' must return never",
    );

    test_compile_error(
        "
            fn main() -> never {
                return \"something\";
            }
        ",
        "2:4 : expected type never, got \"something\"",
    );
}

#[test]
fn global_bindings() {
    test_compile_error(
        "
            let a: int = false;
        ",
        "1:0 : expected type int, got bool",
    );

    test_compile_error(
        "
            let a: int = 123;

            fn foo() {
                a = 456;
            }
        ",
        "4:4 : binding \'a\' is not mut",
    );
}

#[test]
fn maps() {
    test_compile_error(
        "
            fn aaa() {
                let a: map(string -> real) = {
                    abc = 123,
                    ghj = 456,
                };
                let b: real = a[\"abc\"];
            }
        ",
        "6:4 : expected type real, got ?real",
    );

    test_compile_error(
        "
            fn aaa() {
                let a: map(string -> real) = {
                    abc = 123,
                    ghj = 456,
                };
                let b: real = a.abc;
            }
        ",
        "6:4 : expected type real, got ?real",
    );
}

#[test]
fn binop_optionals() {
    test_compile_error(
        "
            fn foo() {
                let a: ?int = 123;
                let b: int = a or \"string\";
            }
        ",
        "3:4 : expected type int, got anything",
    );
}

#[test]
fn at_functions() {
    test_compile_error(
        "
            fn foo() {
                let mut aaa = [123, 456];
                @insert(aaa, true);
            }
        ",
        "3:17 : expected type int, got bool",
    );
    test_compile_error(
        "
            fn foo() {
                let mut aaa = [123, 456];
                @insert(aaa);
            }
        ",
        "3:4 : @insert() function need two arguments, got 1",
    );
    test_compile_error(
        "
            fn foo() -> int {
                let mut aaa = 1234;
                return @len(aaa);
            }
        ",
        "3:16 : Expected array (for @len()), got int",
    );
    test_compile_error(
        "
            fn foo() -> int {
                return @len();
            }
        ",
        "2:11 : @len() function need one argument, got 0",
    );
}

#[test]
fn unions() {
    // wrong type
    test_compile_error(
        "
            let foo: struct {
                common: bool,
                union (type) {
                    \"foo\" -> {
                        the_foo: int,
                        same_name: string,
                    },
                    \"bar\" -> {
                        the_bar: int,
                        same_name: int,
                    },
                }
            } = {
                common = true,
                type = \"bar\",
                the_foo = 1234,
                same_name = \"abcd\",
            };
        ",
        "1:0 : expected type struct {common: bool, union (type) {\"bar\" -> {same_name: int, the_bar: int}, \"foo\" -> {same_name: string, the_foo: int}}}, got struct {common: bool, same_name: \"abcd\", the_foo: int, type: \"bar\"}",
    );
}

#[test]
fn for_range() {
    test_compile_error(
        "
            fn main() {
                for i in \"abc\", 10 {

                }
            }
        ",
        "2:13 : expected type int, got \"abc\"",
    );
    test_compile_error(
        "
            fn main() {
                for i in 0, \"abc\" {

                }
            }
        ",
        "2:16 : expected type int, got \"abc\"",
    );
}
