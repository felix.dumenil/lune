use crate::tokenizer::*;
use std::rc::Rc;

#[test]
fn tokenizing() {
    fn test_tokenize(source: &str, expected_tokens: Vec<RawToken>) {
        let tokens = tokenize(source, Option::<Rc<String>>::None).unwrap();
        let raw_tokens: Vec<RawToken> = tokens.into_iter().map(|tok| tok.raw_tok).collect();
        assert_eq!(raw_tokens, expected_tokens);
    }

    test_tokenize("   ", vec![]);
    test_tokenize(
        "  abc  def",
        vec![
            RawToken::Literal(String::from("abc")),
            RawToken::Literal(String::from("def")),
        ],
    );
    test_tokenize(
        "  fn() [123] {} : if else while a+b-c*d/e%f++g ; true false",
        vec![
            RawToken::Fn,
            RawToken::ParenOpen,
            RawToken::ParenClose,
            RawToken::BracketOpen,
            RawToken::IntLiteral(123),
            RawToken::BracketClose,
            RawToken::BraceOpen,
            RawToken::BraceClose,
            RawToken::Colon,
            RawToken::If,
            RawToken::Else,
            RawToken::While,
            RawToken::Literal(String::from("a")),
            RawToken::Plus,
            RawToken::Literal(String::from("b")),
            RawToken::Minus,
            RawToken::Literal(String::from("c")),
            RawToken::Mul,
            RawToken::Literal(String::from("d")),
            RawToken::Div,
            RawToken::Literal(String::from("e")),
            RawToken::Mod,
            RawToken::Literal(String::from("f")),
            RawToken::Concat,
            RawToken::Literal(String::from("g")),
            RawToken::Semicolon,
            RawToken::BoolLiteral(true),
            RawToken::BoolLiteral(false),
        ],
    );
    test_tokenize(
        "= += -= *= /= %= ++=  == > < >= <= !=  !",
        vec![
            RawToken::Set,
            RawToken::PlusSet,
            RawToken::MinusSet,
            RawToken::MulSet,
            RawToken::DivSet,
            RawToken::ModSet,
            RawToken::ConcatSet,
            RawToken::Eq,
            RawToken::Greater,
            RawToken::Lower,
            RawToken::GreaterEq,
            RawToken::LowerEq,
            RawToken::NotEq,
            RawToken::Not,
        ],
    );
    test_tokenize(
        "
        aaa
        // comment
        bbb
        ",
        vec![
            RawToken::Literal(String::from("aaa")),
            RawToken::Literal(String::from("bbb")),
        ],
    );
    test_tokenize(
        "
        aaa /* comment */ bbb
        ",
        vec![
            RawToken::Literal(String::from("aaa")),
            RawToken::Literal(String::from("bbb")),
        ],
    );
    test_tokenize(
        "
		\"abc def\"

		",
        vec![RawToken::StringLiteral(String::from("abc def"))],
    );
}
