use crate::ast;
use crate::new_expr;
use crate::parser::*;
use crate::TokenPos;

fn test_expr(source_expr: &str, expected_expr: ast::Expr) {
    let source_with_fn = format!("fn main () {{ \n{}\n ; }}", source_expr);
    let ast = parse(&source_with_fn, None).unwrap();
    if let ast::TopLevelField::Function { body, .. } = &ast[0] {
        if let ast::RawInst::Expr(expr) = &body.insts[0].raw_inst {
            assert_eq!(expr, &expected_expr, "Wrong expr : {:#?}", expr);
        } else {
            panic!("No expr found in {:?}", ast);
        }
    } else {
        panic!("No function found in {:?}", ast);
    }
}

#[test]
fn parsing() {
    use ast::{Inst, RawInst};
    let parsed_ast = parse(
        &super::ignore_useless_whitespace(
            "
            fn main() {
                let integer = 123;
                //let real_num = 123.0;
                let string_lit: string = \"abc\";
                let mut boolean: bool = true;
                if integer == 120 + 3 {
                    boolean = false;
                } else if thing {
                    boolean = true;
                } else {
                    // Jack shit
                }
                return 0;
            }

        ",
        ),
        None,
    );
    assert_eq!(
        parsed_ast,
        Ok(vec![ast::TopLevelField::Function {
            pos: TokenPos::new(None, 1, 0, 14, 0),
            container: ast::FunctionContainer::None,
            name: "main".to_string(),
            args: vec![].into_boxed_slice(),
            return_type: None,
            prototype_pos: TokenPos::new(None, 1, 0, 1, 8),
            body: ast::Block {
                insts: vec![
                    Inst {
                        raw_inst: RawInst::Let {
                            is_mut: false,
                            name: "integer".to_string(),
                            type_: None,
                            value: Some(new_expr!(
                                IntLiteral(123),
                                TokenPos::new(None, 2, 18, 2, 20)
                            )),
                        },
                        pos: TokenPos::new(None, 2, 4, 2, 21),
                    },
                    Inst {
                        raw_inst: RawInst::Let {
                            is_mut: false,
                            name: "string_lit".to_string(),
                            type_: Some(crate::new_type!(
                                String,
                                TokenPos::new(None, 4, 20, 4, 25)
                            )),
                            value: Some(new_expr!(
                                StringLiteral("abc".to_string()),
                                TokenPos::new(None, 4, 29, 4, 33),
                            )),
                        },
                        pos: TokenPos::new(None, 4, 4, 4, 34),
                    },
                    Inst {
                        raw_inst: RawInst::Let {
                            is_mut: true,
                            name: "boolean".to_string(),
                            type_: Some(crate::new_type!(Bool, TokenPos::new(None, 5, 21, 5, 24))),
                            value: Some(new_expr!(
                                BoolLiteral(true),
                                TokenPos::new(None, 5, 28, 5, 31)
                            )),
                        },
                        pos: TokenPos::new(None, 5, 4, 5, 32),
                    },
                    Inst {
                        raw_inst: RawInst::If {
                            condition: Box::new(new_expr!(
                                Eq(Box::new((
                                    new_expr!(
                                        Literal("integer".to_string()),
                                        TokenPos::new(None, 6, 7, 6, 13)
                                    ),
                                    new_expr!(
                                        Add(Box::new((
                                            new_expr!(
                                                IntLiteral(120),
                                                TokenPos::new(None, 6, 18, 6, 20)
                                            ),
                                            new_expr!(
                                                IntLiteral(3),
                                                TokenPos::new(None, 6, 24, 6, 24)
                                            ),
                                        ))),
                                        TokenPos::new(None, 6, 18, 6, 24)
                                    ),
                                ))),
                                TokenPos::new(None, 6, 7, 6, 24)
                            )),
                            then_block: ast::Block {
                                pos: TokenPos::new(None, 6, 26, 8, 4),
                                insts: vec![Inst {
                                    raw_inst: RawInst::Assign(
                                        new_expr!(
                                            Literal("boolean".to_string()),
                                            TokenPos::new(None, 7, 8, 7, 14)
                                        ),
                                        new_expr!(
                                            BoolLiteral(false),
                                            TokenPos::new(None, 7, 18, 7, 22)
                                        ),
                                    ),
                                    pos: TokenPos::new(None, 7, 8, 7, 23)
                                },]
                                .into_boxed_slice(),
                            },
                            else_block: Some(ast::Block {
                                insts: vec![Inst {
                                    raw_inst: RawInst::If {
                                        condition: Box::new(new_expr!(
                                            Literal("thing".to_string()),
                                            TokenPos::new(None, 8, 14, 8, 18)
                                        )),
                                        then_block: ast::Block {
                                            insts: vec![Inst {
                                                raw_inst: RawInst::Assign(
                                                    new_expr!(
                                                        Literal("boolean".to_string()),
                                                        TokenPos::new(None, 9, 8, 9, 14)
                                                    ),
                                                    new_expr!(
                                                        BoolLiteral(true),
                                                        TokenPos::new(None, 9, 18, 9, 21)
                                                    ),
                                                ),
                                                pos: TokenPos::new(None, 9, 8, 9, 22),
                                            },]
                                            .into_boxed_slice(),
                                            pos: TokenPos::new(None, 8, 20, 10, 4),
                                        },
                                        else_block: Some(ast::Block {
                                            pos: TokenPos::new(None, 10, 11, 12, 4),
                                            insts: vec![].into_boxed_slice(),
                                        }),
                                    },
                                    pos: TokenPos::new(None, 8, 11, 12, 4)
                                },]
                                .into_boxed_slice(),
                                pos: TokenPos::new(None, 8, 11, 12, 4),
                            }),
                        },
                        pos: TokenPos::new(None, 6, 4, 12, 4),
                    },
                    Inst {
                        raw_inst: RawInst::Return(new_expr!(
                            IntLiteral(0),
                            TokenPos::new(None, 13, 11, 13, 11)
                        )),
                        pos: TokenPos::new(None, 13, 4, 13, 12),
                    },
                ]
                .into_boxed_slice(),
                pos: TokenPos::new(None, 1, 10, 14, 0),
            }
        }]
        .into_boxed_slice()),
        "Wrong ast : {:#?}",
        parsed_ast,
    );

    test_expr(
        "a + b * c",
        new_expr!(
            Add(Box::new((
                new_expr!(Literal("a".to_string()), TokenPos::new(None, 2, 0, 2, 0)),
                new_expr!(
                    Mul(Box::new((
                        new_expr!(Literal("b".to_string()), TokenPos::new(None, 2, 4, 2, 4)),
                        new_expr!(Literal("c".to_string()), TokenPos::new(None, 2, 8, 2, 8)),
                    ))),
                    TokenPos::new(None, 2, 4, 2, 8)
                ),
            ))),
            TokenPos::new(None, 2, 0, 2, 8)
        ),
    );
    test_expr(
        "a * b + c",
        new_expr!(
            Add(Box::new((
                new_expr!(
                    Mul(Box::new((
                        new_expr!(Literal("a".to_string()), TokenPos::new(None, 2, 0, 2, 0)),
                        new_expr!(Literal("b".to_string()), TokenPos::new(None, 2, 4, 2, 4)),
                    ))),
                    TokenPos::new(None, 2, 0, 2, 4)
                ),
                new_expr!(Literal("c".to_string()), TokenPos::new(None, 2, 8, 2, 8)),
            ))),
            TokenPos::new(None, 2, 0, 2, 8)
        ),
    );
    test_expr(
        "a == b + c",
        new_expr!(
            Eq(Box::new((
                new_expr!(Literal("a".to_string()), TokenPos::new(None, 2, 0, 2, 0)),
                new_expr!(
                    Add(Box::new((
                        new_expr!(Literal("b".to_string()), TokenPos::new(None, 2, 5, 2, 5)),
                        new_expr!(Literal("c".to_string()), TokenPos::new(None, 2, 9, 2, 9)),
                    ))),
                    TokenPos::new(None, 2, 5, 2, 9)
                ),
            ))),
            TokenPos::new(None, 2, 0, 2, 9)
        ),
    );
    test_expr(
        "a + b == c",
        new_expr!(
            Eq(Box::new((
                new_expr!(
                    Add(Box::new((
                        new_expr!(Literal("a".to_string()), TokenPos::new(None, 2, 0, 2, 0)),
                        new_expr!(Literal("b".to_string()), TokenPos::new(None, 2, 4, 2, 4)),
                    ))),
                    TokenPos::new(None, 2, 0, 2, 4)
                ),
                new_expr!(Literal("c".to_string()), TokenPos::new(None, 2, 9, 2, 9)),
            ))),
            TokenPos::new(None, 2, 0, 2, 9)
        ),
    );
    test_expr(
        "!a != b",
        new_expr!(
            NotEq(Box::new((
                new_expr!(
                    Not(Box::new(new_expr!(
                        Literal("a".to_string()),
                        TokenPos::new(None, 2, 1, 2, 1)
                    ))),
                    TokenPos::new(None, 2, 0, 2, 1)
                ),
                new_expr!(Literal("b".to_string()), TokenPos::new(None, 2, 6, 2, 6)),
            ))),
            TokenPos::new(None, 2, 0, 2, 6)
        ),
    );
    test_expr(
        "a or b and c",
        new_expr!(
            And(Box::new((
                new_expr!(
                    Or(Box::new((
                        new_expr!(Literal("a".to_string()), TokenPos::new(None, 2, 0, 2, 0)),
                        new_expr!(Literal("b".to_string()), TokenPos::new(None, 2, 5, 2, 5)),
                    ))),
                    TokenPos::new(None, 2, 0, 2, 5)
                ),
                new_expr!(Literal("c".to_string()), TokenPos::new(None, 2, 11, 2, 11)),
            ))),
            TokenPos::new(None, 2, 0, 2, 11)
        ),
    );
    test_expr(
        "aaa:bbb(ccc, ddd)",
        new_expr!(
            MethodCall {
                object_expr: Box::new(new_expr!(
                    Literal("aaa".to_string()),
                    TokenPos::new(None, 2, 0, 2, 2)
                )),
                function_name: "bbb".to_string(),
                args: vec![
                    new_expr!(Literal("ccc".to_string()), TokenPos::new(None, 2, 8, 2, 10)),
                    new_expr!(
                        Literal("ddd".to_string()),
                        TokenPos::new(None, 2, 13, 2, 15)
                    ),
                ]
                .into_boxed_slice(),
            },
            TokenPos::new(None, 2, 0, 2, 16),
        ),
    );
}
