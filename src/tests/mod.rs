mod compile_errors;
mod compile_successes;
mod parser;
mod tokenizer;

// ignore leading and trailing empty lines, whitespace at start of lines
fn split_lines(source: &str) -> Vec<&str> {
    let mut lines = source.lines().collect::<Vec<&str>>();
    while lines
        .get(0)
        .map(|l| l.chars().all(|c| c.is_whitespace()))
        .unwrap_or(false)
    {
        lines.remove(0);
    }
    while lines
        .last()
        .map(|l| l.chars().all(|c| c.is_whitespace()))
        .unwrap_or(false)
    {
        lines.pop();
    }
    let min_indent = lines
        .iter()
        .filter(|line| !line.chars().all(|c| c.is_whitespace()))
        .map(|line| {
            let mut indent = 0;
            for b in line.bytes() {
                if !b.is_ascii_whitespace() {
                    break;
                } else {
                    indent += 1;
                }
            }
            indent
        })
        .min()
        .unwrap_or(0);
    for line in &mut lines {
        if line.len() >= min_indent && line[0..min_indent].chars().all(|c| c.is_whitespace()) {
            *line = &line[min_indent..];
        }
    }

    lines
}

fn ignore_useless_whitespace(source: &str) -> String {
    let lines = split_lines(source);
    lines.join("\n")
}
