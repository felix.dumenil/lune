# Lune - An experimental, strictly typed programming language that compile to Lua.

**Note** : This language is just a personal project, and is not meant for production.
This is why for now, it doesn't have a lot of documentation.

```rust
class BlogPost {
    title: string,
    union(type) {
        "text" -> {
            content: string,
        },
        "image" -> {
            path: string,
        },
    },
    comment_count: int,
    date: ?struct {
        day: int,
        month: int,
        year: int,
    },
}

declare escapeHTML: fn (string) -> string;
declare tostring: fn (anything) -> string;

fn BlogPost:toHTML() -> string {
    let mut html: string = "<article>";
    html ++= "<h1>" ++ escapeHTML(self.title) ++ "</h1>";
    let date = self.date;
    if date != nil {
        let date_str = tostring(date.year) ++ "-" ++ tostring(date.month) ++ "-" ++ tostring(date.day);
        html ++= "<time>" ++ date_str ++ "</time>";
    }
    if self.type == "text" {
        html ++= "<p>" ++ escapeHTML(self.content) ++ "</p>";
    } else if self.type == "image"  {
        html ++= "<img src=\"" ++ escapeHTML(self.path) ++ "\" alt=\"A cool picture\"/>";
    }
    html ++= "</article>";
    return html;
}
```

## Key features

- A syntax closer to what I like, with braces and semicolons.
- Types ! Enough types to do what I usually do in Lua, anyway.
- Globals variables must be declared before being used.
- Decent error messages that show the error's position in the source code.

## Things in it

- Top level `declare` instructions, to say that a variable exist without
  initializing it. Useful for using libraries.
- `enum`s are backed by strings, since that's I do it in Lua.
  Strings literals have a single item enum for type, and `string` is the superset of all enum types.
  You can use `enum` values to index a struct or class : 

```rust
fn get_coords(
	point: struct { x: real, y: real, z: real },
	coord_names: [enum {"x", "y", "z"}],
) -> [real] {
	let coords: [real] = [];
	for i -> coord_name in array coord_names {
		@insert(coords, point[coord_name]);
	}
	return coords;
}

```

- `class`es are just like `struct`s but with methods, there is no inheritance.
  They can be instantiated with `new Class { [fields] }`.
- `struct`s and `class`es can contains an `union` block.
- `map`s and arrays. Maps can be initialized from any compatible struct.

```rust
fn operations_on_maps() {
    let a: map(string -> real) = {
        abc = 123,
        ghj = 456,
    };
    let b: ?real = a["abc"];
    let c: ?real = a.whatever;
    a.something = 789.0;
    a["else"] = 123456;
    a["abc"] = nil;
}
```


- Optional types. Can be unwrapped with `if opt != nil { ... }` or just `opt.?`.
- If a variable is not `mut`able, not initialized and cannot contains `nil`,
  then the variable can be assigned to exactly once,
  and cannot be read from until it is initialized.

```rust
fn get_one_or_two(b: bool) -> int {
    let foo: int;
    if b {
        foo = 1;
    } else {
        foo = 2;
    }
    return foo;
}
```

## Limitations

- No generics. There are some special functions that start with a
  `@` (`@len()` and `@insert()`), that get compiled to existing Lua syntax
  and functions.
- `require()` is not a function, but a special top-level construct.
  Every declaration is in the same namespace, unless you explicitly put your function in a class.
- No intermediate byte-code, the type-checking is done directly on the AST.
  Then the transcription of the code to Lua is done without the types,
  so this means there are some constructs that could not be done, like `if`
  as expressions.

## Possible future features

- Constructor functions for classes. (So you could do `new Point(x,y)` instead of `new Point{.x=x,.y=y}`)
- Being able to use structs and classes as arrays, since in Lua, tables can act as both
- Static (constant) declarations in classes.
- `if` checks for fields (`st.var != nil`, `st.var == nil`)
- Type aliases (`type aaaa = struct { ... };`)
- default init values for class fields (would require type information during transcription).
- Improve single assignment : when a 'if' has one of the branchs return `never`, take the other's init vars
